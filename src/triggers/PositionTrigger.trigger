trigger PositionTrigger on Position__c (after  update) {
  PositionHelper.autoSharetoRecruiter(trigger.newMap,trigger.oldMap);
}