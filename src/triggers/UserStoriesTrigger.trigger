trigger UserStoriesTrigger on User_Stories__c (after insert, after update, after delete, after undelete) {
    
    Set<Id> sprintCount = new Set<Id>();
    
    if(Trigger.isDelete) {
        for(User_Stories__c us:Trigger.Old) { 
            if(us.Sprint__c <> NULL)    
                sprintCount.add(us.Sprint__c);      
        }     
    }
        if(Trigger.isUpdate) {
		 string operation='Update';
        	UserStoriesHelper.autoSharetoDeveloper(trigger.newMap,trigger.oldMap,operation);
            for(User_Stories__c us:Trigger.New) { 
                if(us.Sprint__c <> NULL)       
                    sprintCount.add(us.Sprint__c);      
            }
            for(User_Stories__c us:Trigger.Old) { 
                if(us.Sprint__c <> NULL)       
                    sprintCount.add(us.Sprint__c);       
            }      
        }
        
       if(Trigger.isInsert) {
	   string operation='Insert';
       	UserStoriesHelper.autoSharetoDeveloper(trigger.newMap,trigger.oldMap,operation);
        for(User_Stories__c us:Trigger.New) {
            if(us.Sprint__c <> NULL)        
               sprintCount.add(us.Sprint__c);       
        } 
    }   
    
    if(null<>sprintCount){
        UserStoriesHelper.autoCalculateFinances(sprintCount);
    }
    
}