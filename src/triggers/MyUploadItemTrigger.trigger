trigger MyUploadItemTrigger on myUpload_Item__c (before update, after update) {
    if(Trigger.isBefore && Trigger.isUpdate){
        MyUploadItemTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
    }
    
    
    if(Trigger.isAfter && Trigger.isUpdate){
        MyUploadItemTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
    }
}