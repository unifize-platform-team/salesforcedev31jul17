trigger JobApplicationtrigger on Job_Application__c (after insert, after update ,after delete) {
 
 if(trigger.isAfter){
 
 if(trigger.isInsert){
   string operation ='Insert';
   Map<id,Job_Application__c> oldMap = new Map<id,Job_Application__c>();
    JobApplicationHelper.autoSharetoRecruiter(trigger.newMap,oldMap,operation);
 }
 
 if(trigger.isUpdate){
  string operation  ='Update';
   JobApplicationHelper.autoSharetoRecruiter(trigger.newMap,trigger.oldMap,operation);
 }
 
 if(trigger.isDelete){
   string operation ='Delete';
   Map<id,Job_Application__c> newMap = new Map<id,Job_Application__c>();
    JobApplicationHelper.autoSharetoRecruiter(trigger.newMap,trigger.oldMap,operation);
 }
 
 }
 
}