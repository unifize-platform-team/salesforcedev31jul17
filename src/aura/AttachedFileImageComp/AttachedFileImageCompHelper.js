({
	getAttachmentId : function(cmp) {
		var action = cmp.get("c.getAttachmentId");
        action.setParams({ recordId : cmp.get("v.recordId") });
        
        action.setCallback(this, function(response) {
        	var state = response.getState();
            
            if (state === "SUCCESS") {
                var  attachmentId = response.getReturnValue();
                cmp.set("v.attachmentId", attachmentId);
            }
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
            	var errors = response.getError();
                if (errors) {
                	if (errors[0] && errors[0].message) {
                    	console.log("Error message: " +errors[0].message);
                    }
                }
                else {
                	console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
	},
    
    getUploadItemDetails : function(cmp) {
		var action = cmp.get("c.getUploadItemDetails");
        action.setParams({ recordId : cmp.get("v.recordId") });
        
        action.setCallback(this, function(response) {
        	var state = response.getState();
            
            if (state === "SUCCESS") {
                var  uploadItem = response.getReturnValue();
                cmp.set("v.fileName", uploadItem.File_Name__c);
                cmp.set("v.fileType", uploadItem.File_Type__c);
                
                var createdDate = new Date(uploadItem.CreatedDate);
                createdDate = createdDate.getDate() + '/' + createdDate.getMonth() + '/' + createdDate.getFullYear();
                
                cmp.set("v.createdDateDisplay", createdDate);
            }
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
            	var errors = response.getError();
                if (errors) {
                	if (errors[0] && errors[0].message) {
                    	console.log("Error message: " +errors[0].message);
                    }
                }
                else {
                	console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
	}
})