({
	doInit : function(component, event, helper) {
        var fromGrid = component.get("v.fromGrid");
        
        if(fromGrid != true){
            helper.getAttachmentId(component);
            helper.getUploadItemDetails(component);
        }
        
        var createdDate = component.get("v.createdDate");
        
        if(createdDate != null && createdDate != ''){
            createdDate = new Date(createdDate);
            createdDate = createdDate.getDate() + '/' + createdDate.getMonth() + '/' + createdDate.getFullYear();
            component.set("v.createdDateDisplay", createdDate);
        } 
	},

    
    openForgeViewer : function(component, event, helper){
        console.log(component.get("v.recordId"));
        var recordId = component.get("v.recordId");
        window.open("/apex/ForgeViewerPage?Id="+recordId);
	}
})