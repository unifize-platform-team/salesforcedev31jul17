({
    getAttachedFileDetails : function(cmp) {
		var action = cmp.get("c.getAttachedFileDetailsAfter");
        action.setParams({ recordId : cmp.get("v.recordId") });
        
        action.setCallback(this, function(response) {
        	var state = response.getState();
            
            if (state === "SUCCESS") {
                var  query = response.getReturnValue();
                if(query.myItem_Version_After__c != null && query.myItem_Version_After__r.Current_Attached_File__c != null){
                    cmp.set("v.attachedFileRecordId", query.myItem_Version_After__r.Current_Attached_File__c);
                }	
            }
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
            	var errors = response.getError();
                if (errors) {
                	if (errors[0] && errors[0].message) {
                    	console.log("Error message: " +errors[0].message);
                    }
                }
                else {
                	console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
	}
})