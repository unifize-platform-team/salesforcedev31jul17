({completeTask : function(cmp){


     var selectedTasks = cmp.find("taskTable").get("v.selectedRows");
     var selectedProjId = cmp.get("v.projectId");  
    
     var receivedIsFillerPart = cmp.get("v.isFillerPart");
    console.log("part value is  : "+receivedIsFillerPart);
    
     for(var i = 0;i < selectedTasks.length;i++){
         console.log(selectedTasks[i]);
     }
    
     var action = cmp.get("c.markTasksAsCompleted");
     action.setParams({
     "myPart":JSON.stringify(selectedTasks),
     "selectedProjId"  :selectedProjId ,
     "checkboxValue" : receivedIsFillerPart,
     });
     
     action.setCallback(this,function(resp){
         var state = resp.getState();
         if(cmp.isValid() && state === 'SUCCESS'){
              var toastEvent = $A.get("e.force:showToast");
               
            
              var message = resp.getReturnValue();
              console.log('data received: '+message);
              if(message.length==0)
              {
                  toastEvent.setParams({
                     "title": "Success!",
                     "type" : "success",
                     "message": "The items list has been Added successfully"
                 });
              }
             else
             {
                 alert('Items repeated : '+message);
             }
             
         		cmp.find("taskTable").rerenderRows();
        
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  "url": '/one/one.app#/sObject/'+cmp.get("v.recordId")+'/rlName/myProject_Parts__r/view',
                    "isredirect":true
                });
                urlEvent.fire();
     	}
     else{
     console.log(resp.getError());
     }
    
     });
    
     $A.enqueueAction(action);
     }
})