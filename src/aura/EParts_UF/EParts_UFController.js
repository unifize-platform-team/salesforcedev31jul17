({
    fetchTasks : function(cmp, event, helper) {
        
        var action = cmp.get("c.getTasksOfProject");
       
           
        //Column data for the table
        var taskColumns = [
            {
                'label':'MYPART ID',
                'name':'Name',
                'type':'reference',
                'value':'string',
                'resizeable':true
            },
            
            {
                'label':'DRAWING NO.',
                'name':'myPart__r.Drawing_No__c',
                'type':'string'
            },
            {
                'label':'MATERIAL',
                'name':'myMaterial__r.Name',
                'type':'string'
            },
            {
                'label':'PROCESS NAME',
                'name':'Process__r.Name',
                'type':'reference',
                'value':'Id',
                'resizeable':true
            },
            {
                'label':'STANDARD QUANTITY',
                'name':'Standard_Quantity__c',
                'type':'number'
            },
            {
                'label':'MYPART STATUS',
                'name':'myPart_Status__c',
                'type':'string'
            }
        ];
        
        //Configuration data for the table to enable actions in the table
        var taskTableConfig = {
            "massSelect":true,
            "searchByColumn":true,
            "globalAction":[
                {
                    "label":"Add Parts",
                    "type":"button",
                    "id":"completeTask",
                    "class":"slds-button slds-button--neutral slds-float--right"
                },
                {
                    "label":"Cancel",
                    "type":"button",
                    "id":"canceltask",
                    "class":"slds-button slds-button--neutral slds-float--right"
                }
            ]
            
        };
        
        if(cmp.get("v.projectId")){
            
            action.setParams({
                "projectId":cmp.get("v.projectId")
            });
            
            action.setCallback(this,function(resp){
                var state = resp.getState();
                if(cmp.isValid() && state === 'SUCCESS'){
                    //pass the records to be displayed
                    cmp.set("v.projectTasks",resp.getReturnValue());
                    
                    //pass the column information
                    cmp.set("v.taskColumns",taskColumns);
                    
                    
                    //pass the configuration of task table
                    cmp.set("v.taskTableConfig",taskTableConfig);
                    
                    //initialize the datatable
                    cmp.find("taskTable").initialize({
                        "order":[0,"desc"]
                    });
                }
                else{
                    console.log(resp.getError());
                }
            });
            
            $A.enqueueAction(action);
        }
        
    },
    tableActionHandler : function(cmp,event, helper) {
        
        //get the id of the action being fired
        var actionId = event.getParam('actionId');
        
        if(actionId == 'completeTask'){
             
            //get the row where click happened and its position
            var rowIdx = event.getParam("index");
            var clickedRow = event.getParam('row');
            //Complete the task
            helper.completeTask(cmp, clickedRow, rowIdx);
            
        }else if(actionId=='canceltask'){
            
                      var navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                          "recordId": cmp.get("v.projectId"),
                          "slideDevName": "related",
                            "isredirect" :true
                        });
                        navEvt.fire(); 
        }
    }
})