({
	getMyProjectPartsHelper : function(component, event, helper) {
		var action = component.get("c.getMyProjectParts");
		var srcProjectId = component.get("v.srcProjectId");
		var destProjectId = component.get("v.destProjectId");
		console.log('==============> srcProjectId : ' + srcProjectId);
		console.log('==============> destProjectId : ' + destProjectId);
        action.setParams({ 
            "srcProjectId": srcProjectId,
            "destProjectId": destProjectId
        }); 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var myProjectPartsList = JSON.parse(response.getReturnValue());
                console.log('==============> myProjectPartsList : ' + myProjectPartsList);
                component.set("v.myProjectPartsList", myProjectPartsList);
                setTimeout(function(){ 
                                            $('#example').DataTable({
                                                order: [[ 1, 'asc' ]],
                                                paging: true,
                                                "lengthMenu": [ 10, 20, 50, 100, 200 ],
                                                columnDefs: [ { orderable: false, targets: [0]}]
                                            }); 
                                        }, 1000);
            }
            else if (state === "ERROR") {
                helper.handleErrors(component, event, helper, response.getError());
            }
        });  
        $A.enqueueAction(action);
	},
	addProjectPartsHelper : function(component, event, helper) {
		var action = component.get("c.addProjectParts1");
		var srcProjectId = component.get("v.srcProjectId");
		var destProjectId = component.get("v.destProjectId");
		var myProjectPartsSelMap = component.get("v.myProjectPartsSelMap");
		console.log('==============> srcProjectId : ' + srcProjectId);
		console.log('==============> destProjectId : ' + destProjectId);
		console.log('==============> myProjectPartsSelMap : ' + myProjectPartsSelMap);
		console.log(JSON.stringify(myProjectPartsSelMap));
        action.setParams({ 
            "srcProjectId": srcProjectId,
            "destProjectId": destProjectId,
            "myProjectPartsSelMap" : JSON.stringify(myProjectPartsSelMap),
            "add":component.get("v.doAdd"),
            "overwrite":component.get("v.doOverwrite")
        }); 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
            	window.location.href = window.location.protocol + '//' + window.location.hostname + '/' + response.getReturnValue();
            }
            else if (state === "ERROR") {
                helper.handleErrors(component, event, helper, response.getError());
            }
        });  
        $A.enqueueAction(action);
	},
    checkExistMyparts : function(component, event, helper, errors) {
        
        
        var action = component.get("c.checkExistingParts");
		var srcProjectId = component.get("v.srcProjectId");
		var myProjectPartsSelMap = component.get("v.myProjectPartsSelMap");
		console.log('==============> srcProjectId : ' + srcProjectId);
		console.log('==============> myProjectPartsSelMap : ' + myProjectPartsSelMap);
        action.setParams({ 
            "srcProjectId": srcProjectId,
            "myProjectPartsSelMap" : JSON.stringify(myProjectPartsSelMap)
        }); 
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('---checked response--'+response.getReturnValue());
            if (component.isValid() && state === "SUCCESS") {
                if(response.getReturnValue()==true){
                    
                    document.getElementById('mypartswarningbox').style.display='block';
                }else{
                    helper.addProjectPartsHelper(component, event, helper);
                }
            }
            else if (state === "ERROR") {
                helper.handleErrors(component, event, helper, response.getError());
            }
        });  
        $A.enqueueAction(action);
    },
	handleErrors : function(component, event, helper, errors) {
        if (errors) {
            if (errors[0] && errors[0].message) {
                alert('Failure. Please try after sometime!');
                console.log("Error message: " + errors[0].message);
            }
        } else {
            console.log("Unknown error");
        }
	}
})