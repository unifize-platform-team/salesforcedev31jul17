({
	doInit : function(component, event, helper) {
		helper.getMyProjectPartsHelper(component, event, helper);
	},
	addProjectParts : function(component, event, helper) {
		event.stopPropagation();
		var myProjectPartsSelMap = component.get("v.myProjectPartsSelMap");
		console.log(myProjectPartsSelMap);
		if(myProjectPartsSelMap!=undefined && myProjectPartsSelMap!=null){
		    //helper.addProjectPartsHelper(component, event, helper);
		    helper.checkExistMyparts(component, event, helper);
	    }
		else{
			document.getElementById('propertyErrorBox').style.display='block';
		}
	},
	closebox : function(component, event, helper) {
		event.stopPropagation();
		document.getElementById('propertyErrorBox').style.display='none';
		document.getElementById('mypartswarningbox').style.display='none';
	},
    doAdd : function(component, event, helper) {
		event.stopPropagation();
        component.set("v.doAdd",true)
		helper.addProjectPartsHelper(component, event, helper);
	},
    doOverride : function(component, event, helper) {
		event.stopPropagation();
        component.set("v.doOverwrite",true);
		helper.addProjectPartsHelper(component, event, helper);
	},
	selectProjectPart : function(component, event, helper) {
		event.stopPropagation();
		var selRowId = event.target.id.split(':')[1];
		var selRowValue = document.getElementById(event.target.id).checked;
		var myProjectPartsSelMap = component.get("v.myProjectPartsSelMap");
		if(myProjectPartsSelMap == null){
			myProjectPartsSelMap = new Object();
		}
		if(selRowValue === true){
			var selRowQuantity = document.getElementById('quantity:' + selRowId).value;
			myProjectPartsSelMap[selRowId] = selRowQuantity;
		}
		else{
			delete myProjectPartsSelMap.selRowId;
		}
		component.set("v.myProjectPartsSelMap", myProjectPartsSelMap);
		
	},
	quantityChangeHandler : function(component, event, helper) {
		debugger;
		event.stopPropagation();
		var quantity = event.target.value;
		var selRowId = event.target.id.split(':')[1];
		var myProjectPartsSelMap = component.get("v.myProjectPartsSelMap");
		if(myProjectPartsSelMap != null && (selRowId in myProjectPartsSelMap)){
			myProjectPartsSelMap[selRowId] = quantity;
			component.set("v.myProjectPartsSelMap", myProjectPartsSelMap);
		}
	},
	cancelAddProjectParts : function(component, event, helper) {
		window.location.href = window.location.protocol + '//' + window.location.hostname + '/' + component.get("v.srcProjectId");
	}
})