({
	getUploadItems : function(cmp) {
		var action = cmp.get("c.getUploadItems");
        action.setParams({ recordId : cmp.get("v.recordId") });
        
        action.setCallback(this, function(response) {
        	var state = response.getState();
            
            if (state === "SUCCESS") {
                var uploadItems = response.getReturnValue();
                
                for(var i=0; i<uploadItems.length; i++){
                    uploadItems[i].isSelected = false;
                }
                
                cmp.set("v.uploadItems", uploadItems);
            }
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
            	var errors = response.getError();
                if (errors) {
                	if (errors[0] && errors[0].message) {
                    	console.log("Error message: " +errors[0].message);
                    }
                }
                else {
                	console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
	}
})