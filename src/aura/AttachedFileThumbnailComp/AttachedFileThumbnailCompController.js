({
	doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        if(recordId != null){
            helper.getUploadItemDetails(component);
        }        
	},

    
    openForgeViewer : function(component, event, helper){
        var recordId = component.get("v.recordId");
        window.open("/apex/ForgeViewerPage?Id="+recordId);
	}
})