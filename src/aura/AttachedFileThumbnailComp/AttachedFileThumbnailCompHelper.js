({
    getUploadItemDetails : function(cmp) {
		var action = cmp.get("c.getUploadItemDetails");
        action.setParams({ recordId : cmp.get("v.recordId") });
        
        action.setCallback(this, function(response) {
        	var state = response.getState();
            
            if (state === "SUCCESS") {
                var  uploadItem = response.getReturnValue();
                cmp.set("v.fileName", uploadItem.File_Name__c);
                cmp.set("v.fileType", uploadItem.File_Type__c);
            }
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
            	var errors = response.getError();
                if (errors) {
                	if (errors[0] && errors[0].message) {
                    	console.log("Error message: " +errors[0].message);
                    }
                }
                else {
                	console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
	}
})