({
   
	getQueryItems : function(cmp) 
    {
        var action = cmp.get('c.getMyPorjectQuery');
        console.log('action data: '+action);
        action.setParams({ recordId : cmp.get("v.recordId") });
		action.setCallback(this, function(response) {
        	var state = response.getState();
            if (state === "SUCCESS") 
            {
                var uploadQueryItems = response.getReturnValue();
                console.log('data received: '+uploadQueryItems);
                if((uploadQueryItems != null && uploadQueryItems.length>0)){
                    
                    for(var i=0;i<uploadQueryItems.length;i++){
                        console.log("status value received "+uploadQueryItems[i].Accept_Reject__c+" Id : "+uploadQueryItems[i].Id);
                        if(uploadQueryItems[i].Accept_Reject__c == "Accept"){
                            uploadQueryItems[i].background = '#DFF2BF';
        					
                        }
                        else if(uploadQueryItems[i].Accept_Reject__c == "Reject"){
                            uploadQueryItems[i].background = '#FFBABA';
        				}
                    }
                    
                    cmp.set("v.queryItems", uploadQueryItems);
                    cmp.set("v.isQueryItemExists", true);
                }
                else{
                  	cmp.set("v.isQueryItemExists", false);
                }   
            }
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
            	var errors = response.getError();
                if (errors) {
                	if (errors[0] && errors[0].message) {
                    	console.log("Error message: " +errors[0].message);
                    }
                }
                else {
                	console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
	},
    
    updateStatusHandler : function(comp,event){
        
        var pressedButtonName = event.currentTarget.name;
        console.log('pressed : '+pressedButtonName);
        var itemId = event.currentTarget.id;
        console.log('pressed item id : '+itemId);
        
        if(pressedButtonName=='accept'){
            var cmpTarget = comp.find(itemId);
            console.log('***comp: '+cmpTarget);
        	$A.util.addClass(cmpTarget, 'slds-theme--success');
            //itemId.background = '#DFF2BF';
            comp.set("v.status","Accept");
            comp.set("v.itemRecordId",itemId);
            comp.set("v.isOpen", true);
        }
        else if(pressedButtonName=='reject'){
            var cmpTarget = comp.find(itemId);
        	$A.util.addClass(cmpTarget, 'slds-theme--error');
            //itemId.background = '#FFBABA';
            comp.set("v.status","Reject");
            comp.set("v.itemRecordId",itemId);
            comp.set("v.isOpen", true);
        }
        else if(pressedButtonName=='Edit'){
            comp.set("v.status","Save");
            comp.set("v.itemRecordId",itemId);
            comp.set("v.flagOpen", true);
        }
	},
    
    editAndSaveComments : function(comp)
    {
        //**** update comment value ******
         var itemId = comp.get("v.itemRecordId");
        console.log('comments section pressed item id : '+itemId); 
        var action = comp.get('c.updateComments');
        console.log('action data for comment: '+action);
        var inputCmp = comp.find("commentSaveAndEdit");
        var commentValue = inputCmp.get("v.value");
        console.log('***'+commentValue);
        action.setParams({ recordId : itemId, updatedCommentValue : commentValue });
        action.setCallback(this, function(response) {
        	var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") 
            {
                console.log('success');  
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                      "title": "Success!",
                 	  "type" : "success",
                      "message": "Thank you"
                      });
               	toastEvent.fire();
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
            else if (state === "ERROR") {
            	console.log('Error');
            }
        });
        $A.enqueueAction(action);
        //****************
    },
    
    saveComments : function(comp){
        
        //******* update accept reject status ******
        var itemId = comp.get("v.itemRecordId");
        console.log('comments section pressed item id : '+itemId); 
        var statusVal = comp.get("v.status");
        console.log('status value : '+statusVal); 
        var action = comp.get('c.updateAcceptRejectStatus');
        console.log('action data: '+action);
        action.setParams({ recordId : itemId , updatedValue : statusVal });
		action.setCallback(this, function(response) {
        	var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") 
            {
                console.log('success');
                
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
            else if (state === "ERROR") {
            	console.log('Error');
            }
        });
        $A.enqueueAction(action);
        //***************
        
        //**** update comment value ******
        var action = comp.get('c.updateComments');
        console.log('action data for comment: '+action);
        var inputCmp = comp.find("comments");
        var commentValue = inputCmp.get("v.value");
        console.log('***'+commentValue);
        action.setParams({ recordId : itemId, updatedCommentValue : commentValue });
        action.setCallback(this, function(response) {
        	var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") 
            {
                console.log('success');  
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                      "title": "Success!",
                      "type" : "success",
                      "message": "Thank you"
                      });
               	toastEvent.fire();
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
            else if (state === "ERROR") {
            	console.log('Error');
            }
        });
        $A.enqueueAction(action);
        //****************
    }

})