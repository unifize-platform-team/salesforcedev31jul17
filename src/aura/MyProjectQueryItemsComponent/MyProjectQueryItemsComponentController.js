({
	doInit : function(component, event, helper) {
		helper.getQueryItems(component);
	},
    
    updateStatus : function(component, event, helper){
        helper.updateStatusHandler(component,event)
    },
 
   closeModel: function(component, event, helper) {
      component.set("v.isOpen", false);
      component.set("v.flagOpen", false);
   },
  	SaveClose: function(component, event, helper) {
      helper.saveComments(component);
      component.set("v.isOpen", false);
   },
    
   commentDirectlyForAcceptOrReject : function(component, event, helper) {
      component.set("v.isOpen", true);
      helper.saveComments(component);
      component.set("v.isOpen", false);
   },
    
    saveAndEdit : function(component, event, helper) {
      helper.editAndSaveComments(component);
      component.set("v.flagOpen", false);
   },
    
    commentDirectlyForQuestionResponce : function(component, event, helper) {
      component.set("v.flagOpen", true);
      helper.editAndSaveComments(component);
      component.set("v.flagOpen", false);
   }
})