({
	openDownloadPage : function(component, event, helper){
        var recordId = component.get("v.recordId");
        window.open("/apex/S3FilesDownloadComp?Id="+recordId);
	}
})