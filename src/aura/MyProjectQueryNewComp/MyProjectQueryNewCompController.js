({
	createRecord : function (component, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "myProject_Query_Item__c"
        });
        createRecordEvent.fire();
	}
})