({
	setInitValues : function(component,event) {
        console.log("**Entered Init**");
        
        //Extract Email Id
        var action = component.get('c.getEmailId');
        console.log('action details for email id: '+action);
        action.setParams({ recordId : component.get("v.recordId") });
        action.setCallback(this, function(response) {
        	var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") 
            {
                var emailId = response.getReturnValue();
                console.log('data received: '+emailId);
                if((emailId != null)){
                    component.set("v.emailId", emailId);
                    console.log('Email Id Successfully loaded');
                }
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
            else if (state === "ERROR") {
            	console.log('Error');
            }
        });
        $A.enqueueAction(action);
        
        //Extract Email Templates
        var action = component.get('c.getEmailTemplate');
        console.log('action details for email Template: '+action);
        action.setCallback(this, function(response) {
        	var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") 
            {
                var emailTemp = response.getReturnValue();
                console.log('data received for email Temp: '+emailTemp);
                if((emailTemp != null && emailTemp.length>0)){
                    component.set("v.emailTemplates", emailTemp);
                    console.log('Email Templates Successfully loaded');
                }
            }
            else if (state === "INCOMPLETE") {
                console.log('Incomplete');
            }
            else if (state === "ERROR") {
            	console.log('Error');
            }
        });
        $A.enqueueAction(action);
	},
    onChangeFunctionHelper : function(component,event){
        var listOfTemplates = component.get("v.emailTemplates");
        for(var i=0;i<listOfTemplates.length;i++){
            if(event.getSource().get("v.value")==listOfTemplates[i].Name){
                component.set("v.templateBody",listOfTemplates[i].Body);
                component.set("v.templateSubject",listOfTemplates[i].Subject);
                component.set("v.isDisplay",true);
                break;
            }
        }
    },
    sendMailHelper : function(component,event){
        var to = component.get("v.emailId");
        var sub = component.get("v.templateSubject");
        var body = component.get("v.templateBody");
        console.log(" "+to+" "+sub+" "+body);
        var action = component.get("c.sendMailMethod"); 
        action.setParams({
            'mMail': to,
            'mSubject': sub,
            'mbody': body
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();	
				var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                      "title": "Success!",
                 	  "type" : "success",
                      "message": "Email Sent"
                      });
               	toastEvent.fire();
            }
 
        });
        $A.enqueueAction(action);
    },
})