({
	doInit : function(component, event, helper) {
        helper.setInitValues(component, event);
	},
    onChangeFunction : function(component,event,helper){
       console.log(event.getSource().get("v.value"));    
       helper.onChangeFunctionHelper(component,event);
        
    },
    sendMail : function(component,event,helper){
        helper.sendMailHelper(component,event);
    },
})