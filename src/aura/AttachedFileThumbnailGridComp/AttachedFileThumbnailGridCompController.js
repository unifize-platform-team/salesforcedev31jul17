({
	doInit : function(component, event, helper) {
        component.set("v.isListView", false);
        var recordId = component.get("v.recordId");
        
        if(recordId != null && recordId != ''){
            helper.getUploadItems(component);
        }
	},
    
    
    switchView : function(component, event, helper){
    	var isListView = component.get("v.isListView");
        if(isListView == true){
        	isListView = false;
    	}
        else{
        	isListView = true; 
        }
 		component.set("v.isListView", isListView);
	},
    
    
    deleteClicked : function(component, event, helper){
        helper.deleteAttachedFiles(component, event);
    },
    
    
    deleteSelectedAttachedFiles : function(component, event, helper){
        helper.deleteSelectedAttachedFiles(component, event);
    },
    
    
    downloadSelectedFiles : function(component, event, helper){
        helper.downloadSelectedFiles(component, event);
    }
})