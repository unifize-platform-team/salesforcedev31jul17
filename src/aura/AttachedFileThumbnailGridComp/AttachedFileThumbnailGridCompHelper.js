({
	getUploadItems : function(cmp) {
		var action = cmp.get("c.getUploadItems");
        action.setParams({ recordId : cmp.get("v.recordId") });
        console.log('Upload Items');
        action.setCallback(this, function(response) {
        	var state = response.getState();
            console.log(response.getReturnValue());
            if (state === "SUCCESS") {
                var uploadItems = response.getReturnValue();
                
                for(var i=0; i<uploadItems.length; i++){
                    uploadItems[i].isSelected = false;
                }
                
                cmp.set("v.uploadItems", uploadItems);
            }
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
            	var errors = response.getError();
                if (errors) {
                	if (errors[0] && errors[0].message) {
                    	console.log("Error message: " +errors[0].message);
                    }
                }
                else {
                	console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
	},
    
    
    deleteAttachedFiles : function(cmp, event){
        var proceedToDelete = confirm("Are you sure you want to delete!");
        if(proceedToDelete == true){
            
            var action = cmp.get("c.deleteAttachedFiles");
            
            var uploadItems = cmp.get("v.uploadItems");
            var uploadItemId = event.currentTarget.id;
            
            var index = 0;
            var foundItem = false;
            
            var uploadItemIds = [];
            uploadItemIds.push(uploadItemId);
            
            action.setParams({attachedFilesIdSet : uploadItemIds});
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log(state);
                if (state === "SUCCESS") {
                   var message = response.getReturnValue();
                    if(message == 'Success'){
                        for(var i=0; i<uploadItems.length; i++){
                            if(uploadItems[i].Id == uploadItemId){
                                foundItem = true;
                                break;
                            }
                            index++;
                        }
                        
                        console.log(index);
                        console.log(foundItem);
                        
                        if(foundItem == true){
                            uploadItems.splice(index, 1);
                        }
                        cmp.set("v.uploadItems", uploadItems);
                    }
                    else{
                        alert(message);
                    }
                }
                else if (state === "INCOMPLETE") {
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    console.log(response);
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            alert("Error message: " +errors[0].message);
                        }
                    }
                    else {
                        alert("Unknown error occured. Please contact your system administrator");
                    }
                }
            });
            
            $A.enqueueAction(action);  
        }
    },
    
    
    deleteSelectedAttachedFiles : function(cmp, event){
        var proceedToDelete = confirm("Are you sure you want to delete!");
        if(proceedToDelete == true){
        	var action = cmp.get("c.deleteAttachedFiles");
            var uploadItems = cmp.get("v.uploadItems");
            
            var uploadItemIds = [];
            for(var i=0; i<uploadItems.length; i++){
                if(uploadItems[i].isSelected == true){
                    uploadItemIds.push(uploadItems[i].Id);
                }
            }
            
            if(uploadItemIds.length > 0){
                action.setParams({attachedFilesIdSet : uploadItemIds});
                
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    console.log(state);
                    if (state === "SUCCESS") {
                        var message = response.getReturnValue();
                        if(message == 'Success'){
                            
                            var indexList = [];
                            
                            for(var i=0; i<uploadItems.length; i++){
                                for(var j=0; j<uploadItemIds.length; j++){
                                    if(uploadItems[i].Id == uploadItemIds[j]){
                                        indexList.push(i);
                                    }
                                }
                            }
                            
                            indexList.sort();
                            
                            for(var i=indexList.length-1; i>=0; i--){
                                uploadItems.splice(indexList[i], 1);
                            }
                            cmp.set("v.uploadItems", uploadItems);
                        }
                        else{
                            alert(message);
                        }
                    }               
                    else if (state === "INCOMPLETE") {
                    }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        console.log(response);
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                alert("Error message: " +errors[0].message);
                            }
                        }
                        else {
                            alert("Unknown error occured. Please contact your system administrator");
                        }
                    }
                });
                
                $A.enqueueAction(action);
            }
            else{
                alert('You have not selected any files for delete');
            }
        }
    },
    
    
    downloadSelectedFiles : function(cmp, event){
        var uploadItems = cmp.get("v.uploadItems");
        var idString = '';
        var count = 0;
        
        for(var i=0; i<uploadItems.length; i++){
            if(uploadItems[i].isSelected == true){
                if(idString == ''){
                    idString = uploadItems[i].Id;
                }
                else{
                    idString += ','+uploadItems[i].Id;
                }
                count++;
            }
        }
        if(count > 0){    
            window.open("/apex/S3FilesDownloadComp?Ids="+idString);
        }
        else{
            alert('You have not selected any files for download');
        }    
    }
})