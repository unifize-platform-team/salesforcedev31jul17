({
	insertItemRecord : function(component,event) {
        console.log('entered helper');
		component.set("v.isOpen", true);
	},
    saveRecord : function(component,event) {
        console.log('entered save helper');
		var action = cmp.get('c.createQueryItem');
        console.log('action data: '+action);
        action.setParams({ itemId : cmp.get("v.recordId") });
		action.setCallback(this, function(response) {
        	var state = response.getState();
            if (state === "SUCCESS") 
            {
                window.location.reload();
                console.log('SUCCESS');
            }
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
            	var errors = response.getError();
                if (errors) {
                	if (errors[0] && errors[0].message) {
                    	console.log("Error message: " +errors[0].message);
                    }
                }
                else {
                	console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
	},
    
})