({
	getPicklistValues: function(component) {
        console.log(component.get("v.recordId"));
        console.log(component.get("v.pickListField"));
     return new Promise($A.getCallback(function(resolve, reject) {
      var action = component.get("c.getStatus");
      action.setParams( {
        "recordId": component.get("v.recordId"),
        "pickListField": component.get("v.pickListField")
      });

      action.setCallback(this, function(a) {
       console.log(a.getReturnValue());
          var rw=a.getReturnValue();
          console.log(JSON.parse(rw.options));

          var picArr = JSON.parse(rw.options);
            console.log(picArr);
      var picArrLen = picArr.length;
//Current Record Picklist Value
      var CurrentStg = rw.currentStatus;
          console.log(CurrentStg);
//Checking The Current Picklist Value Against All The Picklist Values
      var CurrentStgPos = picArr.indexOf(CurrentStg);
      var theiLis = '';
   
//Creating the Sale Path Content based on the Record's Picklist Value 
      for(var i=0; i<picArrLen; i++){
  
// HTML Content For Completed Stages           
          if(CurrentStgPos > i){
               theiLis += '<li class="slds-tabs--path__item slds-is-complete" role="presentation">'+
        '<a class="slds-tabs--path__link" id="tabs-path-1" aria-controls="content-path-1" aria-selected="false" tabindex="-1" role="tab" href="#" aria-live="assertive">'+
          '<span class="slds-tabs--path__stage">'+
            '<svg aria-hidden="true" class="slds-icon slds-icon--x-small"><use xlink:href="/resource/1494225514000/LDSSP/assets/icons/utility-sprite/svg/symbols.svg#check"></use></svg></span>'+
            '<span class="slds-tabs--path__title">'+picArr[i]+'</span></a></li>' 
                
          }
// HTML Content For Current Stage  
            else if(CurrentStgPos == i){
// HTML Content If Current Stage is Last Stage             
              if(i == picArrLen-1){
                  theiLis += '<li class="slds-tabs--path__item slds-is-complete" role="presentation">'+
        '<a class="slds-tabs--path__link" id="tabs-path-1" aria-controls="content-path-1" aria-selected="false" tabindex="-1" role="tab" href="#" aria-live="assertive">'+
          '<span class="slds-tabs--path__stage">'+
            '<svg aria-hidden="true" class="slds-icon slds-icon--x-small"><use xlink:href="/resource/1494225514000/LDSSP/assets/icons/utility-sprite/svg/symbols.svg#check"></use></svg></span>'+
            '<span class="slds-tabs--path__title">'+picArr[i]+'</span></a></li>' 
              } 
// HTML Content If Current Stage is Not Last Stage
              else{
                  
                  theiLis += '<li class="slds-tabs--path__item slds-is-current" role="presentation">'+
        '<a class="slds-tabs--path__link" id="tabs-path-1" aria-controls="content-path-1" aria-selected="false" tabindex="-1" role="tab" href="#" aria-live="assertive">'+
          '<span class="slds-tabs--path__stage">'+
            '<svg aria-hidden="true" class="slds-icon slds-icon--x-small"><use xlink:href="/resource/1494225514000/LDSSP/assets/icons/utility-sprite/svg/symbols.svg#check"></use></svg></span>'+
            '<span class="slds-tabs--path__title">'+picArr[i]+'</span></a></li>' 
                  
              }
              
          }
// HTML Content For Next Stages
             else if(CurrentStgPos < i){
              
          theiLis += '<li class="slds-tabs--path__item slds-is-incomplete" role="presentation">'+
              '<a class="slds-tabs--path__link" id="tabs-path-1" aria-controls="content-path-1" aria-selected="false" tabindex="-1" role="tab" href="#" aria-live="assertive">'+
          '<span class="slds-tabs--path__stage">'+
            '<svg aria-hidden="true" class="slds-icon slds-icon--x-small"><use xlink:href="/resource/1494225514000/LDSSP/assets/icons/utility-sprite/svg/symbols.svg#check"></use></svg></span>'+
            '<span class="slds-tabs--path__title">'+picArr[i]+'</span></a></li>' 
            }
      }
        
        
      
//Adding The Created Sales Path Content To Sales Path Body  
 component.set("v.htmlstring",theiLis);  
   //document.getElementById('TheLis').innerHTML=(theiLis);
          
      });

      $A.enqueueAction(action);
     }));
    }
})