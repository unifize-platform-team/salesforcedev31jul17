({
	onSubmit : function(input) {
		return new Promise($A.getCallback(function(resolve, reject) {
    		var action = component.get("c.onSubmit");
            /*action.setParams({
                apexInput: input
            });*/

            action.setCallback(this, function(a) {
                resolve(a.getReturnValue());
            });
            $A.enqueueAction(action);
		}));
	}
})