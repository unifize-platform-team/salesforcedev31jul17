({
	getName : function(component, event, helper) {
        var getName = component.get("c.getProjectName");
        getName.setParams({
        "packId":component.get("v.parentRecord")
        });        
        getName.setCallback(this, function(res) { 
        //console.log(res.getReturnValue());           
            if (res.getState() === "SUCCESS" && res.getReturnValue()) {        
                component.set("v.packgName", res.getReturnValue());
                //console.log(res.getReturnValue());
            } 
            else if (res.getState() === "ERROR") {
                $A.log("Errors", res.getError());
                console.log(res);
            }           
        });  
        
        $A.enqueueAction(getName);             
	}
})