({
     doInit : function(component, event, helper) {  
        ////console.log('--new--'+component.get("v.parentRecord"));
            return new Promise($A.getCallback(function(resolve, reject) {
                var metadataAction = component.get("c.getPickListOptions");
                
                metadataAction.setCallback(this, function(res) {            
                    if (res.getState() === "SUCCESS" && res.getReturnValue()) {        
                        component.set("v.picklistoptions", JSON.parse(res.getReturnValue()));
                        //console.log(res.getReturnValue());
                        helper.getName(component, event, helper);
                    } 
                    else if (res.getState() === "ERROR") {
                        $A.log("Errors", res.getError());
                        console.log(res);
                    }           
                });  
                
                $A.enqueueAction(metadataAction);             
            }));
            
            
        
    },
    cancelAction : function(component, event, helper) {
    	component.set("v.showDialog",false);
    	var cmpTarget=component.find('propertyErrorBox');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
    },
    saveAction : function(component, event, helper) {
    console.log('save');
    	var savePlacement = component.get("c.savePlacement");
        savePlacement.setParams({
        "packId":component.get("v.parentRecord"),
        "creativeUnit":component.get("v.value")
        });        
        savePlacement.setCallback(this, function(res) { 
        console.log(res);           
            if (res.getState() === "SUCCESS" && res.getReturnValue()) {        
                console.log('-save---'+res.getReturnValue());
                var result=res.getReturnValue();
                if(result.success==true){
                	 var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type" : "success",
                        "message": result.message
                    });
                    toastEvent.fire(); 
                    
                    $A.get('e.force:refreshView').fire();
                	component.set("v.showDialog",false);
                	var cmpTarget=component.find('propertyErrorBox');
                    $A.util.addClass(cmpTarget, 'slds-hide');
                    $A.util.removeClass(cmpTarget, 'slds-show');
                }else{
                	
                    component.set('v.errorMessage',result.message);
                    var cmpTarget=component.find('propertyErrorBox');
                    $A.util.addClass(cmpTarget, 'slds-show');
                    $A.util.removeClass(cmpTarget, 'slds-hide');
                    
                    //$A.get('e.force:refreshView').fire();
                	//component.set("v.showDialog",false);
                }
            } 
            else if (res.getState() === "ERROR") {
                $A.log("Errors", res.getError());
                console.log(res);
            }           
        });  
        
        $A.enqueueAction(savePlacement);
    }
})