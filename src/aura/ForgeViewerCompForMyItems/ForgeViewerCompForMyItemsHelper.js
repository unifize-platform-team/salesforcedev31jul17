({
    getAttachedFileDetails : function(cmp) {
		var action = cmp.get("c.getAttachedFileDetails");
        action.setParams({ recordId : cmp.get("v.recordId") });
        
        action.setCallback(this, function(response) {
        	var state = response.getState();
            
            if (state === "SUCCESS") {
                var  myItem = response.getReturnValue();
                if(myItem.Current_myItem_Revision__c != null && myItem.Current_myItem_Revision__r.Current_Attached_File__c != null){
                    cmp.set("v.attachedFileRecordId", myItem.Current_myItem_Revision__r.Current_Attached_File__c);
                }
            }
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
            	var errors = response.getError();
                if (errors) {
                	if (errors[0] && errors[0].message) {
                    	console.log("Error message: " +errors[0].message);
                    }
                }
                else {
                	console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
	}
})