({
    init: function(cmp) {
        var action = cmp.get("c.getImageDetails");
        action.setParams({ recordId : cmp.get("v.recordId") });
        
        action.setCallback(this, function(response) {
        	var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var  query = response.getReturnValue();
                console.log(query);
                if(query.After_Image__c!= null){
                    cmp.set("v.myImageVal", query.After_Image__c);
                }	
            }
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
            	var errors = response.getError();
                if (errors) {
                	if (errors[0] && errors[0].message) {
                    	console.log("Error message: " +errors[0].message);
                    }
                }
                else {
                	console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    }
})