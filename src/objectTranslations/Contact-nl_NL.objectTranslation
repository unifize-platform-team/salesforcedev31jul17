<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <help><!-- Indicate that the employee has provided their address proof original and copy --></help>
        <label><!-- Address Proof --></label>
        <name>Address_Proof__c</name>
    </fields>
    <fields>
        <label><!-- Alternate Email --></label>
        <name>Alternate_Email__c</name>
    </fields>
    <fields>
        <help><!-- Indicate that the employee and the company representative have signed the Appointment Letter --></help>
        <label><!-- Appointment Letter --></label>
        <name>Appointment_Letter__c</name>
    </fields>
    <fields>
        <label><!-- Attended Dreamforce --></label>
        <name>Attended_Dreamforce__c</name>
    </fields>
    <fields>
        <help><!-- Indicate that the employee has provided all their Bank Account Details --></help>
        <label><!-- Bank Account Info --></label>
        <name>Bank_Account_Info__c</name>
    </fields>
    <fields>
        <help><!-- Indicate the Bank Account Number for this employee --></help>
        <label><!-- Bank Account Number --></label>
        <name>Bank_Account_Number__c</name>
    </fields>
    <fields>
        <help><!-- Indicate the name of this employee&apos;s bank --></help>
        <label><!-- Bank Name --></label>
        <name>Bank_Name__c</name>
    </fields>
    <fields>
        <help><!-- Indicate whether the employee has had their Bookmarks Bar added for Inbox, Google Calendar, Google Drive, Whatsapp, Force.com --></help>
        <label><!-- Bookmarks Bar --></label>
        <name>Bookmarks_Bar__c</name>
    </fields>
    <fields>
        <help><!-- Indicate whether the employee has received their business cards --></help>
        <label><!-- Business Cards --></label>
        <name>Business_Cards__c</name>
    </fields>
    <fields>
        <help><!-- Indicate whether this User has been added to their applicable Chatter Groups --></help>
        <label><!-- Chatter Groups --></label>
        <name>Chatter_Groups__c</name>
    </fields>
    <fields>
        <help><!-- Indicate the current Appointment Letter for this Employee --></help>
        <label><!-- Current Appointment Letter --></label>
        <name>Current_Appointment_Letter__c</name>
        <relationshipLabel><!-- Contacts --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Indicates the Designation of this Employee as per the current Appointment Letter --></help>
        <label><!-- Designation --></label>
        <name>Designation__c</name>
    </fields>
    <fields>
        <label><!-- Dreamforce --></label>
        <name>Dreamforce__c</name>
        <picklistValues>
            <masterLabel>No</masterLabel>
            <translation><!-- No --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Yes</masterLabel>
            <translation><!-- Yes --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Indicate the ESI No. for this Employee, where applicable --></help>
        <label><!-- ESI No. --></label>
        <name>ESI_No__c</name>
    </fields>
    <fields>
        <help><!-- Indicate whether the Employee has had their email set up --></help>
        <label><!-- Email --></label>
        <name>Email__c</name>
    </fields>
    <fields>
        <label><!-- Employee No. --></label>
        <name>Employee_No__c</name>
    </fields>
    <fields>
        <help><!-- Indicate the the employee has provided their original Exam Qualification records --></help>
        <label><!-- Exam Certificates --></label>
        <name>Exam_Certificates__c</name>
    </fields>
    <fields>
        <help><!-- Returns the Fixed CTC for this Employee as per the Current Appointment Letter --></help>
        <label><!-- Fixed CTC --></label>
        <name>Fixed_CTC__c</name>
    </fields>
    <fields>
        <help><!-- Indicate that the employee has been provided with Force.com login credentials --></help>
        <label><!-- Force.com Account --></label>
        <name>Force_com_Account__c</name>
    </fields>
    <fields>
        <help><!-- Indicate whether the employee has had their Google Calendars set up --></help>
        <label><!-- Google Calendars --></label>
        <name>Google_Calendars__c</name>
    </fields>
    <fields>
        <help><!-- Indicate the Google Drive Folder for storing employee documents --></help>
        <label><!-- Google Drive Employee Folder --></label>
        <name>Google_Drive_Employee_Folder__c</name>
    </fields>
    <fields>
        <help><!-- Indicate whether the employee has had Google Drive setup and explained to them --></help>
        <label><!-- Google Drive --></label>
        <name>Google_Drive__c</name>
    </fields>
    <fields>
        <help><!-- Indicate that the employee has been added to the applicable Google Groups --></help>
        <label><!-- Google Groups --></label>
        <name>Google_Groups__c</name>
    </fields>
    <fields>
        <help><!-- Indicate whether Holiday Events have been added for this Employee --></help>
        <label><!-- Holidays --></label>
        <name>Holidays__c</name>
    </fields>
    <fields>
        <help><!-- Indicate the IFSC Code for this Employee&apos;s Bank --></help>
        <label><!-- IFSC Code --></label>
        <name>IFSC_Code__c</name>
    </fields>
    <fields>
        <help><!-- Indicate that the employee has been sent an introductory email to their official ID with attachments --></help>
        <label><!-- Intro Email &amp; Attachments --></label>
        <name>Intro_Email_Attachments__c</name>
    </fields>
    <fields>
        <help><!-- Indicate whether the employee has been equipped with a Laptop or Desktop --></help>
        <label><!-- Laptop/Desktop --></label>
        <name>Laptop_Desktop__c</name>
    </fields>
    <fields>
        <help><!-- Indicate that the employee has provided a Medical Certificate --></help>
        <label><!-- Medical Certificate --></label>
        <name>Medical_Certificate__c</name>
    </fields>
    <fields>
        <help><!-- Indicate that the employee has provided the original and copy of their PAN Card --></help>
        <label><!-- PAN Card --></label>
        <name>PAN_Card__c</name>
    </fields>
    <fields>
        <label><!-- PAN Number --></label>
        <name>PAN_Number__c</name>
    </fields>
    <fields>
        <help><!-- Indicate the PF Account No. for this Employee --></help>
        <label><!-- PF Account No. --></label>
        <name>PF_Account_No__c</name>
    </fields>
    <fields>
        <help><!-- Indicate that the employee has provided their passport copy --></help>
        <label><!-- Passport Copy --></label>
        <name>Passport_Copy__c</name>
    </fields>
    <fields>
        <help><!-- Indicate this contact&apos;s primary language --></help>
        <label><!-- Primary Language --></label>
        <name>Primary_Language__c</name>
        <picklistValues>
            <masterLabel>English</masterLabel>
            <translation><!-- English --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Hindi</masterLabel>
            <translation><!-- Hindi --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Kannada</masterLabel>
            <translation><!-- Kannada --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Malayalam</masterLabel>
            <translation><!-- Malayalam --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Tamil</masterLabel>
            <translation><!-- Tamil --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Telugu</masterLabel>
            <translation><!-- Telugu --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Referred By --></label>
        <name>Referred_By__c</name>
    </fields>
    <fields>
        <help><!-- Indicate whether the employee has provided their relieving letter from their previous employer --></help>
        <label><!-- Relieving Letter --></label>
        <name>Relieving_Letter__c</name>
    </fields>
    <fields>
        <help><!-- Indicate that the employee has provided the latest salary slip from their last employer --></help>
        <label><!-- Salary Certificate --></label>
        <name>Salary_Certificate__c</name>
    </fields>
    <fields>
        <help><!-- Indicate whether the Employee has synchronised their Google Calendar with Salesforce1 --></help>
        <label><!-- Salesforce1 Event Synch --></label>
        <name>Salesforce1_Event_Synch__c</name>
    </fields>
    <fields>
        <help><!-- Indicate whether the Employee has downloaded and logged into Salesforce1 --></help>
        <label><!-- Salesforce1 --></label>
        <name>Salesforce1__c</name>
    </fields>
    <fields>
        <help><!-- Indicate whether the employee has received their shirts --></help>
        <label><!-- Shirts --></label>
        <name>Shirts__c</name>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>Status__c</name>
        <picklistValues>
            <masterLabel>Active</masterLabel>
            <translation><!-- Active --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Archive</masterLabel>
            <translation><!-- Archive --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Confirmed</masterLabel>
            <translation><!-- Confirmed --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Contract</masterLabel>
            <translation><!-- Contract --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Ex-Employee</masterLabel>
            <translation><!-- Ex-Employee --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Onboarding</masterLabel>
            <translation><!-- Onboarding --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Probationary</masterLabel>
            <translation><!-- Probationary --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- SurveyReminder --></label>
        <name>TIMBASURVEYS__SurveyReminder__c</name>
    </fields>
    <fields>
        <label><!-- Survey --></label>
        <name>TIMBASURVEYS__Survey__c</name>
        <relationshipLabel><!-- Contacts --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- reminderStringSize --></label>
        <name>TIMBASURVEYS__reminderStringSize__c</name>
    </fields>
    <fields>
        <help><!-- Indicate that the Employee has posted to Salesforce1 in Unifize Global --></help>
        <label><!-- Unifize Global Post --></label>
        <name>Unifize_Global_Post__c</name>
    </fields>
    <fields>
        <help><!-- Returns the Variable CTC for this Employee as per the Current Appointment Letter --></help>
        <label><!-- Variable CTC --></label>
        <name>Variable_CTC__c</name>
    </fields>
    <fields>
        <help><!-- Indicate that the employee has been added to the applicable Whatsapp Groups --></help>
        <label><!-- Whatsapp Groups --></label>
        <name>Whatsapp_Groups__c</name>
    </fields>
    <fields>
        <help><!-- Indicate that the employee has provided their 10th Standard marks card --></help>
        <label><!-- 10th Standard Marks Card --></label>
        <name>X10th_Standard_Marks_Card__c</name>
    </fields>
    <fields>
        <help><!-- Indicate whether the employee has submitted 4 x passport photos --></help>
        <label><!-- 4 x Passport Photos --></label>
        <name>X4_x_Passport_Photos__c</name>
    </fields>
    <fields>
        <help><!-- Indicate whether this Contact should receive myProject Confirmation emails --></help>
        <label><!-- myProject Confirmation Emails --></label>
        <name>myProject_Confirmation_Emails__c</name>
    </fields>
    <layouts>
        <layout>Contact Layout</layout>
        <sections>
            <label><!-- Automation --></label>
            <section>Automation</section>
        </sections>
        <sections>
            <label><!-- Primary Contact Details --></label>
            <section>Primary Contact Details</section>
        </sections>
    </layouts>
    <layouts>
        <layout>Employee Layout</layout>
        <sections>
            <label><!-- Automation --></label>
            <section>Automation</section>
        </sections>
        <sections>
            <label><!-- Bank Details --></label>
            <section>Bank Details</section>
        </sections>
        <sections>
            <label><!-- Current Appointment Letter --></label>
            <section>Current Appointment Letter</section>
        </sections>
        <sections>
            <label><!-- Employee Onboarding --></label>
            <section>Employee Onboarding</section>
        </sections>
        <sections>
            <label><!-- Primary Contact Details --></label>
            <section>Primary Contact Details</section>
        </sections>
        <sections>
            <label><!-- Required Documents --></label>
            <section>Required Documents</section>
        </sections>
        <sections>
            <label><!-- Statutory Details --></label>
            <section>Statutory Details</section>
        </sections>
    </layouts>
    <quickActions>
        <label><!-- Attended DF --></label>
        <name>Attended_DF</name>
    </quickActions>
    <recordTypes>
        <label><!-- Contact --></label>
        <name>Contact</name>
    </recordTypes>
    <recordTypes>
        <label><!-- Employee --></label>
        <name>Employee</name>
    </recordTypes>
    <validationRules>
        <errorMessage><!-- If this is an Employee Record type, the Account Name must be Unifize --></errorMessage>
        <name>AccountIsUnifizeEmployees</name>
    </validationRules>
    <webLinks>
        <label><!-- GoogleMaps --></label>
        <name>GoogleMaps</name>
    </webLinks>
    <webLinks>
        <label><!-- GoogleSearch --></label>
        <name>GoogleSearch</name>
    </webLinks>
    <webLinks>
        <label><!-- SendGmail --></label>
        <name>SendGmail</name>
    </webLinks>
    <webLinks>
        <label><!-- Send_Survey_Contact_Detail --></label>
        <name>TIMBASURVEYS__Send_Survey_Contact_Detail</name>
    </webLinks>
    <webLinks>
        <label><!-- Send_Survey_To_Selected --></label>
        <name>TIMBASURVEYS__Send_Survey_To_Selected</name>
    </webLinks>
    <webLinks>
        <label><!-- send_survey --></label>
        <name>TIMBASURVEYS__send_survey</name>
    </webLinks>
    <webLinks>
        <label><!-- YahooMaps --></label>
        <name>YahooMaps</name>
    </webLinks>
    <webLinks>
        <label><!-- YahooWeather --></label>
        <name>YahooWeather</name>
    </webLinks>
    <webLinks>
        <label><!-- Send_with_DocuSign --></label>
        <name>dsfs__Send_with_DocuSign</name>
    </webLinks>
</CustomObjectTranslation>
