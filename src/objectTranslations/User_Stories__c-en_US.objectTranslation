<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>User Story</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>User Stories</value>
    </caseValues>
    <fields>
        <help><!-- Indicate details of any Access Credentials required to complete this Task --></help>
        <label><!-- Access Credentials --></label>
        <name>Access_Credentials__c</name>
    </fields>
    <fields>
        <help><!-- Indicate the Actual Beta Release Date --></help>
        <label><!-- Actual Beta Release --></label>
        <name>Actual_Beta_Release__c</name>
    </fields>
    <fields>
        <help><!-- Indicate the Actual Production Release Date for this User Story --></help>
        <label><!-- Actual Production Release --></label>
        <name>Actual_Production_Release__c</name>
    </fields>
    <fields>
        <help><!-- Indicate the Agreed Hours for this Project --></help>
        <label><!-- Agreed Hours --></label>
        <name>Agreed_Hours__c</name>
    </fields>
    <fields>
        <label><!-- Assigned To Email --></label>
        <name>Assigned_To_Email__c</name>
    </fields>
    <fields>
        <help><!-- Enter the description of the functionality where this user story is useful --></help>
        <label><!-- Business Justification --></label>
        <name>Business_Justification__c</name>
    </fields>
    <fields>
        <label><!-- Contact --></label>
        <name>Contact__c</name>
        <relationshipLabel><!-- User Stories --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Returns the Cost Per Hour of this User Story --></help>
        <label><!-- Cost Per Hour --></label>
        <name>Cost_Per_Hour__c</name>
    </fields>
    <fields>
        <help><!-- Total Cost for this User Story.  This is the Fixed Cost + (Cost Per Hour x Agreed Hours) --></help>
        <label><!-- Total Cost --></label>
        <name>Cost__c</name>
    </fields>
    <fields>
        <help><!-- Indicate any other User Stories that may be affected by this Story --></help>
        <label><!-- Dependent / Affected Stories --></label>
        <name>Dependent_Affected_Stories__c</name>
    </fields>
    <fields>
        <help><!-- Enter the developer who will be working on technical details for this user story --></help>
        <label><!-- Assigned To --></label>
        <name>Developer_Assigned__c</name>
        <relationshipLabel><!-- User Stories --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Indicate any functional entities, fields and the relationships of the entities for this User Story --></help>
        <label><!-- Entities &amp; Relationships --></label>
        <name>Entities_Relationships__c</name>
    </fields>
    <fields>
        <help><!-- Indicate the Epic with which this User Story is associated --></help>
        <label><!-- Epic --></label>
        <name>Epic__c</name>
        <relationshipLabel><!-- User Stories --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Number of hours spent on this user story. This will be the total of hours spent on each task. --></help>
        <label><!-- Estimated Hours --></label>
        <name>Estimation__c</name>
    </fields>
    <fields>
        <help><!-- Indicate the Fixed Cost for this User Story --></help>
        <label><!-- Fixed Cost --></label>
        <name>Fixed_Cost__c</name>
    </fields>
    <fields>
        <help><!-- Enter detailed functional description of the user story --></help>
        <label><!-- Functional Description --></label>
        <name>Functional_Description__c</name>
    </fields>
    <fields>
        <help><!-- Enter the Goal for this User Story --></help>
        <label><!-- Goal --></label>
        <name>Goal__c</name>
    </fields>
    <fields>
        <help><!-- Insert the Google Drive URL for this User Story --></help>
        <label><!-- Google Drive URL --></label>
        <name>Google_Drive_URL__c</name>
    </fields>
    <fields>
        <label><!-- Pending Defects --></label>
        <name>No_of_Pending_Defects__c</name>
    </fields>
    <fields>
        <label><!-- Total Defects --></label>
        <name>No_of_Total_Defect__c</name>
    </fields>
    <fields>
        <help><!-- Indicate the Beta Release Delivery Date for this User Story --></help>
        <label><!-- Planned Beta Release --></label>
        <name>Planned_Beta_Release__c</name>
    </fields>
    <fields>
        <help><!-- Indicate the Planned Production Release Date for this User Story --></help>
        <label><!-- Planned Production Release --></label>
        <name>Planned_Production_Release__c</name>
    </fields>
    <fields>
        <label><!-- Preceding Story Delivery Date --></label>
        <name>Preceding_Story_Delivery_Date__c</name>
    </fields>
    <fields>
        <help><!-- Indicate any User Story that precedes this one --></help>
        <label><!-- Preceding User Story --></label>
        <name>Preceding_User_Story__c</name>
        <relationshipLabel><!-- User Stories --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Enter the preference of this user story --></help>
        <label><!-- Priority --></label>
        <name>Priority__c</name>
        <picklistValues>
            <masterLabel>High</masterLabel>
            <translation><!-- High --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Low</masterLabel>
            <translation><!-- Low --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Medium</masterLabel>
            <translation><!-- Medium --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- If any prototyping is required for this User Story, indicate details here.  These should be treated as the first / highest priority for any associated Agile Tasks --></help>
        <label><!-- Prototype Requirements (if any) --></label>
        <name>Prototype_Requirements_if_any__c</name>
    </fields>
    <fields>
        <help><!-- Indicate any reference User Stories or other Resources for completing this User Story --></help>
        <label><!-- References / Resources --></label>
        <name>References__c</name>
    </fields>
    <fields>
        <help><!-- Enter the release to which this user story belongs to --></help>
        <label><!-- Release --></label>
        <name>Release__c</name>
        <relationshipLabel><!-- User Stories --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Enter the sprint to which this user story belongs to --></help>
        <label><!-- Sprint --></label>
        <name>Sprint__c</name>
        <relationshipLabel><!-- User Stories --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Enter the status of user story, --></help>
        <label><!-- Status --></label>
        <name>Status__c</name>
        <picklistValues>
            <masterLabel>Analysis</masterLabel>
            <translation><!-- Analysis --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Beta Release</masterLabel>
            <translation><!-- Beta Release --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Cancelled</masterLabel>
            <translation><!-- Cancelled --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Closed</masterLabel>
            <translation><!-- Closed --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Dependent on Preceding Story</masterLabel>
            <translation><!-- Dependent on Preceding Story --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Development</masterLabel>
            <translation><!-- Development --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Estimation</masterLabel>
            <translation><!-- Estimation --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>On Hold</masterLabel>
            <translation><!-- On Hold --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Production Release</masterLabel>
            <translation><!-- Production Release --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>User Story Detailing</masterLabel>
            <translation><!-- User Story Detailing --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Indicate any techncial design specifications or requirements, including inputs and third party integrations --></help>
        <label><!-- Technical Specs --></label>
        <name>Technical_Specs__c</name>
    </fields>
    <fields>
        <help><!-- Indicate any User Interface requirements for this User Story, including drawings / photos or wireframes as may be applicable.  If you require the standard User Interface, please specify this here. --></help>
        <label><!-- User Interface --></label>
        <name>User_Interface__c</name>
    </fields>
    <fields>
        <help><!-- Indicate the descriptive name of this User Story --></help>
        <label><!-- User Story Name --></label>
        <name>User_Story_Name__c</name>
    </fields>
    <fields>
        <help><!-- Indicate the Skill Type required for this User Story --></help>
        <label><!-- Skill Type --></label>
        <name>User_Story_Type__c</name>
        <picklistValues>
            <masterLabel>Force.com &amp; 3rd Party</masterLabel>
            <translation><!-- Force.com &amp; 3rd Party --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Force.com Admin</masterLabel>
            <translation><!-- Force.com Admin --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Force.com Developer</masterLabel>
            <translation><!-- Force.com Developer --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Standalone 3rd Party</masterLabel>
            <translation><!-- Standalone 3rd Party --></translation>
        </picklistValues>
    </fields>
    <layouts>
        <layout>User Stories Layout</layout>
        <sections>
            <label><!-- Cost &amp; Delivery --></label>
            <section>Cost &amp; Delivery</section>
        </sections>
        <sections>
            <label><!-- Custom Links --></label>
            <section>Custom Links</section>
        </sections>
        <sections>
            <label><!-- Defects --></label>
            <section>Defects</section>
        </sections>
        <sections>
            <label><!-- Description --></label>
            <section>Description</section>
        </sections>
        <sections>
            <label><!-- Related To --></label>
            <section>Related To</section>
        </sections>
        <sections>
            <label><!-- Release Dates --></label>
            <section>Release Dates</section>
        </sections>
    </layouts>
    <layouts>
        <layout>User Stories Layout - Portal</layout>
        <sections>
            <label><!-- Cost &amp; Delivery --></label>
            <section>Cost &amp; Delivery</section>
        </sections>
        <sections>
            <label><!-- Custom Links --></label>
            <section>Custom Links</section>
        </sections>
        <sections>
            <label><!-- Description --></label>
            <section>Description</section>
        </sections>
        <sections>
            <label><!-- Related To --></label>
            <section>Related To</section>
        </sections>
    </layouts>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <errorMessage><!-- Please enter the release --></errorMessage>
        <name>Sprint_is_present_then_release_is_Mandat</name>
    </validationRules>
</CustomObjectTranslation>
