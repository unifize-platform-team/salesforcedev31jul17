<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Expense_Report_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Manages employee expense reports and reimbursements</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>false</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>false</enableSharing>
    <enableStreamingApi>false</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>Advance_Amount__c</fullName>
        <description>Advance paid amount.</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter Advance amount.</inlineHelpText>
        <label>Advance Amount</label>
        <precision>10</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Closing_Balance__c</fullName>
        <externalId>false</externalId>
        <formula>Opening_Balance__c   +  Advance_Amount__c - Expense_Amount__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Closing Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Employee_Name__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Employee name for which this Expense is being created</inlineHelpText>
        <label>Employee Name</label>
        <referenceTo>Contact</referenceTo>
        <relationshipName>Expenses</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Expense_Amount__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Sums up the Amounts of all the Expense Items</inlineHelpText>
        <label>Expense Amount</label>
        <summarizedField>Expense_Item__c.Amount__c</summarizedField>
        <summaryForeignKey>Expense_Item__c.Expense_Reimbursement__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Notes_Comments__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate any descriptive Notes / Comments (Narrative) for this Expense Report</inlineHelpText>
        <label>Notes / Comments</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Opening_Balance__c</fullName>
        <description>Opening Balance</description>
        <externalId>false</externalId>
        <inlineHelpText>Enter Opening Balance</inlineHelpText>
        <label>Opening Balance</label>
        <precision>10</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the Status of this Expense Report</inlineHelpText>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Pending</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>Submitted_for_Approval</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Approved</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Cancelled</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Expense Reimbursement</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>EXR-{00000}</displayFormat>
        <label>Expense Report ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Expense Reimbursements</pluralLabel>
    <searchLayouts/>
    <sharingModel>Private</sharingModel>
    <startsWith>Vowel</startsWith>
</CustomObject>
