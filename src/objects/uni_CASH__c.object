<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Manages debits, credits and adjustments for uni[CASH]</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>false</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>false</enableSharing>
    <enableStreamingApi>false</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Account__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Account for which this uniCASH Entry is being credited</inlineHelpText>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipName>uniCASH_Entries</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Notes_Comments__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate descriptive Notes / Comments for this uniCASH Entry</inlineHelpText>
        <label>Notes / Comments</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Status of this uniCASH Entry</inlineHelpText>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Pending</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>Submitted for Approval</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Approved</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Inactive</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>uni_CASH_Credit_Amount__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Quantity of uniCASH credits for this uniCASH Entry</inlineHelpText>
        <label>uni[CASH] Credit Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>uni_CASH_Debit_Amount__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Debit amount for this uni[CASH] entry</inlineHelpText>
        <label>uni[CASH] Debit Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>uni[CASH] Entry</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>UNC-{000000}</displayFormat>
        <label>uni[CASH] Entry ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>uni[CASH] Entries</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>uniCASH_Credit</fullName>
        <active>true</active>
        <description>For crediting uniCASH to an Account</description>
        <label>uniCASH Credit</label>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Approved</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Inactive</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pending</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Rejected</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Submitted for Approval</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>uniCASH_Debit</fullName>
        <active>true</active>
        <description>For debiting uniCASH to an Account</description>
        <label>uniCASH Debit</label>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Approved</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Inactive</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pending</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Rejected</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Submitted for Approval</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
