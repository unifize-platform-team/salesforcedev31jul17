<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>myProject_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>myProject</compactLayoutAssignment>
    <compactLayouts>
        <fullName>myProject</fullName>
        <fields>Project_Name__c</fields>
        <fields>Status__c</fields>
        <fields>Partner__c</fields>
        <fields>Required_Date_Time__c</fields>
        <label>myProject</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This manages myProjects within the myLibrary application</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>false</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>false</enableSharing>
    <enableStreamingApi>false</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Auto_Create_Standard_Raw_Materials__c</fullName>
        <defaultValue>true</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Indicate whether this myProject should use Standard Raw Materials.  You may also add Non-Standard Raw Materials and Remnants in the myProject Raw Materials section</inlineHelpText>
        <label>Auto-Create Standard Raw Materials</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Auto_Create_myProject_Checkpoints__c</fullName>
        <defaultValue>true</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Automatically create myProject Checkpoints based on the added myProject Processes</inlineHelpText>
        <label>Auto-Create myProject Checkpoints</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Cleared_Checkpoints__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Returns the count of all Checkpoints that have had their result marked as either Okay or Not Okay</inlineHelpText>
        <label>Cleared Checkpoints</label>
        <summaryFilterItems>
            <field>myProject_Checkpoint__c.Result__c</field>
            <operation>equals</operation>
            <value>Okay, Not Okay</value>
        </summaryFilterItems>
        <summaryForeignKey>myProject_Checkpoint__c.myProject__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Customer__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Customer with which this myProject is associated</inlineHelpText>
        <label>Customer</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>myProjects (Customer)</relationshipLabel>
        <relationshipName>myProjects1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Partner_Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Partner Contact for this myProject</inlineHelpText>
        <label>Partner Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>myProjects</relationshipLabel>
        <relationshipName>myProjects</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Partner__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Account with which this myProject is associated</inlineHelpText>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>myProjects</relationshipLabel>
        <relationshipName>myUploads</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Project_Name__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the descriptive name for this Project</inlineHelpText>
        <label>myProject Name</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Required_Date_Time__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Date/Time that this myProject is required for delivery</inlineHelpText>
        <label>Required Date/Time</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the Status of this myProject</inlineHelpText>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Pending</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>Preparation_Feasibility</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Nest</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>NC_Program</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Complete</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Cancelled</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Thickness_Unit__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Thickness Unit of Measure for this myProject</inlineHelpText>
        <label>Thickness Unit</label>
        <referenceTo>Unit_of_Measurement__c</referenceTo>
        <relationshipLabel>myProjects</relationshipLabel>
        <relationshipName>myProjects</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Thickness__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Material Thickness for this myProject</inlineHelpText>
        <label>Thickness</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Total_Checkpoints__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Returns the count of all the Checkpoints related to this myProject</inlineHelpText>
        <label>Total Checkpoints</label>
        <summaryForeignKey>myProject_Checkpoint__c.myProject__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Upload_Notes_Comments__c</fullName>
        <description>Triggers the Upload Notes / Comments from a workflow</description>
        <externalId>false</externalId>
        <inlineHelpText>Returns the Upload Notes / Comments for this myProject</inlineHelpText>
        <label>Upload Notes / Comments</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Upload__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Upload with which this myProject is associated</inlineHelpText>
        <label>Upload</label>
        <referenceTo>Upload__c</referenceTo>
        <relationshipLabel>myProjects</relationshipLabel>
        <relationshipName>myProjects</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>myMachine__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the myMachine with which this myProject is associated</inlineHelpText>
        <label>myMachine</label>
        <referenceTo>myMachine__c</referenceTo>
        <relationshipLabel>myProjects</relationshipLabel>
        <relationshipName>myProjects</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>myProject_Notes_Comments__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate generic myProject Notes/Comments.  This will automatically update into the Notes / Comments of any Part Records created</inlineHelpText>
        <label>myProject Notes / Comments</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>myProject_Status__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Returns the Process for which there is most recently an incomplete Job</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the current status of this myProject</inlineHelpText>
        <label>myProject Status</label>
        <referenceTo>Process__c</referenceTo>
        <relationshipLabel>myProjects</relationshipLabel>
        <relationshipName>myProjects</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>myProject</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Project_Name__c</columns>
        <columns>Upload__c</columns>
        <columns>Partner__c</columns>
        <columns>Partner_Contact__c</columns>
        <columns>Required_Date_Time__c</columns>
        <columns>Status__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>Venkat_s_myProject_List_View</fullName>
        <columns>CREATED_DATE</columns>
        <columns>NAME</columns>
        <columns>Customer__c</columns>
        <columns>OBJECT_ID</columns>
        <columns>Required_Date_Time__c</columns>
        <columns>Project_Name__c</columns>
        <filterScope>Everything</filterScope>
        <label>Venkat&apos;s myProject List View</label>
    </listViews>
    <nameField>
        <displayFormat>PROJ-{000000}</displayFormat>
        <label>myProject ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>myProjects</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Nest</fullName>
        <active>false</active>
        <description>For creating myProjects which only require Nesting Output.  Use this option for generating customer Quotes.</description>
        <label>Nest</label>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Complete</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Nest</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pending</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Program</fullName>
        <active>false</active>
        <description>For creating myProjects which require Nesting and Machine Readable NC Program Output.</description>
        <label>Program</label>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Complete</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>NC Program</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Nest</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pending</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Preparation %26 Feasibility</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>myProject</fullName>
        <active>true</active>
        <label>myProject</label>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Cancelled</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Complete</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>NC Program</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Nest</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pending</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Preparation %26 Feasibility</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>Project_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>RECORDTYPE</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Required_Date_Time__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATED_DATE</customTabListAdditionalFields>
        <customTabListAdditionalFields>Partner__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Partner_Contact__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>myMachine__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Project_Name__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>RECORDTYPE</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Required_Date_Time__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATED_DATE</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Partner__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Partner_Contact__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>myMachine__c</lookupDialogsAdditionalFields>
        <lookupFilterFields>NAME</lookupFilterFields>
        <lookupFilterFields>Project_Name__c</lookupFilterFields>
        <lookupFilterFields>RECORDTYPE</lookupFilterFields>
        <lookupFilterFields>Status__c</lookupFilterFields>
        <lookupFilterFields>Required_Date_Time__c</lookupFilterFields>
        <lookupFilterFields>Partner__c</lookupFilterFields>
        <lookupPhoneDialogsAdditionalFields>Project_Name__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>RECORDTYPE</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Required_Date_Time__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>CREATED_DATE</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Partner__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Partner_Contact__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>myMachine__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Project_Name__c</searchFilterFields>
        <searchFilterFields>RECORDTYPE</searchFilterFields>
        <searchFilterFields>Status__c</searchFilterFields>
        <searchFilterFields>Required_Date_Time__c</searchFilterFields>
        <searchFilterFields>CREATED_DATE</searchFilterFields>
        <searchFilterFields>Partner__c</searchFilterFields>
        <searchFilterFields>Partner_Contact__c</searchFilterFields>
        <searchFilterFields>myMachine__c</searchFilterFields>
        <searchResultsAdditionalFields>Project_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>RECORDTYPE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Required_Date_Time__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATED_DATE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Partner__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Partner_Contact__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>myMachine__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <webLinks>
        <fullName>Create_myParts</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Create myParts</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>/flow/myPart_Creation_from_myUploads?UploadID={!myProject__c.Id}</url>
    </webLinks>
</CustomObject>
