<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Position_Record_Page2</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object stores information about the open job positions at our company.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Read</externalSharingModel>
    <fields>
        <fullName>Apex__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Apex</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Close_Date__c</fullName>
        <externalId>false</externalId>
        <label>Close Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Csharp__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>C#</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Days_Open__c</fullName>
        <description>The number of days a position has been (or was) open.</description>
        <externalId>false</externalId>
        <formula>IF( ISNULL( Close_Date__c ) ,
             TODAY()  -  Open_Date__c ,
             Close_Date__c  -  Open_Date__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Days Open</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Department_Manager__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Department Manager for this Position.  They will receive automated mails when Job Applications are received.</inlineHelpText>
        <label>Department Manager</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Positions</relationshipLabel>
        <relationshipName>Positions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Educational_Requirements__c</fullName>
        <externalId>false</externalId>
        <label>Educational Requirements</label>
        <length>32000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Hire_By__c</fullName>
        <defaultValue>TODAY() + 90</defaultValue>
        <externalId>false</externalId>
        <label>Hire By</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>JavaScript__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>JavaScript</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Java__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Java</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Job_Applications__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the total number of Job Applications for this Position</inlineHelpText>
        <label>Job Applications</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Job_Description__c</fullName>
        <description>High-level description of the job and what its duties are.</description>
        <externalId>false</externalId>
        <label>Job Description</label>
        <length>32000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Location__c</fullName>
        <externalId>false</externalId>
        <label>Location</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Bangalore, India</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>San Francisco, CA</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Austin, TX</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Boulder, CO</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>London, England</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>New York, NY</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Mumbai, India</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Sydney, Australia</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Tokyo, Japan</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Mandatory_Skills__c</fullName>
        <externalId>false</externalId>
        <label>Mandatory Skills</label>
        <length>32000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Max_Pay__c</fullName>
        <externalId>false</externalId>
        <label>Max Pay</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Min_Pay__c</fullName>
        <externalId>false</externalId>
        <label>Min Pay</label>
        <precision>9</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Open_Date__c</fullName>
        <externalId>false</externalId>
        <label>Open Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Preferred_Skills__c</fullName>
        <externalId>false</externalId>
        <label>Preferred Skills</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Recruiter_10__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Recruiter 10</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Positions9</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Recruiter_1__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Recruiter 1</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Positions1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Recruiter_2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Recruiter 2</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Positions2</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Recruiter_3__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Recruiter 3</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Positions3</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Recruiter_4__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Recruiter 4</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Positions4</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Recruiter_5__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Recruiter 5</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Positions5</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Recruiter_6__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Recruiter 6</label>
        <referenceTo>User</referenceTo>
        <relationshipName>PositionsHs0y</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Recruiter_7__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Recruiter 7</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Positions6</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Recruiter_8__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Recruiter 8</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Positions7</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Recruiter_9__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Recruiter 9</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Positions8</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Responsibilities__c</fullName>
        <externalId>false</externalId>
        <label>Responsibilities</label>
        <length>32000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Reviews__c</fullName>
        <externalId>false</externalId>
        <label>Reviews</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>New Position</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>Pending Approval</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Open - Approved</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>On Hold</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Closed - Filled</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Closed - Not Approved</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Closed - Canceled</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Full Time</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Part Time</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Internship</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Contractor</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Position</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Location__c</columns>
        <columns>Status__c</columns>
        <columns>Type__c</columns>
        <columns>Min_Pay__c</columns>
        <columns>Max_Pay__c</columns>
        <columns>Open_Date__c</columns>
        <columns>Close_Date__c</columns>
        <columns>Days_Open__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Position Title</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Positions</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Position</fullName>
        <active>true</active>
        <description>Record Type for Positions</description>
        <label>Position</label>
        <picklistValues>
            <picklist>Location__c</picklist>
            <values>
                <fullName>Austin%2C TX</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Bangalore%2C India</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Boulder%2C CO</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>London%2C England</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Mumbai%2C India</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>New York%2C NY</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>San Francisco%2C CA</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Sydney%2C Australia</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Tokyo%2C Japan</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Closed - Canceled</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Closed - Filled</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Closed - Not Approved</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>New Position</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>On Hold</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Open - Approved</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pending Approval</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Type__c</picklist>
            <values>
                <fullName>Contractor</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Full Time</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Internship</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Part Time</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>Min_Pay__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Max_Pay__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Type__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Open_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Close_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Days_Open__c</customTabListAdditionalFields>
        <searchResultsAdditionalFields>Location__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Min_Pay__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Max_Pay__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Open_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Close_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Days_Open__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Read</sharingModel>
    <validationRules>
        <fullName>Close_Date_Rule</fullName>
        <active>true</active>
        <errorConditionFormula>AND (
    ISNULL(  Close_Date__c ),
    OR (
         ISPICKVAL(  Status__c , &quot;Closed - Filled&quot;),
         ISPICKVAL(  Status__c , &quot;Closed - Not Approved&quot;))
)</errorConditionFormula>
        <errorDisplayField>Close_Date__c</errorDisplayField>
        <errorMessage>Close Date must be specified when Status is set to &apos;Closed.&apos;</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Min_Pay_Rule</fullName>
        <active>true</active>
        <description>Min Pay should never exceed Max Pay.</description>
        <errorConditionFormula>Min_Pay__c  &gt;  Max_Pay__c</errorConditionFormula>
        <errorDisplayField>Min_Pay__c</errorDisplayField>
        <errorMessage>Min Pay cannot exceed Max Pay.</errorMessage>
    </validationRules>
</CustomObject>
