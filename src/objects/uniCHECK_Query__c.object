<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>myProject_Query_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>myProject_Query</compactLayoutAssignment>
    <compactLayouts>
        <fullName>myProject_Query</fullName>
        <fields>Name</fields>
        <fields>Accept_Reject_Resolution__c</fields>
        <fields>Resolution_Type__c</fields>
        <label>myProject Query</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Manages the myQueries relating to a Job and myProject</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Accept_Reject_Resolution__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Accept/Reject Action for this this myProject Query</inlineHelpText>
        <label>Accept / Reject Resolution</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Accepted</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Account_Notes_Comments__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate any Notes / Comments relating to this myQuery, including any reasons for Rejecting</inlineHelpText>
        <label>Account Notes / Comments</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>After_Image__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the After Image of this myProject Query</inlineHelpText>
        <label>After Image</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Before_Image__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Before Image of this myQuery</inlineHelpText>
        <label>Before Image</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Drawing_Full_View__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>The full view of the original drawing with which this myQuery is associated</inlineHelpText>
        <label>Drawing Full View</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Job__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Job with which this myQuery is associated</inlineHelpText>
        <label>Job</label>
        <referenceTo>Job__c</referenceTo>
        <relationshipName>uniCHECK_Query</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Material__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Material relating to this myProject Query</inlineHelpText>
        <label>Material</label>
        <referenceTo>Material__c</referenceTo>
        <relationshipName>myProject_Queries</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Quantity relating to the myProject Query Part</inlineHelpText>
        <label>Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Query_Problem_Statement__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Problem Statement of this myProject Query</inlineHelpText>
        <label>Query Problem Statement</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Resolution_Action_Taken__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the summary of the Resolution Action Taken by Unifize</inlineHelpText>
        <label>Resolution Action Taken</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Resolution_Type__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the type of resolution that has been taken by Unifize to clear this myProject Query</inlineHelpText>
        <label>Resolution Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Exclusion</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Assumption</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Deviation</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>myItem_Version_After__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the myPart Revision (After) for this myProject Query</inlineHelpText>
        <label>myItem Version - After</label>
        <referenceTo>myItem_Version__c</referenceTo>
        <relationshipLabel>uni[CHECK] Queries (myPart Revision - After)</relationshipLabel>
        <relationshipName>myProject_Queries1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>myItem_Version__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the myPart Revision (Before) relating to this myProject Query</inlineHelpText>
        <label>myItem Version - Before</label>
        <referenceTo>myItem_Version__c</referenceTo>
        <relationshipLabel>uni[CHECK] Queries (myPart Revision - Before)</relationshipLabel>
        <relationshipName>myProject_Queries</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>myNest__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the myNest with which this myQuery is associated</inlineHelpText>
        <label>myNest</label>
        <referenceTo>Nest__c</referenceTo>
        <relationshipName>uniCHECK_Queries</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>myPart__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the myPart with which this myQuery is associated</inlineHelpText>
        <label>myPart</label>
        <referenceTo>Part__c</referenceTo>
        <relationshipName>uniCHECK_Queries</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>myProgram__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the myProgram with which this myQuery is associated</inlineHelpText>
        <label>myProgram</label>
        <referenceTo>Program__c</referenceTo>
        <relationshipName>uniCHECK_Queries</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>myProject__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the myProject with which this myQuery is associated</inlineHelpText>
        <label>myProject</label>
        <referenceTo>myProject__c</referenceTo>
        <relationshipName>uniCHECK_Queries</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>uni[CHECK] Query</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>QUE-{000000}</displayFormat>
        <label>myProject Query ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>uni[CHECK] Queries</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <startsWith>Vowel</startsWith>
</CustomObject>
