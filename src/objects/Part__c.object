<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>myItem_Record_Page1</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <compactLayouts>
        <fullName>myitem</fullName>
        <fields>Name</fields>
        <fields>RecordTypeId</fields>
        <fields>myPart_Status__c</fields>
        <label>myitem</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Manages the upload, viewing and storage of Customer and Partner Items</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Account with which this myItem is associated</inlineHelpText>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>myItems</relationshipLabel>
        <relationshipName>Parts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Area_Unit__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the Area Unit of Measurement for this myItem</inlineHelpText>
        <label>Area Unit</label>
        <referenceTo>Unit_of_Measurement__c</referenceTo>
        <relationshipLabel>myItems (Area Unit)</relationshipLabel>
        <relationshipName>myParts3</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Closing_Stock__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the total closing stock of this myItem in all related myItem Locations</inlineHelpText>
        <label>Closing Stock</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Current_Revision_URL__c</fullName>
        <externalId>false</externalId>
        <label>Current Version URL</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>Current_Version_No__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Returns the Current Version No. for this myItem</inlineHelpText>
        <label>Current Version No.</label>
        <summarizedField>myItem_Version__c.Version_No__c</summarizedField>
        <summaryFilterItems>
            <field>myItem_Version__c.Current_Version__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>myItem_Version__c.myItem__c</summaryForeignKey>
        <summaryOperation>max</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Current_Versions__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Returns the total number of current Item Versions for this myItem</inlineHelpText>
        <label>Current Versions</label>
        <summaryFilterItems>
            <field>myItem_Version__c.Current_Version__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>myItem_Version__c.myItem__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Current_myItem_Revision__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Current myItem Version for this myItem</inlineHelpText>
        <label>Current myItem Version</label>
        <referenceTo>myItem_Version__c</referenceTo>
        <relationshipLabel>myItems</relationshipLabel>
        <relationshipName>myItems</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Cut_Length__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the Cut Length of this myItem</inlineHelpText>
        <label>Cut Length</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cutting_Time_seconds__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the Cutting Time in seconds</inlineHelpText>
        <label>Cutting Time (s)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Do_Not_Nest_in_Internal_Contours__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Defines whether the myItem can be nested in Internal Contours</inlineHelpText>
        <label>Do Not Nest in Internal Contours</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Download__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK( Current_Revision_URL__c, &quot;Download Current Version&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Click this link to download the current version of this myItem</inlineHelpText>
        <label>Download</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Drawing_No__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Drawing No. or other reference for this myItem</inlineHelpText>
        <label>Drawing No.</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EndPoint__c</fullName>
        <description>Field for storing EndPoint</description>
        <externalId>false</externalId>
        <label>EndPoint</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Filler_Part__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Indicate whether this myItem should be considered as a Filler Part</inlineHelpText>
        <label>Filler Part</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Job__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Job from which this myItem was created</inlineHelpText>
        <label>Job</label>
        <referenceTo>Job__c</referenceTo>
        <relationshipLabel>myItems</relationshipLabel>
        <relationshipName>myParts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Length_Unit__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the Unit of Measurement for Length</inlineHelpText>
        <label>Length Unit</label>
        <referenceTo>Unit_of_Measurement__c</referenceTo>
        <relationshipLabel>myItems (Length Unit)</relationshipLabel>
        <relationshipName>myItems4</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Length__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the myItem Length</inlineHelpText>
        <label>Length</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Material__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Material with which this myItem is associated</inlineHelpText>
        <label>Material</label>
        <referenceTo>Material__c</referenceTo>
        <relationshipLabel>myItems</relationshipLabel>
        <relationshipName>myItem</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Net_Area__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the Net Area of this myItem</inlineHelpText>
        <label>Net Area</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Net_Weight__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Net Weight of this myItem</inlineHelpText>
        <label>Net Weight</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>No_of_Pierces__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the No. of Pierces for this myItem</inlineHelpText>
        <label>No. of Pierces</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Notes_Comments__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate any Notes / Comments associated with this myItem.  You may also upload photos and other details as may be required.</inlineHelpText>
        <label>Detailed Description</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Rect_Area__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the Rectangular Area of this myItem.</inlineHelpText>
        <label>Rect Area</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Rect_Weight__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the Rectangular Weight of this myItem</inlineHelpText>
        <label>Rect Weight</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Standard_Quantity__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Standard Quantity of this myItem</inlineHelpText>
        <label>Standard Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Thickness_Unit__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Thickness Unit of Measurement for this myItem</inlineHelpText>
        <label>Thickness Unit</label>
        <referenceTo>Unit_of_Measurement__c</referenceTo>
        <relationshipLabel>myItems (Thickness Unit)</relationshipLabel>
        <relationshipName>myItems1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Thickness__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the myItem thickness</inlineHelpText>
        <label>Thickness</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Tolerance_Unit__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Tolerance Unit of Measurement for this myItem</inlineHelpText>
        <label>Tolerance Unit</label>
        <referenceTo>Unit_of_Measurement__c</referenceTo>
        <relationshipLabel>myItems (Tolerance Unit)</relationshipLabel>
        <relationshipName>myItems2</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Tolerance__c</fullName>
        <defaultValue>2</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the manufacturing tolerance for this myItem</inlineHelpText>
        <label>Tolerance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Total_Versions__c</fullName>
        <description>Used to prevent the deletion on myItem Versions in conjunction with a Validation Rule</description>
        <externalId>false</externalId>
        <label>Total Versions</label>
        <summaryForeignKey>myItem_Version__c.myItem__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Upload_Item__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Upload Item with which this myItem is associated</inlineHelpText>
        <label>Upload Item</label>
        <referenceTo>myUpload_Item__c</referenceTo>
        <relationshipLabel>myItems</relationshipLabel>
        <relationshipName>myItems</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Weight_Unit__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Weight Unit of Measurement of this myItem</inlineHelpText>
        <label>Weight Unit</label>
        <referenceTo>Unit_of_Measurement__c</referenceTo>
        <relationshipLabel>myItems (Weight Unit)</relationshipLabel>
        <relationshipName>myItems</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Width__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the myItem Width</inlineHelpText>
        <label>Width</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>myFactory__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the myFactory with which this myItem is associated</inlineHelpText>
        <label>myFactory</label>
        <referenceTo>myFactory__c</referenceTo>
        <relationshipLabel>myItems</relationshipLabel>
        <relationshipName>myItems</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>myItem_Name__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the myItem Name.  This should be a summary description of this myItem</inlineHelpText>
        <label>myItem Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>myItem_Quantity_Unit__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Unit of Measurement for this myItem</inlineHelpText>
        <label>myItem Quantity Unit</label>
        <referenceTo>Unit_of_Measurement__c</referenceTo>
        <relationshipLabel>myItems (myItem Unit)</relationshipLabel>
        <relationshipName>myItems3</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>myItem_Rate_Unit__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the myItem Rate Unit of Measurement</inlineHelpText>
        <label>myItem Rate Unit</label>
        <referenceTo>Unit_of_Measurement__c</referenceTo>
        <relationshipLabel>myItems (myItem Rate Unit)</relationshipLabel>
        <relationshipName>myItems5</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>myItem_Unique_ID__c</fullName>
        <externalId>false</externalId>
        <formula>&quot;UNI-ITEM-&quot;&amp; &quot;A&quot; &amp; Account__r.Account_No__c &amp; &quot;-P&quot; &amp; SUBSTITUTE(Name, &quot;ITEM-&quot;,&quot;&quot;) &amp; &quot;-V&quot; &amp;  text(Current_Version_No__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Returns the complete Uni-Item internal ID for this myItem</inlineHelpText>
        <label>myItem Unique ID</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>myMaterial__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the myMaterial associated with this myItem</inlineHelpText>
        <label>myMaterial</label>
        <referenceTo>myMaterial__c</referenceTo>
        <relationshipLabel>myItems</relationshipLabel>
        <relationshipName>myItems</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>myPart_Status__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Pending: Drawing uploaded
Active: Ready for use
Review: Needs review by owner  
Inactive: Deactivated for use</inlineHelpText>
        <label>myItem Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Pending</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>Review</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Active</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Inactive</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>myItem</label>
    <listViews>
        <fullName>All</fullName>
        <columns>myPart_Status__c</columns>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>Drawing_No__c</columns>
        <columns>Thickness__c</columns>
        <columns>Net_Weight__c</columns>
        <columns>Tolerance__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>ITEM-{0000000}</displayFormat>
        <label>myItem ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>myItems</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Assembly</fullName>
        <active>true</active>
        <description>For creating an assembly of Parts</description>
        <label>Assembly</label>
        <picklistValues>
            <picklist>myPart_Status__c</picklist>
            <values>
                <fullName>Active</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Inactive</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pending</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Review</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Part</fullName>
        <active>true</active>
        <description>For creating a Part, which is an individual component made from the same material and processes</description>
        <label>Part</label>
        <picklistValues>
            <picklist>myPart_Status__c</picklist>
            <values>
                <fullName>Active</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Inactive</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pending</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Review</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Remnant</fullName>
        <active>true</active>
        <description>For creating a Remnant / Offcut / Cut-Sheet, or other reusable byproduct of a manufacturing process</description>
        <label>Remnant</label>
        <picklistValues>
            <picklist>myPart_Status__c</picklist>
            <values>
                <fullName>Active</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Inactive</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pending</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Review</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Scrap</fullName>
        <active>true</active>
        <description>For creating a scrap item generated during the manufacturing process</description>
        <label>Scrap</label>
        <picklistValues>
            <picklist>myPart_Status__c</picklist>
            <values>
                <fullName>Active</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Inactive</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pending</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Review</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Sheet</fullName>
        <active>true</active>
        <description>For creating a sheet raw material item</description>
        <label>Sheet</label>
        <picklistValues>
            <picklist>myPart_Status__c</picklist>
            <values>
                <fullName>Active</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Inactive</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Pending</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Review</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>Drawing_No__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>myPart_Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Thickness__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>myMaterial__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Standard_Quantity__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Tolerance__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Net_Weight__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Drawing_No__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>myPart_Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Thickness__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>myMaterial__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Standard_Quantity__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Tolerance__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Net_Weight__c</lookupDialogsAdditionalFields>
        <lookupFilterFields>NAME</lookupFilterFields>
        <lookupFilterFields>Drawing_No__c</lookupFilterFields>
        <lookupFilterFields>Account__c</lookupFilterFields>
        <lookupFilterFields>myPart_Status__c</lookupFilterFields>
        <lookupFilterFields>Thickness__c</lookupFilterFields>
        <lookupFilterFields>myMaterial__c</lookupFilterFields>
        <lookupPhoneDialogsAdditionalFields>Drawing_No__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Account__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>myPart_Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Thickness__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>myMaterial__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Standard_Quantity__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Tolerance__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Net_Weight__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Drawing_No__c</searchFilterFields>
        <searchFilterFields>Account__c</searchFilterFields>
        <searchFilterFields>myPart_Status__c</searchFilterFields>
        <searchFilterFields>Thickness__c</searchFilterFields>
        <searchFilterFields>myMaterial__c</searchFilterFields>
        <searchFilterFields>Standard_Quantity__c</searchFilterFields>
        <searchResultsAdditionalFields>Drawing_No__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>myPart_Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Thickness__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>myMaterial__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Standard_Quantity__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Tolerance__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Net_Weight__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Cannotdeletemyitemversions</fullName>
        <active>true</active>
        <errorConditionFormula>PRIORVALUE(Total_Versions__c)&gt;Total_Versions__c</errorConditionFormula>
        <errorMessage>You cannot delete Item Versions!  Deactivate them if you don&apos;t want to use them.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Morethanonecurrent</fullName>
        <active>true</active>
        <errorConditionFormula>Current_Versions__c &gt;1</errorConditionFormula>
        <errorMessage>You must first deactivate the earlier Version before marking this as the Current Version for this myItem</errorMessage>
    </validationRules>
</CustomObject>
