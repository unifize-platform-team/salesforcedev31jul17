<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Manages Expense Items relating to an Expense Report</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>false</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>false</enableSharing>
    <enableStreamingApi>false</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Amount__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Amount of this Expense Item</inlineHelpText>
        <label>Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Date_of_Expense__c</fullName>
        <description>Enter the date, when the expense was made.</description>
        <externalId>false</externalId>
        <inlineHelpText>This Date defines when the Expense was made.</inlineHelpText>
        <label>Date of Expense</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Expense_Reimbursement__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Expense Report with which this Expense Item is associated</inlineHelpText>
        <label>Expense Reimbursement</label>
        <referenceTo>Expense_Reimbursement__c</referenceTo>
        <relationshipLabel>Expense Items</relationshipLabel>
        <relationshipName>Expense_Items</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Expense_Type__c</fullName>
        <description>A picklist field to select the Expense Item Type.</description>
        <externalId>false</externalId>
        <inlineHelpText>A picklist field to select the Expense Item Type.
Just Select 1 Value from the Picklist values available</inlineHelpText>
        <label>Expense Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Conveyance</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Food &amp; Allowances</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Parking + Toll Charges</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Petrol</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Traveling (Bus/Taxi/etc)</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>From_Date__c</fullName>
        <description>enter expense reimbursement start date.</description>
        <externalId>false</externalId>
        <label>From Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Notes_Comments__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate any descriptive Notes / Comments for this Expense Item</inlineHelpText>
        <label>Notes / Comments</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Photo_s__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Insert and photos of receipts or other documents for this Expense Item</inlineHelpText>
        <label>Photo(s)</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Purpose_of_Visit__c</fullName>
        <description>Enter the reason of visit.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field indicate the reason of visit.</inlineHelpText>
        <label>Purpose of Visit</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>To_Date__c</fullName>
        <externalId>false</externalId>
        <label>To Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>Expense Item</label>
    <nameField>
        <displayFormat>EXI-{00000}</displayFormat>
        <label>Expense Item ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Expense Items</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <startsWith>Vowel</startsWith>
</CustomObject>
