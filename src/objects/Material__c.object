<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Manages materials used throughout myFactory</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Finish__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Finish that applies to this Material Code</inlineHelpText>
        <label>Finish</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Grade_New__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Grade that applies to this Material Code</inlineHelpText>
        <label>Grade</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Material_Name__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the descriptive name of this Material</inlineHelpText>
        <label>Material Name</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Material__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Material for this Material Code</inlineHelpText>
        <label>Material</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>MS</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>SS</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>GI</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Aluminium</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Copper</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Acrylic</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Wood</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Notes_Comments__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate any descriptive Notes / Comments relating to this Material</inlineHelpText>
        <label>Notes / Comments</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Reflectivity__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Reflectivity of this Material Code</inlineHelpText>
        <label>Reflectivity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Tensile_Strength__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Tensile Strength of this Material Code</inlineHelpText>
        <label>Tensile Strength</label>
        <precision>14</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Material</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Material_Name__c</columns>
        <columns>Material__c</columns>
        <columns>Grade_New__c</columns>
        <columns>Finish__c</columns>
        <columns>Reflectivity__c</columns>
        <columns>Tensile_Strength__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>MAT-{000}</displayFormat>
        <label>Material ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Material</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Material_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Material__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Finish__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Grade_New__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Reflectivity__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Tensile_Strength__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Notes_Comments__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Material_Name__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Material__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Finish__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Grade_New__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Reflectivity__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Tensile_Strength__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Notes_Comments__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Material_Name__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Material__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Finish__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Grade_New__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Reflectivity__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Tensile_Strength__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Notes_Comments__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Material_Name__c</searchFilterFields>
        <searchFilterFields>Material__c</searchFilterFields>
        <searchFilterFields>Finish__c</searchFilterFields>
        <searchFilterFields>Grade_New__c</searchFilterFields>
        <searchFilterFields>Reflectivity__c</searchFilterFields>
        <searchFilterFields>Tensile_Strength__c</searchFilterFields>
        <searchResultsAdditionalFields>Material_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Material__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Finish__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Grade_New__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Reflectivity__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Tensile_Strength__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Notes_Comments__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
