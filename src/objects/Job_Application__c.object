<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Job_Application_Record_Page1</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Represents a candidate&apos;s application to a position.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Average_Rating__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Number_of_Reviews__c&lt;&gt;0, 
Total_Rating__c / Number_of_Reviews__c, NULL)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Average Rating</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Candidate_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Candidate__r.First_Name__c &amp; &quot; &quot; &amp;  Candidate__r.Last_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Returns the Candidates Name from the related Candidate</inlineHelpText>
        <label>Candidate Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Candidate__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Candidate</label>
        <referenceTo>Candidate__c</referenceTo>
        <relationshipLabel>Job Applications</relationshipLabel>
        <relationshipName>R00ND0000001pXIZMA2</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Cover_Letter__c</fullName>
        <externalId>false</externalId>
        <label>Cover Letter</label>
        <length>32000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>Department_Manager__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Triggered from the Position.  Will receive automated emails when the Job Application is received.</inlineHelpText>
        <label>Department Manager</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Job Applications</relationshipLabel>
        <relationshipName>Job_Applications</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Number_of_Reviews__c</fullName>
        <externalId>false</externalId>
        <label>Number of Reviews</label>
        <summaryForeignKey>Review__c.Job_Application__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Picklist__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>New</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>Review Resume</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Phone Screen</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Schedule Interviews</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Shortlisted for Round 1</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Shortlisted for Round 2</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Extend an Offer</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Hired</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Candidate Not Interested</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Hold</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Position__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Position</label>
        <referenceTo>Position__c</referenceTo>
        <relationshipLabel>Job Applications</relationshipLabel>
        <relationshipName>Job_Applications</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Total_Rating__c</fullName>
        <externalId>false</externalId>
        <label>Total Rating</label>
        <summarizedField>Review__c.Total_Rating__c</summarizedField>
        <summaryForeignKey>Review__c.Job_Application__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <label>Job Application</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>Candidate__c</columns>
        <columns>Candidate_Name__c</columns>
        <columns>Position__c</columns>
        <columns>Number_of_Reviews__c</columns>
        <columns>Total_Rating__c</columns>
        <columns>Average_Rating__c</columns>
        <columns>Picklist__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>JA-{00000}</displayFormat>
        <label>Job Application Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Job Applications</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>CREATEDBY_USER</customTabListAdditionalFields>
        <customTabListAdditionalFields>Candidate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Position__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Number_of_Reviews__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Average_Rating__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Picklist__c</customTabListAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>CREATEDBY_USER</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Candidate__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Position__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Number_of_Reviews__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Average_Rating__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Picklist__c</lookupPhoneDialogsAdditionalFields>
        <searchResultsAdditionalFields>CREATEDBY_USER</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Candidate__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Position__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Number_of_Reviews__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Average_Rating__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Picklist__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <webLinks>
        <fullName>Close_Job_Applications</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Close Job Applications</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>// Include and initialize the AJAX Toolkit javascript library
//
{!REQUIRESCRIPT(&quot;/soap/ajax/10.0/connection.js&quot;)}

// Get the list of job applications that should be closed by using the
// $ObjectType merge field to indicate the type of record Ids that
// are expected.
//
var jobAppIdArr = {!GETRECORDIDS( $ObjectType.Job_Application__c )};

if (jobAppIdArr == null || jobAppIdArr.length == 0) {
    alert(&quot;Please select the job applications you wish to reject.&quot;);

} else {

    // Retrieving the job applications that should be deleted from
    // the database is inefficient and unnecessary. Instead, create
    // new job application records for each job application that
    // should be updated, store them in an array, and then use the
    // update API call.
    //
    var jobApps = new Array();

    for (var i = 0; i &lt; jobAppIdArr.length; i++) {
        var jobApp = new sforce.SObject(&quot;Job_Application__c&quot;);

        // Since we&apos;ll be using the update call, we must set the id
        // on the new job application record.
        //
        jobApp.Id = jobAppIdArr[i];

        // Next set the appropriate fields to reject the
        //application.
        //
        jobApp.Picklist__c = &quot;Closed&quot;;

        // Finally add the record to our array.
        //
        jobApps.push(jobApp);
    }

    // Now make the update API call in a try statement so we can
    // catch any errors. Save the resulting array so we can also
    // check for problems with individual records.
    //
    var callCompleted = false;
    try {
        var result = sforce.connection.update(jobApps);
        callCompleted = true;

    } catch(error) {
        alert(&quot;Failed to update Job Applications with error: &quot; + error);
    }

    // Now check for problems with individual records.
    //
    if (callCompleted) {
        for (var i = 0; i &lt; result.length; i++) {
            if (!result[i].getBoolean(&quot;success&quot;)) {
                alert(&quot;Job Application (id=&apos;&quot; + jobAppIdArr[i] +
                      &quot;&apos;) could not be updated with error: &quot; +
                      result[i].errors);
            }
        }

        // Finally, refresh the browser to provide confirmation
        // to the user that the job applications were rejected.
        //
        window.location.reload(true);
    }
}</url>
    </webLinks>
</CustomObject>
