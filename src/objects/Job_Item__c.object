<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Job_Input_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Manages Job Items relating to Jobs</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>End_Date_Time__c</fullName>
        <externalId>false</externalId>
        <label>End Date/Time</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Job__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Job with which this Job Input is associated</inlineHelpText>
        <label>Job</label>
        <referenceTo>Job__c</referenceTo>
        <relationshipLabel>Job Items</relationshipLabel>
        <relationshipName>Job_Inputs</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>RecordTypeName__c</fullName>
        <description>For use in Lightning Component related list filters</description>
        <externalId>false</externalId>
        <formula>RecordType.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Record Type Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sequence_No__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Sequence No. of this Job Item</inlineHelpText>
        <label>Sequence No.</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Start_Date_Time__c</fullName>
        <externalId>false</externalId>
        <label>Start Date/Time</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the Status of this Job Item</inlineHelpText>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Not Started</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>In Progress</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Completed</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Upload_Item__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Upload Item with which this Job Input is associated</inlineHelpText>
        <label>Upload Item</label>
        <referenceTo>myUpload_Item__c</referenceTo>
        <relationshipName>Job_Inputs</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>myItem_Attached_File__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Attached File to myItem for this Job Item</inlineHelpText>
        <label>myItem Attached File</label>
        <referenceTo>myUpload_Item__c</referenceTo>
        <relationshipLabel>Job Items</relationshipLabel>
        <relationshipName>Job_Items</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>myItem__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the myPart with which this Job Input is associated</inlineHelpText>
        <label>myItem</label>
        <referenceTo>Part__c</referenceTo>
        <relationshipLabel>myItems</relationshipLabel>
        <relationshipName>Job_Inputs</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>myNest__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the myNest with which this Job Input is associated</inlineHelpText>
        <label>myNest</label>
        <referenceTo>Nest__c</referenceTo>
        <relationshipName>Job_Inputs</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>myProgram__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the myProgram related to this Job Item</inlineHelpText>
        <label>myProgram</label>
        <referenceTo>Program__c</referenceTo>
        <relationshipLabel>Job Iitem</relationshipLabel>
        <relationshipName>Job_Iitem</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>myProject_Item__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the myProject Item with which this Job Item is associated</inlineHelpText>
        <label>myProject Item</label>
        <referenceTo>myProject_Part__c</referenceTo>
        <relationshipLabel>Job Items</relationshipLabel>
        <relationshipName>Job_Items</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Job Item</label>
    <nameField>
        <displayFormat>JOI-{00000}</displayFormat>
        <label>Job Item ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Job Items</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Input</fullName>
        <active>true</active>
        <description>For creating Job Items that are Inputs to Jobs</description>
        <label>Input</label>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Completed</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>In Progress</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Not Started</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Output</fullName>
        <active>true</active>
        <description>For creating Job Items that are Outputs to Jobs</description>
        <label>Output</label>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Completed</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>In Progress</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Not Started</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
