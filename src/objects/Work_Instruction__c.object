<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Work_Instruction_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Manages Work Instructions relating to Unifize Processes</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>false</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>false</enableSharing>
    <enableStreamingApi>false</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Exclusions__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Define any Exclusions to this Work Instruction</inlineHelpText>
        <label>Exclusions</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Inputs__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Inputs to this Work Instruction</inlineHelpText>
        <label>Inputs</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Outputs__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Outputs relating to this Work Instruction</inlineHelpText>
        <label>Outputs</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Process__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Process with which this Work Instruction is associated</inlineHelpText>
        <label>Process</label>
        <referenceTo>Process__c</referenceTo>
        <relationshipLabel>Work Instructions</relationshipLabel>
        <relationshipName>Work_Instructions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Purpose__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Purpose of this Work Instruction</inlineHelpText>
        <label>Purpose</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Responsibility__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Person/Group/Role that is responsible for this Work Instruction</inlineHelpText>
        <label>Responsibility</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Revision_Details__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate details of this revision, especially highlighting any differences with the previous revision</inlineHelpText>
        <label>Revision Details</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Revision_No__c</fullName>
        <defaultValue>0</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Revision No. of this Work Instruction</inlineHelpText>
        <label>Revision No.</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Scope__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Define the Scope of this Work Instruction</inlineHelpText>
        <label>Scope</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Work_Instruction_Name__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Indicate the Descriptive Name of this Work Instruction</inlineHelpText>
        <label>Work Instruction Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <label>Work Instruction</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Work_Instruction_Name__c</columns>
        <columns>Process__c</columns>
        <columns>Revision_No__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>WI-{00000}</displayFormat>
        <label>Work Instruction ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Work Instructions</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Work_Instruction_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Process__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Revision_No__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATEDBY_USER</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATED_DATE</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Work_Instruction_Name__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Process__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Revision_No__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATEDBY_USER</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATED_DATE</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Work_Instruction_Name__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Process__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Revision_No__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>CREATEDBY_USER</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>CREATED_DATE</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Work_Instruction_Name__c</searchFilterFields>
        <searchFilterFields>Process__c</searchFilterFields>
        <searchFilterFields>Revision_No__c</searchFilterFields>
        <searchFilterFields>CREATEDBY_USER</searchFilterFields>
        <searchFilterFields>CREATED_DATE</searchFilterFields>
        <searchResultsAdditionalFields>Work_Instruction_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Process__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Revision_No__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATEDBY_USER</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATED_DATE</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
