<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#2E4C68</headerColor>
        <logoVersion>1</logoVersion>
    </brand>
    <description>Unifize&apos;s Learning Management System</description>
    <formFactors>Large</formFactors>
    <label>Unifize Training</label>
    <navType>Standard</navType>
    <tab>My_Training</tab>
    <tab>Learning__c</tab>
    <tab>Learning_Category__c</tab>
    <tab>Training_Plan__c</tab>
    <tab>Training_Track__c</tab>
    <tab>Achievement__c</tab>
    <tab>SubTabAchievements</tab>
    <tab>Learning_Assignment__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <uiType>Lightning</uiType>
    <utilityBar>Unifize_Training_UtilityBar1</utilityBar>
</CustomApplication>
