<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#2E4C68</headerColor>
        <logoVersion>1</logoVersion>
    </brand>
    <description>Manages the upload, storage, retrieval and arrangement of Parts, Groupings and Work Orders</description>
    <formFactors>Large</formFactors>
    <label>myFactory</label>
    <navType>Standard</navType>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-report</tab>
    <tab>myFactory__c</tab>
    <tab>myMachine__c</tab>
    <tab>myTools__c</tab>
    <tab>myProject__c</tab>
    <tab>myInventory__c</tab>
    <uiType>Lightning</uiType>
    <utilityBar>myFactory_UtilityBar2</utilityBar>
</CustomApplication>
