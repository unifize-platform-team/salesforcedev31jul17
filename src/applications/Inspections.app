<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>Inspections</label>
    <tab>Store__c</tab>
    <tab>Inspection__c</tab>
    <tab>Survey__c</tab>
    <tab>Survey_Question__c</tab>
    <tab>Survey_Response__c</tab>
    <tab>Inspection</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Part__c</tab>
    <tab>Process__c</tab>
</CustomApplication>
