<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#2E4C68</headerColor>
        <logoVersion>1</logoVersion>
    </brand>
    <description>Manages the Unifize Customer &amp; Partner service functions</description>
    <formFactors>Large</formFactors>
    <label>Unifize Service</label>
    <navType>Standard</navType>
    <tab>standard-home</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-report</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Case</tab>
    <tab>Ticket__c</tab>
    <uiType>Lightning</uiType>
    <utilityBar>Unifize_Service_UtilityBar</utilityBar>
</CustomApplication>
