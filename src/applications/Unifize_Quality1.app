<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#2E4C68</headerColor>
        <logoVersion>1</logoVersion>
    </brand>
    <description>Manages Unifize Quality, including Non-Conformances, Root Causes and CAPAs</description>
    <formFactors>Large</formFactors>
    <label>Unifize Quality</label>
    <navType>Standard</navType>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>Acceptance__c</tab>
    <tab>Non_Conformance__c</tab>
    <tab>Root_Cause__c</tab>
    <tab>Root_Cause_Type__c</tab>
    <tab>CAPA__c</tab>
    <tab>COPQ__c</tab>
    <tab>myCheckpoint__c</tab>
    <tab>myProject_Checkpoint__c</tab>
    <tab>uniCHECK_Query__c</tab>
    <tab>Work_Instruction__c</tab>
    <uiType>Lightning</uiType>
    <utilityBar>Unifize_Quality_UtilityBar1</utilityBar>
</CustomApplication>
