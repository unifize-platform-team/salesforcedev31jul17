<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>From the creation of a new position through the interviews aimed at filling it, Recruiting for AppExchange offers a basic format for tracking your company’s evaluation of potential employees. It gives you a single location from which to manage the schedul</description>
    <label>Recruiting</label>
    <tab>standard-report</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>Position__c</tab>
    <tab>Candidate__c</tab>
    <tab>Job_Application__c</tab>
    <tab>Review__c</tab>
</CustomApplication>
