<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Manages the Master Data used throughout the Unifize Platform</description>
    <label>Master Data</label>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>Process__c</tab>
    <tab>Ticket__c</tab>
    <tab>Material__c</tab>
    <tab>Work_Instruction__c</tab>
</CustomApplication>
