<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Manages the upload, storage, retrieval and arrangement of Parts, Groupings and Work Orders</description>
    <label>MyLibrary</label>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>Part__c</tab>
    <tab>Nest__c</tab>
</CustomApplication>
