public class S3FolderStructureNests implements S3FolderStructure{
    public String getFolderStructure(String recordId){
        Nest__c nest = [SELECT Name, Account__r.Account_ID__c FROM Nest__c WHERE Id = :recordId];
        
        String folderStructure = 'Jobs/Nest_Jobs/'+nest.Account__r.Account_ID__c+'/'+nest.Name+'/';
        
        return folderStructure;
    }
}