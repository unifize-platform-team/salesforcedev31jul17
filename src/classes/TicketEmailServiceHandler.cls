/*
Type Name: TicketEmailServiceHandler
Author: 
Created Date: 19/05/2016
Reason: Reason: This class is used creating ticket created by using Email Service
Test Class: 
Change History:
Author:
Modified Date: 
……..//
……..
--
*/
global class TicketEmailServiceHandler implements Messaging.InboundEmailHandler {
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope env) {
        String toAddress;
        String ccAddress;
        String value;
        Ticket__c tckt = new Ticket__c();
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        List<Attachment> email_atts=new List<Attachment>();//List of attachments coming in mail
        
        String[] prefixes = new String[] {
                                               'fw:',
                                               're:',
                                               'fwd:',
                                               'FW:',
                                               'RE:',
                                               'FWD:',
                                               'Re:',
                                               'Fwd:',
                                               'Re:',
                                               'automatic reply:',
                                               'out of office autoreply:',
                                               'out of office',
                                               'Automatic Reply:',
                                               'Out Of Office Autoreply:',
                                               ':',
                                               '-',
                                               'Out Of Office'
                                              };
        
        try{  
            User[] userObj = [SELECT Id FROM User WHERE Email = : email.fromAddress];  
            if(userObj.size() > 0){
                tckt.Owner__c = userObj[0].id;
            } 
               
            Contact[] contObj = [SELECT Id, Name, AccountId, Email, Phone, MobilePhone, Fax FROM Contact WHERE Email = : email.fromAddress];
            if (contObj.size() > 0) {
                Contact theContact = contObj[0];
                tckt.Account__c = theContact.AccountId;
                tckt.Contact__c = theContact.id;
                tckt.ContactEmail__c = theContact.email;
                tckt.ContactFax__c = theContact.fax;
                tckt.ContactMobile__c = theContact.mobilephone;
                tckt.ContactPhone__c = theContact.phone;
            }  
            
            /* *** TO */                    
            value = '';
            if (email.toAddresses != null) {
                Boolean seenOne = false;
                for (String to: email.toAddresses) {
                    if (seenOne) {
                        value += ';\n';
                    }
                    to = extractAddress(to);
                    value += to;
                    seenOne = true;
                }
            }
            toAddress = value;
            
           /* **** CC */
            value = '';
            if (email.ccAddresses != null) {            
                Boolean seenOne = false;
                for (String cc: email.ccAddresses) {
                    if (seenOne) {
                        value += ';\n';
                    }
                    cc = extractAddress(cc);
                    value += cc;
                    seenOne = true;
                }
            }  
            
            tckt.Origin__c = 'Email';
            tckt.CreatedDate__c = system.now();
            string emailBody = 'From:'+ email.fromAddress+'\n To:'+ toAddress + '\n CC:'+ ccAddress + '\n Date: ' + DateTime.now() + '\n Subject: '+ email.Subject + '\n\n' + email.plainTextBody;
            tckt.Description__c = emailBody;
            tckt.IsEscalated__c = true;
            
            String sub = email.Subject;
            for (String prefix: prefixes) {
                if(sub.contains(prefix)){
                sub = sub.substringAfterLast(prefix).trim();
                }   
            }
            sub  = sub.replaceAll('(\\s+)', ' ');
            tckt.Subject__c = sub;
            tckt.SuppliedEmail__c = email.fromAddress;
            tckt.SuppliedName__c = email.fromName; 
            tckt.priority__c = 'Medium';
            tckt.Status__c = 'New';
            insert tckt;  
            
            if (email.textAttachments != null && email.textAttachments.size() > 0) {
                for (Messaging.Inboundemail.TextAttachment tAttachment: email.textAttachments) {
                    Attachment attachment = new Attachment();
                    attachment.Name = tAttachment.fileName;
                    attachment.Body = Blob.valueOf(tAttachment.body);
                    attachment.ParentId = tckt.Id;
                    email_atts.add(attachment);
                }
            }

            if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
                for (Messaging.Inboundemail.BinaryAttachment bAttachment: email.binaryAttachments) {
                    Attachment attachment = new Attachment();
                    attachment.Name = bAttachment.fileName;
                    attachment.Body = bAttachment.body;
                    attachment.ParentId = tckt.Id; 
                    email_atts.add(attachment);
                }
            }       
            
            insert email_atts;             
        }
        catch(Exception e){
            system.debug(e.getmessage());
        }
              
        result.success = true;
         
        return result;
    }
    
    public String extractAddress(String inAddress) {
        try{
            String address;
            String patternString;
            Pattern thePattern;
            Matcher matcher;
            patternString = '.*<(.*)>.*';
            thePattern = Pattern.compile(patternString);
            matcher = thePattern.matcher(inAddress);
            
            if (matcher.matches()) {
                address = matcher.group(1);
            } else {
                address = inAddress;
            }
                       
            return address;
            
        }catch (Exception e) {
            system.debug(e.getmessage());
            return null;                       
        } 
    }
}