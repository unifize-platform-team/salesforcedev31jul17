public class RequestProviderAutodeskFileUpload{
    public static HttpRequest getRequest(Id myUploadItemId){
        AutodeskIntegrationProperties__c autodeskIntegratonProperties = AutodeskIntegrationProperties__c.getInstance();
        
        myUpload_Item__c myUploadItem = [SELECT Id, Name, Bucket_Name__c, AWS_Key__c, Key__c, File_Name__c, VersionId__c FROM myUpload_Item__c WHERE Id = :myUploadItemId];
        
        String key = '';
        if(myUploadItem.AWS_Key__c <> null && myUploadItem.AWS_Key__c <> ''){
            key = myUploadItem.AWS_Key__c;
        }
        else{
            key = myUploadItem.Key__c + '/' + myUploadItem.File_Name__c;
        }
        
        String body = '';
        body += '{';
        body +=     '"Records":[';
        body +=         '{';
        body +=             '"s3ObjectKey": "'+key+'",';
        body +=             '"s3BucketName": "'+myUploadItem.Bucket_Name__c+'",';
        body +=             '"versionId": "'+myUploadItem.VersionId__c+'",';
        body +=             '"autodeskBucketKey": "'+autodeskIntegratonProperties.BucketKey__c+'",';
        body +=             '"autodeskClientId": "'+autodeskIntegratonProperties.ClientId__c+'",';
        body +=             '"autodeskClientSecret": "'+autodeskIntegratonProperties.ClientSecret__c+'"';
        body +=         '}';
        body +=     ']';   
        body += '}';
        
        HttpRequest request = new HttpRequest();
        request.setEndpoint(autodeskIntegratonProperties.FileUploadEndPoint__c);
        request.setMethod('POST');
        request.setBody(body);
        request.setTimeout(60000);
        
        return request;
    }
}