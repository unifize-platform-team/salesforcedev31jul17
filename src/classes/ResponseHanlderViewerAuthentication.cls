public class ResponseHanlderViewerAuthentication{
     public static String processResponse(HttpResponse response){
         String responseString = response.getBody();
         system.debug(responseString);
         Map<String, object> bodyMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
      
         return (String) bodyMap.get('access_token');
     }
}