public class AutodeskFileUpload implements Queueable, Database.AllowsCallouts {
    Id uploadItemId = null;
    public AutodeskFileUpload(Id uploadItemId){
        this.uploadItemId = uploadItemId;
    }
    
    
    public void execute(QueueableContext context) {
        AutodeskIntegrationHandler.uploadFileToAutodesk(uploadItemId);
    }
}