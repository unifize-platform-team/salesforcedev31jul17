public class PositionHelper{
    
    public static void autoSharetoRecruiter(map<id,position__c>postNewMap,map<id,position__c>postOldMap ){
        list<candidate__c>cdList = new list<candidate__c>();
        for(position__c pst:[Select id,Recruiter_1__c,Recruiter_2__c,Recruiter_3__c,Recruiter_10__c,Recruiter_4__c,
                             Recruiter_5__c, Recruiter_6__c,Recruiter_7__c,Recruiter_8__c,Recruiter_9__c,
                             (select id,candidate__c from Job_Applications__r)from position__c where id in :postNewMap.keyset()]){
                                 if((pst.Recruiter_1__c !=postOldMap.get(pst.id).Recruiter_1__c) || (pst.Recruiter_2__c !=postOldMap.get(pst.id).Recruiter_2__c) ||
                                    (pst.Recruiter_3__c !=postOldMap.get(pst.id).Recruiter_3__c) || (pst.Recruiter_4__c !=postOldMap.get(pst.id).Recruiter_4__c) ||
                                    (pst.Recruiter_5__c !=postOldMap.get(pst.id).Recruiter_5__c) || (pst.Recruiter_6__c !=postOldMap.get(pst.id).Recruiter_6__c) ||
                                    (pst.Recruiter_7__c !=postOldMap.get(pst.id).Recruiter_7__c) || (pst.Recruiter_8__c !=postOldMap.get(pst.id).Recruiter_8__c) ||
                                    (pst.Recruiter_9__c !=postOldMap.get(pst.id).Recruiter_9__c) || (pst.Recruiter_10__c !=postOldMap.get(pst.id).Recruiter_10__c)){
                                        for(Job_application__c jpp:pst.Job_applications__r){		 
                                            if(null<>jpp.candidate__c){
                                                candidate__c cd = new candidate__c (id=jpp.candidate__c);
                                                if(pst.Recruiter_1__c !=postOldMap.get(pst.id).Recruiter_1__c ){
                                                    cd.Recruiter_1__c =pst.Recruiter_1__c;
                                                }
                                                if(pst.Recruiter_2__c !=postOldMap.get(pst.id).Recruiter_2__c ){
                                                    cd.Recruiter_2__c =pst.Recruiter_2__c;
                                                }
                                                if(pst.Recruiter_3__c !=postOldMap.get(pst.id).Recruiter_3__c ){
                                                    cd.Recruiter_3__c =pst.Recruiter_3__c;
                                                }
                                                if(pst.Recruiter_4__c !=postOldMap.get(pst.id).Recruiter_4__c ){
                                                    cd.Recruiter_4__c =pst.Recruiter_4__c;
                                                }
                                                if(pst.Recruiter_5__c !=postOldMap.get(pst.id).Recruiter_5__c ){
                                                    cd.Recruiter_5__c =pst.Recruiter_5__c;
                                                }
                                                if(pst.Recruiter_6__c !=postOldMap.get(pst.id).Recruiter_6__c ){
                                                    cd.Recruiter_6__c =pst.Recruiter_6__c;
                                                }
                                                if(pst.Recruiter_7__c !=postOldMap.get(pst.id).Recruiter_7__c ){
                                                    cd.Recruiter_7__c =pst.Recruiter_7__c;
                                                }
                                                if(pst.Recruiter_8__c !=postOldMap.get(pst.id).Recruiter_8__c ){
                                                    cd.Recruiter_8__c =pst.Recruiter_8__c;
                                                }
                                                if(pst.Recruiter_9__c !=postOldMap.get(pst.id).Recruiter_9__c ){
                                                    cd.Recruiter_9__c =pst.Recruiter_9__c;
                                                }
                                                if(pst.Recruiter_10__c !=postOldMap.get(pst.id).Recruiter_10__c ){
                                                    cd.Recruiter_10__c =pst.Recruiter_10__c;
                                                } 
												cdList.add(cd);
                                            }
                                        }
                                    }
                             }
        if(cdList.size()>0){
			database.update(cdList,false);
		}
    }
    
}