@isTest(SeeAllData=false)
private class S3FolderStructurePartsTest{
     @testSetup
    static void setup(){
        Account acc = new Account(Name = 'Test');
        insert acc;
        
        Part__c part = new Part__c( 
            Account__c = acc.Id
        );
        insert part;
    }    
    
    
    private static testMethod void testOne() {
        Part__c part = [SELECT Id FROM Part__c LIMIT 1];
        S3FolderStructureParts s3FolderStructure = new S3FolderStructureParts();
        s3FolderStructure.getFolderStructure(part.Id);
    }
}