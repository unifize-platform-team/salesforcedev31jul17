public class ResponseHandlerAutodeskFileUpload{
    public static void processResponse(HttpResponse response, Id myUploadItemId){
        String responseString = response.getBody();
        System.debug(responseString);
        ResponseWrapperAutodeskMain responseParsed = (ResponseWrapperAutodeskMain) JSON.deserialize(responseString, ResponseWrapperAutodeskMain.Class);
        
        myUpload_Item__c myUploadItem = [SELECT Id, No_of_Retries__c FROM myUpload_Item__c WHERE Id =:myUploadItemId];
        
        if(responseParsed.code == 200){
            myUploadItem.Autodesk_Bucket__c = responseParsed.returnValue.bucketKey;
            myUploadItem.Autodesk_ContentType__c = responseParsed.returnValue.contentType;
            myUploadItem.Autodesk_Location__c = responseParsed.returnValue.location;
            myUploadItem.Autodesk_ObjectId__c = responseParsed.returnValue.objectId;
            myUploadItem.Autodesk_ObjectKey__c = responseParsed.returnValue.objectKey;
            myUploadItem.Autodesk_SHA1__c = responseParsed.returnValue.sha1;
            myUploadItem.Status__c = 'Autodesk Upload Successful';
            myUploadItem.ErrorCode__c = null;
            myUploadItem.ErrorMessage__c = '';
            myUploadItem.ErrorCodeText__c = '';
            myUploadItem.No_of_Retries__c = 0;
        }
        else{
            myUploadItem.Status__c = 'Autodesk Upload Failed';
            myUploadItem.ErrorCode__c = responseParsed.code;
            myUploadItem.ErrorCodeText__c = 'FILE UPLOAD FAILED';
            myUploadItem.ErrorMessage__c = responseParsed.errorMessage;
            myUploadItem.No_of_Retries__c = myUploadItem.No_of_Retries__c == null ? 0 : myUploadItem.No_of_Retries__c + 1;
        }
        update myUploadItem;
    }
}