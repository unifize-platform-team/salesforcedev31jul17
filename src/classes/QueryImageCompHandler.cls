public class QueryImageCompHandler {
    @auraEnabled
    public static uniCHECK_Query__c getImageDetails(String recordId){
        return [SELECT After_Image__c,Before_Image__c FROM uniCHECK_Query__c WHERE id=:recordId] ;      
    }
}