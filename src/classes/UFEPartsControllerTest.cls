@isTest
public class UFEPartsControllerTest {
    static testMethod void myUnitTest() {
        
        Account a = new Account(Name = 'Test Account Joe ');
        insert a;
        
        List<recordtype> recList=[SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'myProject__c'];
        myProject__c  m=new myProject__c ();
        m.RecordTypeId=recList[0].id;
        m.Status__c='Upload';
        m.Partner__c=a.id;
        insert m;
        
        
        Part__c mp=new Part__c();
        mp.Account__c=a.id;
       // mp.myMaterial__c=m.id;
        mp.Drawing_No__c='DOO1';
        mp.myPart_Status__c='Active';
        insert mp;
        
        String s = 'str';
        UFEPartsController.getTasksOfProject(string.valueOf(m.id));
        UFEPartsController.markTasksAsCompleted(s,string.valueOf(m.id));    
    }  
}