public class AutodeskManifest implements Queueable, Database.AllowsCallouts {
    Id uploadItemId = null;
    public AutodeskManifest(Id uploadItemId){
        this.uploadItemId = uploadItemId;
    }
    
    
    public void execute(QueueableContext context) {
        AutodeskIntegrationHandler.manifest(uploadItemId);
    }
}