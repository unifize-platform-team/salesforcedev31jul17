public class AttachedFileThumbnailGridCompForMyItem {
	@AuraEnabled
    public static List<myUpload_Item__c> getUploadItems(String recordId){
        Set<String> uploadItemIdSet = new Set<String>();
        for(myItem_Version__c version :[SELECT Id, Current_Attached_File__c FROM myItem_Version__c WHERE myItem__c =:recordId]){
            uploadItemIdSet.add(version.Current_Attached_File__c);
        }
        
        List<myUpload_Item__c> listToReturn = new List<myUpload_Item__c>();
        
        List<myUpload_Item__c> listOne = new List<myUpload_Item__c>();
        List<myUpload_Item__c> listTwo = new List<myUpload_Item__c>();
        
        List<myUpload_Item__c> uploadItemList =  [SELECT Id, Name, File_Name__c, File_Type__c, File_Size__c, CreatedDate, Status__c 
            FROM myUpload_Item__c 
            WHERE Id IN :uploadItemIdSet 
            ORDER BY CreatedDate DESC];
            
        Map<String, Attachment> attmntMap = new Map<String, Attachment>();
        for(Attachment attmnt :[SELECT Id, ParentId FROM Attachment WHERE ParentId IN :uploadItemIdSet]){
            attmntMap.put(attmnt.ParentId, attmnt);
        }
        
        for(myUpload_Item__c uploadItem :uploadItemList){
            uploadItem.AttachmentId__c = attmntMap.containsKey(uploadItem.Id) ? attmntMap.get(uploadItem.Id).Id : null;
            if(uploadItem.Status__c == 'Thumbnail Creation Successful'){
                listOne.add(uploadItem);
            }
            else{
                listTwo.add(uploadItem);
            }
        }  
        
        listToReturn.addAll(listOne);
        listToReturn.addAll(listTwo);
        
        return listToReturn;
    }
}