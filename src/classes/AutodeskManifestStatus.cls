public class AutodeskManifestStatus implements Queueable, Database.AllowsCallouts {
    Id uploadItemId = null;
    public AutodeskManifestStatus(Id uploadItemId){
        this.uploadItemId = uploadItemId;
    }
    
    
    public void execute(QueueableContext context) {
        AutodeskIntegrationHandler.manifestStatus(uploadItemId);
    }
}