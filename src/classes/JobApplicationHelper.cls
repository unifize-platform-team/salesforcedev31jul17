public class JobApplicationHelper {
    
    public static void autoSharetoRecruiter(map<id,job_application__c>jNMap,map<id,job_application__c>jOMap,String operation){
        list<candidate__c> candShareList = new list<candidate__c>();
        set<id>cndSet = new set<id>();
        if(operation!='Delete'){
            for(job_application__c app :[select id ,candidate__c,position__r.Recruiter_1__c,position__r.Recruiter_2__c,
                                         position__r.Recruiter_3__c,position__r.Recruiter_10__c,position__r.Recruiter_4__c,
                                         position__r.Recruiter_5__c, position__r.Recruiter_6__c,position__r.Recruiter_7__c,
                                         position__r.Recruiter_8__c,position__r.Recruiter_9__c from job_application__c
                                         where id in :  jNMap.keyset()]){
                                             if(null<>app.candidate__c ){
                                                 if(operation=='Update'){
                                                     if(app.candidate__c !=jOMap.get(app.id).candidate__c){
                                                         cndSet.add(jOMap.get(app.id).candidate__c);     
                                                     }
                                                 } 
                                                 
                                                 candidate__c cnd = new candidate__c(id=app.candidate__c);
                                                 cnd.Recruiter_1__c=app.Position__r.Recruiter_1__c;
                                                 cnd.Recruiter_2__c=app.Position__r.Recruiter_2__c;
                                                 cnd.Recruiter_3__c=app.Position__r.Recruiter_3__c;
                                                 cnd.Recruiter_4__c=app.Position__r.Recruiter_4__c;
                                                 cnd.Recruiter_5__c=app.Position__r.Recruiter_5__c;
                                                 cnd.Recruiter_6__c=app.Position__r.Recruiter_6__c;
                                                 cnd.Recruiter_7__c=app.Position__r.Recruiter_7__c;
                                                 cnd.Recruiter_8__c=app.Position__r.Recruiter_8__c;
                                                 cnd.Recruiter_9__c=app.Position__r.Recruiter_9__c;
                                                 cnd.Recruiter_10__c=app.Position__r.Recruiter_10__c;
                                                 candShareList.add(cnd);
                                             } 
                                         }                                       
        } else {
            for(job_application__c japp:jOMap.values()){
                cndSet.add(japp.candidate__c);
            }
        }
        if(cndSet.size()>0){
            list<candidate__c>cdList= deleteShare(cndSet);
            if(null<>cdList){
                candShareList.addAll(cdList);
            }
        }
        if(candShareList.size()>0) {                        
            database.update(candShareList,false);
        }
    }
    public static list<candidate__c>deleteShare(set<id>cIdSet){
        
        list<candidate__c> cIdRSet = new list<candidate__c>();
        for(id cid:cIdSet){
            candidate__c cnd = new candidate__c(id=cId);
            cnd.Recruiter_1__c=null;
            cnd.Recruiter_2__c=null;
            cnd.Recruiter_3__c=null;
            cnd.Recruiter_4__c=null;
            cnd.Recruiter_5__c=null;
            cnd.Recruiter_6__c=null;
            cnd.Recruiter_7__c=null;
            cnd.Recruiter_8__c=null;
            cnd.Recruiter_9__c=null;
            cnd.Recruiter_10__c=null;
            cIdRSet.add(cnd);                                        
        }
        return cIdRSet;
    }
}