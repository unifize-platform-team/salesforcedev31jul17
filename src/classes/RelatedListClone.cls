public class RelatedListClone {
 @AuraEnabled
    public static String getClonemyProject(String oldId) {
        system.debug('oldId ' + oldId);
        string obj1 ='myProject__c';
        string obj2 = 'myProject_Part__c';   
        string query= formSOQL(obj1);
        string innerQuery =formSOQL(obj2);
        string soql=query+',('+ innerQuery +' from myProject_Parts__r ' +')';
        soql += ' FROM myProject__c';
        // Add on a WHERE/ORDER/LIMIT statement as needed
        soql += ' WHERE Id =  \''+ oldId +'\'  LIMIT 1'; 
        system.debug('soql??' +soql);  
        myProject__c project = database.query(soql);
        system.debug(project);
        myProject__c newprj = new myProject__c();
            newprj = newprj.clone(false, false, false, false);
            newprj.Status__c ='Upload';
            newprj.Required_Date_Time__c =NULL;

        //newprj.Name='new clone';
        insert newprj;   
       
       if(project.myProject_Parts__r.size()>0){
           innerQuery += ' FROM myProject_Part__c';
           innerQuery += ' WHERE myProject__c =  \''+ oldId +'\'  '; 
           system.debug('innerQuery??' +innerQuery);
           list<myProject_Part__c> mpList = new list<myProject_Part__c>();
           list<myProject_Part__c> mppList = new list<myProject_Part__c>();
           mpList=database.query(innerQuery);
           for(myProject_Part__c mpc: mpList){
               myProject_Part__c mpp = mpc.clone(false, false, false, false);
               mpp.myProject__c = newprj.id;
               mppList.add(mpp);
           }
           
           if(mppList.size()>0){
               database.Insert(mppList);
           }
       }
       
       
        
        return newprj.id;
        }
        
        public static string formSOQL(String objName){
            // Initialize setup variables
        String objectName =objName;  // modify as needed
        String query = 'SELECT';
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        
        // Grab the fields from the describe method and append them to the queryString one by one.
        for(String s : objectFields.keySet()) {
           query += ' ' + s + ', ';
        }
       
        
        // Strip off the last comma if it exists.
        if (query.subString(query.Length()-1,query.Length()) == ','){
            query = query.subString(0,query.Length()-1);
        }
        
        query = query.trim().removeEnd(',');
        system.debug(query);  
          return query;     
            
        }
}