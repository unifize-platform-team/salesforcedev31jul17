public class ResponseWrapperAutodeskMain{
    public Integer code;
    public String errorMessage;
    public ResponseWrapperAutodesk returnValue;
    
    public ResponseWrapperAutodeskMain(){
        returnValue = new ResponseWrapperAutodesk();
    }
}