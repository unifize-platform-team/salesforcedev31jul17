global class DeleteOrphanAttachedFilesBatchScheduler implements Schedulable {
    global void execute(SchedulableContext ctx) {
        AttachedFilesDeleteBatchControl__c attachedFileDeleteBatchControl = AttachedFilesDeleteBatchControl__c.getInstance();
        if(attachedFileDeleteBatchControl.Execute_Delete_Batch_Job__c == true){
            Database.executeBatch(new DeleteOrphanAttachedFilesBatch());
        }
    }
}