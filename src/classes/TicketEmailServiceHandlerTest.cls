@isTest
public class TicketEmailServiceHandlerTest{

    public static testMethod void testRun(){
        //Account acc = new Account(name = 'Test Account', Priority = 1, Status = 1);
        //insert acc;
        
        //Contact conobj = new Contact(FirstName = 'portal', LastName = 'user', AccountId = acc.Id, Email = 'portaluser@testorg.com');
        //insert conobj;
        
        Profile prof = [Select id from profile where name = 'System Administrator'];
        
        User u = new User(
             ProfileId = prof.id,
             LastName = 'user',
             Email = 'portaluser@testorg.com',
             Username = 'portaluser@testorg.com' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US'
        );
        
        Test.startTest();
        system.runAs(u){
            // create a new email 
            Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
            Messaging.InboundEmail email = new Messaging.InboundEmail() ;
            
            envelope.fromAddress = 'portaluser@testorg.com';
            envelope.toAddress = 'qual_logistics@asfd.com';
            
            email.subject = 'Subject asdas asdasd ';
            email.plainTextBody = 'Test email';
            email.fromname = 'portal user';
            email.toAddresses = new String[] {'someaddress@email.com'};
            email.ccAddresses = new String[] {'one@email.com','two@email.com'};
            email.fromAddress = 'portaluser@testorg.com';
            String csv = 'this is just a test';
            
            // add an attachment
            Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
            attachment.body = blob.valueOf(csv);
            attachment.fileName = 'data.csv';
            attachment.mimeTypeSubType = 'text/plain';
      
            email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
      
            Messaging.InboundEmail.TextAttachment tattachment = new Messaging.InboundEmail.TextAttachment();
            tattachment.body = csv;
            tattachment.fileName = 'data.csv';
            tattachment.mimeTypeSubType = 'text/plain';
      
            email.textAttachments = new Messaging.inboundEmail.TextAttachment[] { tattachment };
      
            // call the email service class and test it with the data in the testMethod
            TicketEmailServiceHandler emailProcess = new TicketEmailServiceHandler();
            Messaging.InboundEmailResult result = emailProcess.handleInboundEmail(email,envelope);
            Messaging.InboundEnvelope envelope1 = new Messaging.InboundEnvelope();
            envelope1.toAddress = 'test_technical@asfd.com';
            TicketEmailServiceHandler emailProcessTech = new TicketEmailServiceHandler();
            Messaging.InboundEmailResult result1 = emailProcessTech.handleInboundEmail(email,envelope1);
            System.assert(result1.success == true);
            Test.stopTest();
        }
    }
}