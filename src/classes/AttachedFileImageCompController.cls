public class AttachedFileImageCompController {
	@AuraEnabled
    public static myUpload_Item__c getUploadItemDetails(String recordId){
        List<myUpload_Item__c> uploadItemList = [SELECT Id, Name, File_Name__c, File_Type__c, CreatedDate FROM myUpload_Item__c WHERE Id = :recordId];
    
        if(uploadItemList.size() > 0){
            return uploadItemList[0];
        }
        else{
            return new myUpload_Item__c();
        }
    }
    
    @AuraEnabled
    public static String getAttachmentId(String recordId){
        String attachmentId = null;
        List<Attachment> attachmentList = [SELECT Id FROM Attachment WHERE ParentId = :recordId];
        if(attachmentList.size() > 0){
            attachmentId = attachmentList[0].Id;
        }
        
        return attachmentId;
    }
}