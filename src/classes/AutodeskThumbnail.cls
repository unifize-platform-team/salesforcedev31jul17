public class AutodeskThumbnail implements Queueable, Database.AllowsCallouts {
    Id uploadItemId = null;
    public AutodeskThumbnail(Id uploadItemId){
        this.uploadItemId = uploadItemId;
    }
    
    
    public void execute(QueueableContext context) {
        AutodeskIntegrationHandler.thumbnail(uploadItemId);
    }
}