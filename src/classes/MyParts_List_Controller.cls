public with sharing class MyParts_List_Controller {
	@AuraEnabled
    public static String getMyProjectParts(String srcProjectId, String destProjectId) {
    	
    	List<Part__c> myPartsList = new List<Part__c>();
    	List<myProject_Part__c> myProjectPartsList = new List<myProject_Part__c>();
    	List<MyProjectPartsWrapper> myProjectPartsWrapperList = new List<MyProjectPartsWrapper>();
    	myProject__c myProject = new myProject__c();
    	
    	if(srcProjectId != null && destProjectId != null){
        	myProjectPartsList = [SELECT Id, myPart__c, myPart__r.Name, myProject__r.Project_Name__c, myProject__r.Name, myPart__r.Drawing_No__c, 
										 myPart__r.myMaterial__r.Name,  
										 Quantity__c, myPart__r.Standard_Quantity__c, myPart__r.myPart_Status__c 
								  FROM myProject_Part__c WHERE myProject__c = :srcProjectId];
		    if(myProjectPartsList.size() > 0){
		 		for(myProject_Part__c myProjectPart : myProjectPartsList){
		 			MyProjectPartsWrapper myProjectPartsWrapperRec = new MyProjectPartsWrapper();
		 			myProjectPartsWrapperRec.sProjectPartId = myProjectPart.Id;
		 			myProjectPartsWrapperRec.sSelected = false;
		 			myProjectPartsWrapperRec.sPartId = myProjectPart.myPart__r.Name;
		 			myProjectPartsWrapperRec.sProjectName = myProjectPart.myProject__r.Project_Name__c;
		 			myProjectPartsWrapperRec.sProjectId = myProjectPart.myProject__r.Name;
		 			myProjectPartsWrapperRec.sDrawingName = myProjectPart.myPart__r.Drawing_No__c;
		 			myProjectPartsWrapperRec.sMaterial = myProjectPart.myPart__r.myMaterial__r.Name;
		 			//myProjectPartsWrapperRec.sProcess = myProjectPart.myPart__r.Process_Name__c;
		 			//myProjectPartsWrapperRec.sThickness = String.valueOf(myProjectPart.myPart__r.Thickness_mm__c);
		 			myProjectPartsWrapperRec.sQuantity = String.valueOf(myProjectPart.Quantity__c);
		 			myProjectPartsWrapperRec.sStatus = myProjectPart.myPart__r.myPart_Status__c;
		 			myProjectPartsWrapperList.add(myProjectPartsWrapperRec);
		 		}
		 	}
		}
		else{
			myProject = [Select Id, Name, Project_Name__c From myProject__c Where Id = :srcProjectId];
			myPartsList = [Select Id, Name, Drawing_No__c, myMaterial__r.Name, 
								  Standard_Quantity__c, myPart_Status__c
								  FROM Part__c];
		  	if(myPartsList.size() > 0){
		 		for(Part__c myPart : myPartsList){
                    List<Unit_of_Measurement__c> uomList=new List<Unit_of_Measurement__c>();
                    //Unit_of_Measurement__c thickness=new Unit_of_Measurement__c(name=String.valueOf(myPart.Thickness_mm__c));
                    

		 			MyProjectPartsWrapper myProjectPartsWrapperRec = new MyProjectPartsWrapper();
		 			myProjectPartsWrapperRec.sProjectPartId = myPart.Id;
		 			myProjectPartsWrapperRec.sSelected = false;
		 			myProjectPartsWrapperRec.sPartId = myPart.Name;
		 			myProjectPartsWrapperRec.sProjectName = myProject.Project_Name__c;
		 			myProjectPartsWrapperRec.sProjectId = myProject.Name;
		 			myProjectPartsWrapperRec.sDrawingName = myPart.Drawing_No__c;
		 			myProjectPartsWrapperRec.sMaterial = myPart.myMaterial__r.Name;
		 			//myProjectPartsWrapperRec.sProcess = myPart.Process_Name__c;
		 			//myProjectPartsWrapperRec.sThickness = String.valueOf(myPart.Thickness_mm__c);
		 			myProjectPartsWrapperRec.sQuantity = String.valueOf(myPart.Standard_Quantity__c);
		 			myProjectPartsWrapperRec.sStatus = myPart.myPart_Status__c;
		 			myProjectPartsWrapperList.add(myProjectPartsWrapperRec);
		 		}
		 	}
            
		}
        return JSON.serialize(myProjectPartsWrapperList);
    }
    
	@AuraEnabled
    public static String addProjectParts1(String srcProjectId, String destProjectId, String myProjectPartsSelMap,boolean add,boolean overwrite) {
       	System.debug('==============> srcProjectId : ' + srcProjectId);
       	System.debug('==============> destProjectId : ' + destProjectId);
       	System.debug('==============> myProjectPartsSelMap : ' + myProjectPartsSelMap);
       	
       	Map<String, Object> mapSelProjectPartIdVsQuantity = (Map<String, Object>)JSON.deserializeUntyped(myProjectPartsSelMap);
       	Set<String> selProjectPartSet = mapSelProjectPartIdVsQuantity.keySet();
       	List<myProject_Part__c> createMyProjectPartList = new List<myProject_Part__c>();
       	if(srcProjectId != null && destProjectId != null){
       		List<myProject_Part__c> selMyProjectPartList = [Select Id, myPart__c From myProject_Part__c Where  id in :selProjectPartSet];
	       	for(myProject_Part__c myProjectPart : selMyProjectPartList){
	       		myProject_Part__c createMyProjectPart = new myProject_Part__c();
	       		createMyProjectPart.myPart__c = myProjectPart.myPart__c;
       			createMyProjectPart.myProject__c = destProjectId;
       			if(mapSelProjectPartIdVsQuantity.get(myProjectPart.Id)!=null){
		       		if(add)createMyProjectPart.Quantity__c = Decimal.valueOf((String) mapSelProjectPartIdVsQuantity.get(myProjectPart.Id));
	                if(overwrite){
	                    if(createMyProjectPart.Quantity__c==null)createMyProjectPart.Quantity__c=0;
	                    createMyProjectPart.Quantity__c += Decimal.valueOf((String) mapSelProjectPartIdVsQuantity.get(myProjectPart.Id));
	                }
       			}
                createMyProjectPartList.add(createMyProjectPart);
	       	}
       	}
       	else{
       		for(String sPartId : selProjectPartSet){
	       		myProject_Part__c createMyProjectPart = new myProject_Part__c();
	       		createMyProjectPart.myPart__c = sPartId;
       			createMyProjectPart.myProject__c = srcProjectId;
	       		if(add)createMyProjectPart.Quantity__c = Decimal.valueOf((String) mapSelProjectPartIdVsQuantity.get(sPartId));
                if(overwrite){
                    if(createMyProjectPart.Quantity__c==null)createMyProjectPart.Quantity__c=0;
                    createMyProjectPart.Quantity__c += Decimal.valueOf((String) mapSelProjectPartIdVsQuantity.get(sPartId));
                }
	       		createMyProjectPartList.add(createMyProjectPart);
	       	}
       	}
       	System.debug('==============> mapSelProjectPartIdVsQuantity : ' + mapSelProjectPartIdVsQuantity);
       	System.debug('==============> createMyProjectPartList : ' + createMyProjectPartList);
       	insert createMyProjectPartList;
       	
       	if(srcProjectId != null && destProjectId != null){
        	return destProjectId;
       	}
       	else{
       		return srcProjectId;
       	}
    }
    
    @Auraenabled //returns true is parts already exist
    public static boolean  checkExistingParts(String srcProjectId, String myProjectPartsSelMap){
        Map<String, Object> mapSelProjectPartIdVsQuantity = (Map<String, Object>)JSON.deserializeUntyped(myProjectPartsSelMap);
       	Set<String> selProjectPartSet = mapSelProjectPartIdVsQuantity.keySet();
        if(srcProjectId != null){
       		List<myProject_Part__c> selMyProjectPartList = [Select Id, myPart__c From myProject_Part__c Where myPart__c In :selProjectPartSet];
            if(selMyProjectPartList!=null && selMyProjectPartList.size()>0){
                return true;
            }
       	}
        return false;
    } 
    
    public class MyProjectPartsWrapper{
    	public String sProjectPartId;
    	public Boolean sSelected;
    	public String sPartId;
    	public String sProjectName;
    	public String sProjectId;
    	public String sDrawingName;
    	public String sMaterial;
    	public String sProcess;
    	public String sThickness;
    	public String sQuantity;
    	public String sStatus;
    }
}