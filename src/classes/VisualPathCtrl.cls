public class VisualPathCtrl {
 

	@AuraEnabled
    public static responseWrapper getStatus(String pickListField,string recordId) {
        //response wrapper
        responseWrapper rw=new responseWrapper();
        
        //get sobject name from Id
        Id sId=recordId;
        string object_name=sId.getSObjectType().getDescribe().getName();
       //new list for holding all of the picklist options
        List <string> options = new List <String> ();
        /*
        sObject sObj = Schema.getGlobalDescribe().get(object_name).newSObject();
        //grab the sobject that was passed
        Schema.sObjectType sobject_type = sObj.getSObjectType();
        //describe the sobject
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); 
        //get a map of fields for the passed sobject
        Map < String, Schema.SObjectField > field_map = sobject_describe.fields.getMap();
        //grab the list of picklist values for the passed field on the sobject
        List < Schema.PicklistEntry > pick_list_values = field_map.get(pickListField).getDescribe().getPickListValues(); 
        //for all values in the picklist list
        for (Schema.PicklistEntry a: pick_list_values) { 
             //add the label to our final list
            options.add(a.getLabel());
        }*/
        options = VisualPathPicklistDescriber.describe(recordId, pickListField);

		//Stringifying The Array and Assigning It To A String 
        string opts = JSON.serialize(options);
        
        //Get current status value from the record id;
        List<Sobject> objList=database.query('select id,'+pickListField+' from '+object_name+' where id=\''+recordId+'\'');
        if(objList!=null && !objList.isEmpty()){
            rw.currentStatus=string.valueof(objList[0].get(pickListField));
        }
        rw.options=opts;
        
		return rw;
    }
    
    public class responseWrapper{
        @AuraEnabled public string options{get;set;}
        @AuraEnabled public string currentStatus{get;set;}
    }


}