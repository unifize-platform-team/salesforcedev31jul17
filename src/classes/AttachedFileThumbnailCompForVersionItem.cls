public class AttachedFileThumbnailCompForVersionItem {
	@AuraEnabled
    public static myItem_Version__c getAttachedFileDetails(String recordId){
        return [SELECT Id, Current_Attached_File__c FROM myItem_Version__c WHERE Id = :recordId];
    }
}