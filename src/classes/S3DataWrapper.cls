global class S3DataWrapper{
    global String uploadItemId;
    global String location;
    global String key;
    global String fileName;
    global String recordId;
    global String eTag;
    global String relatedFieldName;
    global String bucketName;
    global String fileSize;
    global String versionId;
    global Integer numberOfFiles;
    global String onSaveController;
}