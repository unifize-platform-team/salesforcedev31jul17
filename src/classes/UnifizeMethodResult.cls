public with sharing class UnifizeMethodResult 
{
	/**
     * This class is a general purpose class to be used as a formal parameter in the calling method to 
     * communicate the precise information of the method execution to the calling method to enable 
     * communicating preicse information to the end-users. 
     * 
     * The calling method creates new object and passes the object reference to the called method. The
     * called in method should set the member variables based on the execution result before returning.
     * 
     * It's very important to remember that called method CAN NOT create a new object of this type and
     * assign value which is against the supported phylosophy of Apex method calling convention.
     * 
     * Author : venkat.reddy@unifize.com
     * Creation Date : 07-04-2017
     * Comments : 
     */
    
    /*
     * Called method returns below values for result
     *  1 - success
     *  0 - failure
     */
    public boolean result {get; set;}
	/*
     * Called method returns below values for errorID in case of result is a failure. This value needs to be
     * globally set using some number offsets for each functional modules - integer values
     */
    public integer errorID {get; set;}
    
    /*
     * Called method can set and return any error information for the failure. This is only an additional
     * error/failure description information on top of errorID.
     */
    public string errorInfo {get; set;}
    
}