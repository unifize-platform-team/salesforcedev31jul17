public class S3FolderStructureParts implements S3FolderStructure{
    public String getFolderStructure(String recordId){
        Part__c part = [SELECT Name, Account__r.Account_ID__c FROM Part__c WHERE Id = :recordId];
        
        String folderStructure = 'Jobs/Part_Jobs/'+part.Account__r.Account_ID__c+'/'+part.Name+'/';
        
        return folderStructure;
    }
}