public class MyProjectQueryHandler {
    @auraEnabled
    public static List<myProject_Query_Item__c> getMyPorjectQuery(String recordId){
        return [SELECT Id,RecordTypeId,Name,Accept_Reject__c,Resolution_Type__c,Resolution_Action_Taken__c,Question__c,Response__c FROM myProject_Query_Item__c WHERE myProject_Query__c =: recordId];
    }
    @auraEnabled
    public static void updateAcceptRejectStatus(String recordId,String updatedValue)
    {
        //myProject_Query__c query=[SELECT Accept_Reject_Resolution__c from myProject_Query__c where id=:recordId];
        //query.Accept_Reject_Resolution__c=updatedValue;
        //myProject_Query_Item__c query=[SELECT Accept_Reject__c from myProject_Query_Item__c WHERE myProject_Query__c=:recordId];
        myProject_Query_Item__c queryItem = new myProject_Query_Item__c(Id = recordId, Accept_REject__c = updatedValue);
        System.debug('data : '+queryItem);
        update queryItem;
    }
    
	@auraEnabled
    public static void updateComments(String recordId,String updatedCommentValue)
    {
        /*myProject_Query__c query=[SELECT Account_Notes_Comments__c from myProject_Query__c where id=:recordId];
        query.Account_Notes_Comments__c=updatedCommentValue;
        System.debug('data : '+query);*/
        myProject_Query_Item__c queryItem = new myProject_Query_Item__c(Id = recordId, Comments__c = updatedCommentValue);
        System.debug('data comment : '+queryItem);
        update queryItem;
    }

}