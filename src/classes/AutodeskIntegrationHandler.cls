public class AutodeskIntegrationHandler{
    public static void uploadFileToAutodesk(Id myUploadItemId){
        HttpRequest request = RequestProviderAutodeskFileUpload.getRequest(myUploadItemId);
        HttpResponse response = (new Http()).send(request);
        ResponseHandlerAutodeskFileUpload.processResponse(response, myUploadItemId);
    }
    
    
    public static void authenticate(){
        HttpRequest request = RequestProviderAutodeskAuthentication.getRequest();
        HttpResponse response = (new Http()).send(request);
        ResponseHanlderAutodeskAuthentication.processResponse(response);
    }
    
    
    public static void manifest(Id myUploadItemId){
        if(AutodeskIntegrationHandler.getAccessToken() == null){
            AutodeskIntegrationHandler.authenticate();
        }
    
        HttpResponse response = AutodeskIntegrationHandler.getManifestResponse(myUploadItemId);
        
        if(response.getStatusCode() == 401){
            AutodeskIntegrationHandler.authenticate();
            
            response = AutodeskIntegrationHandler.getManifestResponse(myUploadItemId);
        }
        
        ResponseHandlerAutodeskManifest.processResponse(response, myUploadItemId);
    }
    
    
    public static HttpResponse getManifestResponse(Id myUploadItemId){
        HttpRequest request = RequestProviderAutodeskManifest.getRequest(myUploadItemId);
        return (new Http()).send(request);
    }
    
    
    public static void manifestStatus(Id myUploadItemId){
        if(AutodeskIntegrationHandler.getAccessToken() == null){
            AutodeskIntegrationHandler.authenticate();
        }
    
        HttpResponse response = AutodeskIntegrationHandler.getManifestStatusResponse(myUploadItemId);
        
        if(response.getStatusCode() == 401){
            AutodeskIntegrationHandler.authenticate();
            
            response = AutodeskIntegrationHandler.getManifestStatusResponse(myUploadItemId);
        }
        
        ResponseHandlerAutodeskManifestStatus.processResponse(response, myUploadItemId);
    }
    
    
    public static HttpResponse getManifestStatusResponse(Id myUploadItemId){
        HttpRequest request = RequestProviderAutodeskManifestStatus.getRequest(myUploadItemId);
        return (new Http()).send(request);
    }
    
    
    public static void thumbnail(Id myUploadItemId){
        if(AutodeskIntegrationHandler.getAccessToken() == null){
            AutodeskIntegrationHandler.authenticate();
        }
    
        HttpResponse response = AutodeskIntegrationHandler.getThumbnailResponse(myUploadItemId);
        
        if(response.getStatusCode() == 401){
            AutodeskIntegrationHandler.authenticate();
            
            response = AutodeskIntegrationHandler.getThumbnailResponse(myUploadItemId);
        }
        
        ResponseHandlerAutodeskThumbnail.processResponse(response, myUploadItemId);
    }
    
    
    public static HttpResponse getThumbnailResponse(Id myUploadItemId){
        HttpRequest request = RequestProviderAutodeskThumbnail.getRequest(myUploadItemId);
        return (new Http()).send(request);
    }
    
    
    public static String getAccessToken(){
        String accessToken = '';
        if(Test.isRunningTest()){
            accessToken = 'Test';
        }
        else{       
            accessToken = (String) Cache.Org.get('AutodeskAccessToken');
        }
        return accessToken;
    }
    
    
    public static String authenticateViewer(){
        HttpRequest request = RequestProviderViewerAuthentication.getRequest();
        HttpResponse response = (new Http()).send(request);
        return ResponseHanlderViewerAuthentication.processResponse(response);
    }
}