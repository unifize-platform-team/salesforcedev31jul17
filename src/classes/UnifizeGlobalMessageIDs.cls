public with sharing class UnifizeGlobalMessageIDs 
{
	/*
     * Class Description:
     * This class defines all global message IDs for Unifize app
     * 
     * Author : venkat.reddy@unifize.com
     * Creation Date : 20-04-2017
     * Comments : 
     *  The rationale behind numbering of message IDs is to define a 3 digit ID (starting with two leaning 0s)
     *  per feature
     */
    
    //box.com integration - 1xx
    public static Integer ENTERPRISE_API_CONNECTION_NULL = 100;
    public static Integer EXCEPTION_WHILE_CREATING_BOX_APP_USER = 101;
    public static Integer UNKNOWN_ERROR_ENTERPRISE_API_CONNECTION = 102;
    public static Integer EXCEPTION_WHILE_GETTING_ENTERPRISE_API_CONNECTION = 103;
    public static Integer UNKNOWN_ERROR_WHILE_CREATING_BOX_APP_USER = 104;
    
    //aws sdk and integration - 2xx
    
}