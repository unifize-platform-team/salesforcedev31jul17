public class ResponseHandlerAutodeskThumbnail{
    public static void processResponse(HttpResponse response, Id myUploadItemId){
        
        Integer statusCode = response.getStatusCode();
        
        myUpload_Item__c myUploadItem = [SELECT Id, No_of_Retries__c FROM myUpload_Item__c WHERE Id = :myUploadItemId];
        
        if(statusCode == 200){
            myUploadItem.Status__c = 'Thumbnail Creation Successful';
            myUploadItem.ErrorCode__c = null;
            myUploadItem.ErrorCodeText__c = '';
            myUploadItem.ErrorMessage__c = '';
            myUploadItem.No_of_Retries__c = 0;
            update myUploadItem;
            
            Attachment attmnt = new Attachment();
            attmnt.ParentId = myUploadItemId;
            attmnt.Body = response.getBodyAsBlob();
            attmnt.Name = 'Manifest Thumbnail-' + myUploadItemId+'.png';
            attmnt.ContentType = 'image/png';
            
            insert attmnt;
        }
        else{
            myUploadItem.ErrorCode__c = statusCode;
            myUploadItem.ErrorCodeText__c = 'INVALID/ CORRUPT FILE';
            myUploadItem.ErrorMessage__c = response.getBody();
            myUploadItem.Status__c = 'Thumbnail Creation Failed';
            myUploadItem.No_of_Retries__c = myUploadItem.No_of_Retries__c == null ? 0 : myUploadItem.No_of_Retries__c + 1;
            
            update myUploadItem;
        }    
    }    
}