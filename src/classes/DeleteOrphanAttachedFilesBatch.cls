global class DeleteOrphanAttachedFilesBatch implements Database.AllowsCallouts, Database.Batchable<sObject>{
    global Database.querylocator start(Database.BatchableContext BC){
        String[] types = new String[]{'myUpload_Item__c'};
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        
        List<String> listOfReferenceFields = new List<String>();
        
        for(Schema.DescribeSobjectResult res : results) {
            for(String fieldName :res.fields.getMap().keySet()){
                if(res.fields.getMap().get(fieldName).getDescribe().getType() == Schema.DisplayType.Reference && res.fields.getMap().get(fieldName).getDescribe().isCustom()){
                    listOfReferenceFields.add(fieldName);
                }
            }
        }
        
        String query = 'SELECT Id, ';
        
        for(String fieldName :listOfReferenceFields){
            query += fieldName + ', ';
        }
        
        query += 'Do_not_Delete__c FROM myUpload_Item__c WHERE Do_not_Delete__c = FALSE';
        
        for(String fieldName :listOfReferenceFields){
            query += ' AND ' + fieldName + '= null';
        }
        
        System.debug('query---'+query);
        
        return Database.getQueryLocator(query);
    }
        
        
    global void execute(Database.BatchableContext BC, List<myUpload_Item__c> scope){
        Database.DeleteResult[] drList = Database.delete(scope, false);
        
        List<myUpload_Item__c> uploadItemsToUpadte = new List<myUpload_Item__c>();
        for(Database.DeleteResult dr : drList){
            if(!dr.isSuccess()) {
                uploadItemsToUpadte.add(new myUpload_Item__c(Id = dr.getId(), Do_not_Delete__c = TRUE));
            }
        }
        
        if(uploadItemsToUpadte.size() > 0){
            update uploadItemsToUpadte;
        }
    }
    
    
    global void finish(Database.BatchableContext BC){
    }
}