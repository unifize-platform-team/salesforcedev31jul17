public class S3FilesDownloadCompController{
    public String file {get;set;}
    
    public S3FilesDownloadCompController(){
        Set<String> attachedFileIdSet = new Set<String>();
        
        if(ApexPages.currentPage().getParameters().containsKey('Id') && ApexPages.currentPage().getParameters().get('Id') <> null){
            attachedFileIdSet.add(ApexPages.currentPage().getParameters().get('Id'));
        }
    
        
        if(ApexPages.currentPage().getParameters().containsKey('Ids') && ApexPages.currentPage().getParameters().get('ids') <> null){
            attachedFileIdSet.addAll(ApexPages.currentPage().getParameters().get('ids').split(','));
        }
    
        List<myUpload_Item__c> attchedFiles = [SELECT Id, File_Name__c, Key__c, AWS_Key__c, Bucket_Name__c, VersionId__c, EndPoint__c
             FROM myUpload_Item__c 
             WHERE Id IN :attachedFileIdSet];
             
        List<FileWrapper> fileList = new List<FileWrapper>();
        
        for(myUpload_Item__c attchedFile :attchedFiles){
            FileWrapper fileWrapper = new FileWrapper();
            fileWrapper.key = attchedFile.AWS_Key__c <> null ? attchedFile.AWS_Key__c : attchedFile.Key__c +'/'+attchedFile.File_Name__c;
            fileWrapper.versionId = attchedFile.VersionId__c;
            fileWrapper.bucket = attchedFile.Bucket_Name__c;
            fileWrapper.region = attchedFile.EndPoint__c; 
            fileList.add(fileWrapper);
        }
        file = JSON.serialize(fileList);   
    }
    
    public class FileWrapper{
        public String key;
        public String versionId;
        public String bucket;
        public String region;
    }
}