public class AttachedFileThumbnailGridCompController {
    class CustomException extends Exception{}
    
    
    @AuraEnabled
    public static List<myUpload_Item__c> getUploadItems(String recordId){
        List<myUpload_Item__c> listToReturn = new List<myUpload_Item__c>();
        
        List<myUpload_Item__c> listOne = new List<myUpload_Item__c>();
        List<myUpload_Item__c> listTwo = new List<myUpload_Item__c>();
        
        List<myUpload_Item__c> uploadItemList =  [SELECT Id, Name, File_Name__c, File_Type__c, File_Size__c, CreatedDate, Status__c 
            FROM myUpload_Item__c 
            WHERE Upload__c = :recordId
            ORDER BY CreatedDate DESC];
            
        Set<Id> uploadItemIdSet = new Set<Id>();
        for(myUpload_Item__c uploadItem :uploadItemList){
            uploadItemIdSet.add(uploadItem.Id);
        } 
        
        Map<String, Attachment> attmntMap = new Map<String, Attachment>();
        for(Attachment attmnt :[SELECT Id, ParentId FROM Attachment WHERE ParentId IN :uploadItemIdSet]){
            attmntMap.put(attmnt.ParentId, attmnt);
        }
        
        for(myUpload_Item__c uploadItem :uploadItemList){
            uploadItem.AttachmentId__c = attmntMap.containsKey(uploadItem.Id) ? attmntMap.get(uploadItem.Id).Id : null;
            if(uploadItem.Status__c == 'Thumbnail Creation Successful'){
                listOne.add(uploadItem);
            }
            else{
                listTwo.add(uploadItem);
            }
        }  
        
        listToReturn.addAll(listOne);
        listToReturn.addAll(listTwo);
        
        return listToReturn;
    }
    
    
    public static Map<String, Attachment> getAttachmentMap(String recordId){
        Map<String, Attachment> mapToReturn = new Map<String, Attachment>();
        
        Set<Id> uploadItemIdSet = new Set<Id>();
        for(myUpload_Item__c uploadItem :[SELECT Id FROM myUpload_Item__c WHERE Upload__c = :recordId]){
            uploadItemIdSet.add(uploadItem.Id);
        }
        
        for(Attachment attmnt :[SELECT Id, ParentId FROM Attachment WHERE ParentId IN :uploadItemIdSet]){
            mapToReturn.put(attmnt.ParentId, attmnt);
        }
        
        return mapToReturn;
    }
    
    
    @AuraEnabled
    public static String deleteAttachedFiles(List<String> attachedFilesIdSet){
        String returnValue = 'Success';
        Database.DeleteResult[] drList;
        Savepoint sp = Database.setSavepoint();
        
        try{
            //AttachedFileDeleteService.deleteAttachedFiles(attachedFilesIdSet);
            Map<Id, myUpload_Item__c> attachedFilesMap = new Map<Id, myUpload_Item__c>([SELECT Id, File_Name__c FROM myUpload_Item__c WHERE Id IN :attachedFilesIdSet]);
            drList = Database.delete(attachedFilesMap.values(), false);
            
            if(drList.size() == 1){
                if(drList[0].isSuccess()){
                }
                else{
                    returnValue = 'File could not be deleted';
                    for(Database.Error err : drList[0].getErrors()){
                        returnValue += '\n' + err.getMessage() + '\nPlease contact system administrator for more details';
                    }
                    
                    throw new CustomException(returnValue);
                }
            }
            
            Integer countSuccess = 0;
            Integer countFailure = 0;
            
            String errorFilesMessage = '';
            if(drList.size() > 1){
                for(Database.DeleteResult dr : drList) {
                    if (dr.isSuccess()){
                        countSuccess++;
                    }
                    else{
                        countFailure++;
                        errorFilesMessage += '\nFile: '+attachedFilesMap.get(dr.getId()).File_Name__c+':';
                        for(Database.Error err : dr.getErrors()){
                            errorFilesMessage += '\n' + err.getMessage();
                        }
                    }
                }
                
                if(countSuccess < drList.size()){
                    throw new CustomException('Files could not be deleted\n' + errorFilesMessage + '\nPlease contact system administrator for more details');
                }    
            }  
        }
        catch(Exception e){
            Database.rollback(sp);
            return e.getMessage();
        }
        return returnValue;
    }
}