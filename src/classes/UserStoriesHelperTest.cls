@isTest
public class UserStoriesHelperTest{

    public static testmethod void testRun(){
    
       /* Scrum_Teams__c scrumTeam = new Scrum_Teams__c();
        scrumTeam.Name = 'Test Team';
        scrumTeam.Primary_Contact__c = 'Test Contact';
        scrumTeam.Contact_Email_Address__c = 'test@gmail.com';
        insert scrumTeam;*/
        
        Release__c rels = new Release__c();
        rels.name = 'Test Release1';
        rels.Start_Date__c = system.today();
        insert rels;
        
        Sprint__c sprint = new Sprint__c();
       // sprint.name = 'Test Sprint1';
        sprint.Release__c = rels.id;
        //sprint.Scrum_Team__c = scrumTeam.id;
        sprint.Start_Date__c = system.today();
        sprint.End_Date__c = system.today();
        insert sprint;
        
        test.startTest();
        
        User_Stories__c userStory = new User_Stories__c();
        userStory.Goal__c = 'Test the user story 1';
        userStory.Release__c = rels.id;
        //userStory.Scrum_Team__c = scrumTeam.id;
        userStory.Sprint__c = sprint.id;
        userStory.Status__c = 'Estimation';
        userStory.Priority__c = 'Medium';
        insert userStory;
        
        update userStory;
        
        delete userStory;
        
        test.stopTest();
    }
}