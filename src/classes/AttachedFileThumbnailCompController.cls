public with sharing class AttachedFileThumbnailCompController{
    @AuraEnabled
    public static myUpload_Item__c getUploadItemDetails(String recordId){
        System.debug('RECORD ID----'+recordId);
        return [SELECT Id, Name, File_Name__c, File_Type__c FROM myUpload_Item__c WHERE Id = :recordId];
    }
}