public class AttachedFileThumbnailCompForMyItems {
	@AuraEnabled
    public static Part__c getAttachedFileDetails(String recordId){
        return [SELECT Id, Current_myItem_Revision__r.Current_Attached_File__c FROM Part__c WHERE Id = :recordId];
    }	
}