public class ResponseHandlerAutodeskManifest{
    public static void processResponse(HttpResponse response, Id myUploadItemId){
        System.debug(response);
        String responseString = response.getBody();
        Integer statusCode = response.getStatusCode();
        
        myUpload_Item__c myUploadItem = [SELECT Id, No_of_Retries__c FROM myUpload_Item__c WHERE Id = :myUploadItemId];
        
        if(statusCode == 200){
            Map<String, Object> responseParsed = (Map<String, Object>) JSON.deserializeUntyped(responseString);
            
            myUploadItem.URN__c = (String) responseParsed.get('urn');
            myUploadItem.Status__c = 'Translation Initiated';
            myUploadItem.ErrorCode__c = null;
            myUploadItem.ErrorCodeText__c = '';
            myUploadItem.ErrorMessage__c = '';
            myUploadItem.No_of_Retries__c = 0;
        }
        else{
            myUploadItem.ErrorCode__c = statusCode;
            myUploadItem.ErrorMessage__c = responseString;
            myUploadItem.ErrorCodeText__c = 'INVALID FILE/ UNSUPPORTED FORMAT';
            myUploadItem.Status__c = 'Translation Initiation Failed';
            myUploadItem.No_of_Retries__c = myUploadItem.No_of_Retries__c == null ? 0 : myUploadItem.No_of_Retries__c + 1;
        }
        
        update myUploadItem;
    }    
}