public class RequestProviderAutodeskManifest{
    public static HttpRequest getRequest(Id myUploadItemId){
        AutodeskIntegrationProperties__c autodeskIntegratonProperties = AutodeskIntegrationProperties__c.getInstance();
        
        myUpload_Item__c myUploadItem = [SELECT Id, Autodesk_ObjectId__c FROM myUpload_Item__c WHERE Id = :myUploadItemId];
        
        String urn = EncodingUtil.base64Encode(Blob.valueOf(myUploadItem.Autodesk_ObjectId__c));
        
        String body = '';
        body += '{';
        body +=     '"input": {';
        body +=        '"urn":"'+urn+'"';
        body +=     '},';
        body +=     '"output": {';
        body +=         '"formats": [';
        body +=             '{';
        body +=                '"type": "svf",';
        body +=                 '"views":["2d","3d"]';
        body +=             '}';
        body +=         ']';
        body +=     '}';
        body += '}';
        
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setEndPoint(autodeskIntegratonProperties.AutodeskManifestEndpoint__c);
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Bearer '+ AutodeskIntegrationHandler.getAccessToken());
        request.setBody(body);
        
        return request;
    }
}