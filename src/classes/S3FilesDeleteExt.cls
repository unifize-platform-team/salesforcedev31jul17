public class S3FilesDeleteExt{
    public class CustomException extends Exception{}
    
    public List<myUpload_Item__c> uploadItemsList {get;set;}
    private List<Id> uploadItemsIdSet = new List<Id>();
    private Id parentId;
    private String sObjName;
    
    public S3FilesDeleteExt(ApexPages.StandardSetController con){
         system.debug('AAAA-----'+ApexPages.currentPage());
         
         parentId = ApexPages.currentPage().getParameters().get('Id');
         sObjName = parentId.getSObjectType().getDescribe().getName();
         
         if(ApexPages.currentPage().getParameters().get('ids') <> null){
             uploadItemsIdSet  = ApexPages.currentPage().getParameters().get('ids').split(',');
         }
         
         if(uploadItemsIdSet.size() == 1){
             for(myUpload_Item__c myUploadItem :(List<myUpload_Item__c>)con.getSelected()){
                 uploadItemsIdSet.add(myUploadItem.Id);
             }
         }
    }
    
    
    public void deleteMyUploadItems(){
        
        Map<String, S3FileDeleteProperties__c> fileDeletePropertiesMap = S3FileDeleteProperties__c.getAll();
         
         if(!fileDeletePropertiesMap.containsKey(sObjName)){
             throw new CustomException('Record to hold deleted items is not configured! Contact your system to be able to delete.');
         }
         
         String fieldName = fileDeletePropertiesMap.get(sObjName).Related_Field_Name__c;
         String reparentId = fileDeletePropertiesMap.get(sObjName).Reparent_to_Record_Id__c; 
         
         String queryString = 'SELECT Id, Name, Original_Parent_Id__c, ' + fieldName + ' FROM myUpload_Item__c WHERE Id IN :uploadItemsIdSet';
         
         List<myUpload_Item__c> myUploadItems = Database.query(queryString);
         
         for(myUpload_Item__c myUploadItem :myUploadItems){
             myUploadItem.Original_Parent_Id__c = (String) myUploadItem.get(fieldName);
             myUploadItem.put(fieldName, reparentId);
             myUploadItem.Is_Deleted__c = true;
         }
         
         update myUploadItems;
    }
}