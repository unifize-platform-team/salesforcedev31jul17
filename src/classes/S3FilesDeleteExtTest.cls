@isTest(SeeAllData=false)
private class S3FilesDeleteExtTest{
    @testSetup
    static void setup(){
        Account acc = new Account(Name = 'Test');
        insert acc;
        
        myUpload_Item__c uploadItem = new myUpload_Item__c(Account__c = acc.Id);
        insert uploadItem;
    }
    
    
    private static testMethod void testOne() { 
        List<myUpload_Item__c> myUploadItemList = [SELECT Id, Account__c FROM myUpload_Item__c LIMIT 1];
        
        S3FileDeleteProperties__c delProp = new S3FileDeleteProperties__c(
            Name = 'Account',
            Related_Field_Name__c = 'Account__c',
            Reparent_to_Record_Id__c = myUploadItemList[0].Account__c 
        );
        insert delProp;
        
        
        ApexPages.currentPage().getParameters().put('Id', myUploadItemList[0].Account__c);
        ApexPages.currentPage().getParameters().put('ids',myUploadItemList[0].Id);
        ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(myUploadItemList);
        setCon.setSelected(myUploadItemList);
        S3FilesDeleteExt fileDelete = new S3FilesDeleteExt(setCon);
        List<myUpload_Item__c> uploadItemsList = fileDelete.uploadItemsList;
        
        Test.startTest();
        fileDelete.deleteMyUploadItems();
        Test.stopTest();
        
        System.assert([SELECT Id, Is_Deleted__c FROM myUpload_Item__c LIMIT 1].Is_Deleted__c);
    }
    
    
    private static testMethod void testTwo() {
        List<myUpload_Item__c> myUploadItemList = [SELECT Id, Account__c FROM myUpload_Item__c LIMIT 1];
        ApexPages.currentPage().getParameters().put('Id', myUploadItemList[0].Account__c);
        ApexPages.currentPage().getParameters().put('ids',myUploadItemList[0].Id);
        ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(myUploadItemList);
        setCon.setSelected(myUploadItemList);
        S3FilesDeleteExt fileDelete = new S3FilesDeleteExt(setCon);
        List<myUpload_Item__c> uploadItemsList = fileDelete.uploadItemsList;
        
        Test.startTest();
        try{
            fileDelete.deleteMyUploadItems();
        }
        catch(Exception e){}
        Test.stopTest();
        
        System.assert([SELECT Id, Is_Deleted__c FROM myUpload_Item__c LIMIT 1].Is_Deleted__c == FALSE);
    }
}