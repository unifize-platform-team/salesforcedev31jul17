public class candidateRegistrationController {
    public attachment att {get;set;}
    public Candidate__c cnd {get;set;}
    public job_application__c jpp ;
    public boolean gBack {get;set;}
    
    public candidateRegistrationController(ApexPages.StandardController controller) 
    {
        att = new attachment();
        cnd  = new Candidate__c();
        jpp = new job_application__c ();
        gBack=true;
    }
    
    public PageReference saveApplication(){
        PageReference pageRef = new PageReference('/apex/jobs');
            insert cnd;
            
            jpp.position__c =ApexPages.currentPage().getParameters().get('pid');
            jpp.Candidate__c =cnd.id;
            insert jpp;
            
            att.parentId =jpp.id;
            insert att;
            gBack=false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Job Application has been saved Succesfully'));
            
            
        
        return pageRef ;
    }
}