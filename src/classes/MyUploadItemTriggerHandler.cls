public class MyUploadItemTriggerHandler{
    public static void onBeforeUpdate(List<myUpload_Item__c> uploadItemList, Map<Id, myUpload_Item__c> uploadItemMap){
        for(myUpload_Item__c uploadItem :uploadItemList){
            if(
                (uploadItem.AWS_Key__c <> uploadItemMap.get(uploadItem.Id).AWS_Key__c && uploadItem.AWS_Key__c <> null) ||
                (uploadItem.Key__c <> uploadItemMap.get(uploadItem.Id).Key__c && uploadItem.Key__c <> null)    
            ){
                uploadItem.Status__c = 'S3 Upload Successful';
            }
        }
    }
    
    
    public static void onAfterUpdate(List<myUpload_Item__c> uploadItemList, Map<Id, myUpload_Item__c> uploadItemMap){
        AutodeskIntegrationProperties__c autodeskIntegratonProperties = AutodeskIntegrationProperties__c.getInstance();
        Integer manifestRetry = autodeskIntegratonProperties.AutodeskManifestRetry__c <> null ? (Integer) autodeskIntegratonProperties.AutodeskManifestRetry__c : 5;
        Integer fileUploadRetry = autodeskIntegratonProperties.FileUploadRetry__c <> null ? (Integer) autodeskIntegratonProperties.FileUploadRetry__c : 5;
        Integer thumbnailRetry = autodeskIntegratonProperties.ThumbnailRetry__c <> null ? (Integer) autodeskIntegratonProperties.ThumbnailRetry__c : 5;
        
        Integer count = 0;
        Integer halfWay = uploadItemList.size()/2;
        
        for(myUpload_Item__c uploadItem :uploadItemList){
            count++;
            //*** First time file upload to autodesk ***//
            if(
                (uploadItem.AWS_Key__c <> uploadItemMap.get(uploadItem.Id).AWS_Key__c && uploadItem.AWS_Key__c <> null) ||
                (uploadItem.Key__c <> uploadItemMap.get(uploadItem.Id).Key__c && uploadItem.Key__c <> null)
            ){
                if(count < halfWay){
                    MyUploadItemTriggerHandler.uploadFileToAutodesk(uploadItem.Id);
                }
                else{
                    System.enqueueJob(new AutodeskFileUpload(uploadItem.Id));
                }    
            }
            
            //*** Retry file upload to autodesk (Autodesk Upload Failed) ***//
            if(uploadItem.Status__c == 'Autodesk Upload Failed' && uploadItem.No_of_Retries__c < fileUploadRetry){
                System.enqueueJob(new AutodeskFileUpload(uploadItem.Id));
            }
            
            
            //*** Manifest after file upload is successful ***//
            if(uploadItem.Status__c <> uploadItemMap.get(uploadItem.Id).Status__c && uploadItem.Status__c == 'Autodesk Upload Successful'){
                MyUploadItemTriggerHandler.manifest(uploadItem.Id);
            }
            
            //*** Retry Manifest (Translation Initiation Failed) ***//
            if(uploadItem.Status__c == 'Translation Initiation Failed' && uploadItem.No_of_Retries__c < manifestRetry){
                MyUploadItemTriggerHandler.manifest(uploadItem.Id);
            }
            
            //*** Get Thumbnail ***//
            if(uploadItem.Status__c <> uploadItemMap.get(uploadItem.Id).Status__c && uploadItem.Status__c == 'File Conversion Successful'){
                MyUploadItemTriggerHandler.thumbnail(uploadItem.Id);
            }
            
            //*** Retry get thumbnail (Thumbnail Creation Failed) ***//
            if(uploadItem.Status__c == 'Thumbnail Creation Failed' && uploadItem.No_of_Retries__c < thumbnailRetry){
                MyUploadItemTriggerHandler.thumbnail(uploadItem.Id);
            }  
        }
    }
    
    
    @future(callout=true)   
    public static void uploadFileToAutodesk(Id myUploadItemId){
        AutodeskIntegrationHandler.uploadFileToAutodesk(myUploadItemId);
    }
    
    
      
    public static void manifest(Id myUploadItemId){
        System.enqueueJob(new AutodeskManifest(myUploadItemId));
    }
    
    
    public static void manifestStatus(Id myUploadItemId){
        System.enqueueJob(new AutodeskManifestStatus(myUploadItemId));
    }
    
    
    public static void thumbnail(Id myUploadItemId){
        System.enqueueJob(new AutodeskThumbnail(myUploadItemId));
    }
}