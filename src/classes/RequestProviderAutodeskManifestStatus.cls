public class RequestProviderAutodeskManifestStatus{
     public static HttpRequest getRequest(Id myUploadItemId){
        AutodeskIntegrationProperties__c autodeskIntegratonProperties = AutodeskIntegrationProperties__c.getInstance();
        
        myUpload_Item__c myUploadItem = [SELECT Id, URN__c FROM myUpload_Item__c WHERE Id = :myUploadItemId];
        
         HttpRequest request = new HttpRequest();
         request.setMethod('GET');
         request.setEndpoint(autodeskIntegratonProperties.AutodeskManifestStatusEndpoint__c+myUploadItem.URN__c+'/manifest');
         request.setHeader('Authorization', 'Bearer '+ AutodeskIntegrationHandler.getAccessToken());
         
         return request;
     }    
}