public class ResponseHandlerAutodeskManifestStatus{
    public static void processResponse(HttpResponse response, Id myUploadItemId){
        String responseString = response.getBody();
        System.debug(responseString);
        Map<String, Object> responseParsed = (Map<String, Object>) JSON.deserializeUntyped(responseString);
        String manifestStatus = (String) responseParsed.get('status');
        
        myUpload_Item__c myUploadItem = [SELECT Autodesk_Manifest_Status__c, Status__c, No_of_Retries__c FROM myUpload_Item__c WHERE Id = :myUploadItemId];
        
        if(manifestStatus == 'failed'){
            myUploadItem.Autodesk_Manifest_Status__c = manifestStatus;
            myUploadItem.Status__c = 'File Conversion Failed';
            myUploadItem.No_of_Retries__c = myUploadItem.No_of_Retries__c == null ? 0 : myUploadItem.No_of_Retries__c + 1;
            myUploadItem.ErrorCodeText__c = 'INVALID/ CORRUPT FILE';
            myUploadItem.ErrorCode__c = 992;
            myUploadItem.ErrorMessage__c = responseString;
        }
        else
        if(manifestStatus == 'Success'){
            myUploadItem.Status__c = 'File Conversion Successful';
            myUploadItem.No_of_Retries__c = 0;
            myUploadItem.ErrorCode__c = null;
            myUploadItem.ErrorCodeText__c = '';
            myUploadItem.ErrorMessage__c = '';
        }
        else{
            myUploadItem.Status__c = 'File Conversion Inprogress'; 
            myUploadItem.No_of_Retries__c = myUploadItem.No_of_Retries__c == null ? 0 : myUploadItem.No_of_Retries__c + 1;
            myUploadItem.ErrorCodeText__c = '';
            myUploadItem.ErrorMessage__c = responseString;
        }
        
        update myUploadItem;
    }    
}