public class NewMyProjectQueryItemsHandler {
    
    @auraEnabled
    public static void createQueryItem(String itemId)
    {
        myProject_Query_Item__c item = new myProject_Query_Item__c(myProject_Query__c=itemId);
        insert item;
    }

}