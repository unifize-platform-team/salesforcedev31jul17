public class S3FilesDownloadExt{
    public List<myUpload_Item__c> uploadItemsList {get;set;}
    public String keys {get;set;}

    private List<Id> uploadItemsIdSet = new List<Id>();
    
    public S3FilesDownloadExt(ApexPages.StandardSetController con){
         system.debug('AAAA-----'+ApexPages.currentPage());
         
         if(ApexPages.currentPage().getParameters().get('ids') <> null){
             uploadItemsIdSet  = ApexPages.currentPage().getParameters().get('ids').split(',');
         }
         
         if(uploadItemsIdSet.size() == 1){
             for(myUpload_Item__c myUploadItem :(List<myUpload_Item__c>)con.getSelected()){
                 uploadItemsIdSet.add(myUploadItem.Id);
             }
         }
         
         
         uploadItemsList = [SELECT Id, Name, File_Name__c, Key__c FROM myUpload_Item__c WHERE Id IN :uploadItemsIdSet ];
         List<String> keyList = new List<String>();
         for(myUpload_Item__c uploadItem :uploadItemsList){
             keyList.add(uploadItem.Key__c);
         }
         
         keys = JSON.serialize(keyList);
    }
}