/*
This class is used to calculate all the financials related to user stories 
from tasks Breakdown 
*/
public class UserStoriesHelper  {
    
    public static void autoCalculateFinances(set<id>sprintCount){
        
        List<Sprint__c> ct = new List<Sprint__c>();  
        
        AggregateResult[] groupedResults = [SELECT COUNT(Id),SUM(Estimation__c),SUM(Cost__c), Sprint__c FROM User_Stories__c where Sprint__c IN :sprintCount GROUP BY Sprint__c];  
        
        for(AggregateResult ar:groupedResults) {     
            Id custid = (ID)ar.get('Sprint__c'); 
            Integer count = (INTEGER)ar.get('expr0'); 
            double estimation=(double)ar.get('expr1'); 
            double cost=(double)ar.get('expr2'); 
            Sprint__c sp = new Sprint__c(Id=custid); 
            sp.Total_User_Stories__c = count; 
            sp.Estimation__c=estimation;
            sp.Cost__c= cost ;   
            ct.add(sp);      
        }
        try{
            update ct;
        } catch (exception ex){
            system.debug(ex.getMessage());
        }   
    }
   
    public static void autoSharetoDeveloper(map<id,User_Stories__c>newUsrMap,map<id,User_Stories__c>oMap,string operation){
		
		list<User_Stories__c> ustUpdate= new list<User_Stories__c>();
		for(User_Stories__c ust:[select id,epic__c,Developer_Assigned__c,Developer_Assigned__r.ContactId,
		                        epic__r.Developer_Assigned__c,epic__r.Developer_Assigned__r.ContactId
		                         from User_Stories__c where id in :	newUsrMap.keySet()]){
			if(operation=='Update'){
				if((ust.Developer_Assigned__c !=oMap.get(ust.id).Developer_Assigned__c) ||
				    ust.epic__c !=oMap.get(ust.id).epic__c){
					if(null<>ust.Epic__c){
						ust.epic__r.Developer_Assigned__c =ust.Developer_Assigned__c;
						ust.epic__r.Contact__c =ust.Developer_Assigned__r.ContactId;
						
					}
					if(null<>ust.Developer_Assigned__c){
						ust.Contact__c=ust.Developer_Assigned__r.ContactId;
					}
					ustUpdate.add(ust);
				}
			}else{
				if(null<>ust.Epic__c){
						ust.epic__r.Developer_Assigned__c =ust.Developer_Assigned__c;
						ust.epic__r.Contact__c =ust.Developer_Assigned__r.ContactId;
						
					}
					if(null<>ust.Developer_Assigned__c){
						ust.Contact__c=ust.Developer_Assigned__r.ContactId;
					}
					ustUpdate.add(ust);
			}
		}
		 if(ustUpdate.size()>0){
			 try{
				 update ustUpdate;
			 }catch (exception ex){
				 system.debug('Sharing Error ' +ex.getMessage());
			 }
		 }
	}
}