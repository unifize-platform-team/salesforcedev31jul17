public class UFEPartsController {
    @AuraEnabled
    public static List<Part__c> getTasksOfProject(String projectId)
    {
        myProject__c pr =[Select id,Customer__c,Partner__c 
                          from myProject__c where id = :projectId ];
        return [SELECT Id,Name,Drawing_No__c, 
                myMaterial__r.Name,
                Standard_Quantity__c,myPart_Status__c 
                FROM Part__c
                WHERE Account__c = :pr.Partner__c];
    }
    
    
    @AuraEnabled
    
    public static String markTasksAsCompleted(String myPart,string selectedProjId, string checkboxValue)
    {
        system.debug('insidemarkTasksAsCompleted');
        String strArr='';
        List<myProject_Part__c> existingPartList = [SELECT myPart__c FROM myProject_Part__c WHERE myProject__c=:selectedProjId ];
        System.debug('ext****'+existingPartList);
        string myPar = '';
        myPar = myPart;
        System.debug('Whole String : '+myPar);
        string selectedProj ='';
        selectedProj =selectedProjId;
        list <myProject_Part__c> mpcList = new list<myProject_Part__c>();
        if(!test.isRunningTest())
        {
        	List<Part__c> myParts = (List<Part__c>)System.JSON.deserialize(myPar,List<Part__c>.class);
            for(Part__c mp : myParts)
            {	
                myProject_Part__c pt = new myProject_Part__c();
                mpcList.add(pt);
                for(myProject_Part__c ep:existingPartList)
                {	
                    
                    if(ep.myPart__c != mp.Id){
                        pt.myPart__c = mp.ID;
                        pt.myProject__c= selectedProj;
                        pt.Quantity__c=mp.Standard_Quantity__c;
                        pt.Filler_Part__c=Boolean.valueOf(checkboxValue);
                    }
                    else if(ep.myPart__c == mp.Id)
                    {
                        strArr+=String.valueOf(mp.Name)+' ';
                        System.debug('Part already Existed');
                        break;
                    }  
            	}
            }
            System.debug('str : '+strArr);
            if(mpcList != null){
                insert mpcList;
                System.debug('list: '+mpcList);
            }
            
        }
        return strArr;
    }}