public class ResponseWrapperAutodesk{
    public String bucketKey;
    public String objectId;
    public String objectKey;
    public String sha1;
    public String size;
    public String contentType;
    public String location;
}