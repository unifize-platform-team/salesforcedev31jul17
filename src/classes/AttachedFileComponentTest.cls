@isTest(SeeAllData=false)
private class AttachedFileComponentTest{
    @testSetup
    static void setup(){
        Account acc = new Account(
            Name = 'Test'
        );
        insert acc;
        
        Upload__c upload = new Upload__c(
            Account__c = acc.Id
        );
        insert upload;
        
        List<myUpload_Item__c> uploadItemList = new List<myUpload_Item__c>();
        uploadItemList.add(new myUpload_Item__c(
            Upload__c = upload.Id
        ));
        
        uploadItemList.add(new myUpload_Item__c(
            Upload__c = upload.Id
        ));
        
        uploadItemList.add(new myUpload_Item__c(
            Upload__c = upload.Id
        ));
        
        insert uploadItemList;
        
        List<Attachment> attachmentList = new List<Attachment>();
        
        attachmentList.add(new Attachment(
            ParentId = uploadItemList[0].Id,
            Body = Blob.valueOf('Test'),
            Name = 'Test'
        ));
        
        attachmentList.add(new Attachment(
            ParentId = uploadItemList[1].Id,
            Body = Blob.valueOf('Test'),
            Name = 'Test'
        ));
        
        insert attachmentList;
        
        Part__c myItem = new Part__c();
        insert myItem;
        
        myItem_Version__c itemVersion = new myItem_Version__c(
            Current_Attached_File__c = uploadItemList[1].Id,
            myItem__c = myItem.Id
        );
        insert itemVersion;
        
        myProject__c project = new myProject__c(
            Partner__c = acc.Id
        );
        insert project;
        
        uniCHECK_Query__c query = new uniCHECK_Query__c(
            myProject__c = project.Id,
            myItem_Version_After__c = itemVersion.Id,
            myItem_Version__c = itemVersion.Id
        );
        insert query;
    }
    
    
    private static testMethod void testAttachedFileImageCompController(){
        String recordId = [SELECT Id FROM myUpload_Item__c LIMIT 1].Id;
        Test.startTest();
        AttachedFileImageCompController.getUploadItemDetails(recordId);
        AttachedFileImageCompController.getAttachmentId(recordId);
        Test.stopTest();
    }
    
    
    private static testMethod void testAttachedFileThumbnailCompController(){
        String recordId = [SELECT Id FROM myUpload_Item__c LIMIT 1].Id;
        Test.startTest();
        AttachedFileThumbnailCompController.getUploadItemDetails(recordId);
        Test.stopTest();
    } 
    
    
    private static testMethod void testAttachedFileThumbnailCompForVersionItem(){
        String recordId = [SELECT Id FROM myItem_Version__c LIMIT 1].Id;
        Test.startTest();
        AttachedFileThumbnailCompForVersionItem.getAttachedFileDetails(recordId);
        Test.stopTest();
    }
    
    
    private static testMethod void testAttachedFileThumbnailCompForMyItems(){
        String recordId = [SELECT Id FROM Part__c LIMIT 1].Id;
        Test.startTest();
        AttachedFileThumbnailCompForMyItems.getAttachedFileDetails(recordId);
        Test.stopTest();
    }
    
    
    private static testMethod void testS3FilesDownloadCompController(){
        String recordId = [SELECT Id FROM myUpload_Item__c LIMIT 1].Id;
        ApexPages.currentPage().getParameters().put('Id', recordId);
        ApexPages.currentPage().getParameters().put('ids', recordId);
        Test.startTest();
        S3FilesDownloadCompController s3Download = new S3FilesDownloadCompController();
        Test.stopTest();
    }
    
    
    private static testMethod void testAttachedFileThumbnailGridCompController(){
        String recordId = [SELECT Id FROM Upload__c LIMIT 1].Id;
        List<String> attachedFilesIdSet = new List<String>();
        for(myUpload_Item__c uploadItem :[SELECT Id FROM myUpload_Item__c]){
            attachedFilesIdSet.add(uploadItem.Id);
        }
        Test.startTest();
        AttachedFileThumbnailGridCompController.getUploadItems(recordId);
        AttachedFileThumbnailGridCompController.getAttachmentMap(recordId);
        AttachedFileThumbnailGridCompController.deleteAttachedFiles(attachedFilesIdSet);
        Test.stopTest();
    }
    
    
    private static testMethod void testAttachedFileThumbnailGridCompController2(){
        List<String> attachedFilesIdSet = new List<String>();
        for(myUpload_Item__c uploadItem :[SELECT Id FROM myUpload_Item__c]){
            attachedFilesIdSet.add(uploadItem.Id);
        }
        Test.startTest();
        AttachedFileThumbnailGridCompController.deleteAttachedFiles(new List<String>{attachedFilesIdSet[0]});
        Test.stopTest();
    }
    
    
    private static testMethod void testAttachedFileThumbnailGridCompController3(){
        List<String> attachedFilesIdSet = new List<String>();
        for(myUpload_Item__c uploadItem :[SELECT Id FROM myUpload_Item__c]){
            attachedFilesIdSet.add(uploadItem.Id);
        }
        Test.startTest();
        AttachedFileThumbnailGridCompController.deleteAttachedFiles(new List<String>{attachedFilesIdSet[1]});
        Test.stopTest();
    }
    
    
    private static testMethod void testAttachedFileThumbnailGridCompForMyItem(){
        String recordId = [SELECT Id FROM Part__c LIMIT 1].Id;
        Test.startTest();
        AttachedFileThumbnailGridCompForMyItem.getUploadItems(recordId);
        Test.stopTest();
    }
    
    
    private static testMethod void testThumbnailCompForUnicheckQuery(){
        String recordId = [SELECT Id FROM uniCHECK_Query__c LIMIT 1].Id;
        Test.startTest();
        ThumbnailCompForUnicheckQuery.getAttachedFileDetailsBefore(recordId);
        ThumbnailCompForUnicheckQuery.getAttachedFileDetailsAfter(recordId);
        Test.stopTest();
    }
    
    
    private static testMethod void testQueryImageCompHandler(){
        String recordId = [SELECT Id FROM uniCHECK_Query__c LIMIT 1].Id;
        Test.startTest();
        QueryImageCompHandler.getImageDetails(recordId);
        Test.stopTest();
    }
    
    
    class MockAutodeskFileUploadResponderSuccess implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setStatusCode(200);
            
            ResponseWrapperAutodeskMain responseBody = new ResponseWrapperAutodeskMain();
            responseBody.returnValue.bucketKey = 'Test';
            responseBody.returnValue.objectId = 'Test';
            responseBody.code = 200;
            response.setBody(JSON.serialize(responseBody));
            return response;
        }
    }
    
    
    private static testMethod void testForgeViewerPageController(){
        myUpload_Item__c uploadItem = [SELECT Id FROM myUpload_Item__c LIMIT 1];
        ApexPages.currentPage().getParameters().put('Id',uploadItem.Id);
        
        insert new AutodeskIntegrationProperties__c(
            AuthenticationEndpoint__c = 'Test'
        );
        
        Test.setMock(HttpCalloutMock.class, new MockAutodeskFileUploadResponderSuccess());
        
        Test.startTest();
        ForgeViewerPageController forgeViewer = new ForgeViewerPageController(new ApexPages.StandardController(uploadItem));
        Test.stopTest();
    }     
    
    
    private static testMethod void testDeleteOrphanAttachedFilesBatchScheduler(){
        AttachedFilesDeleteBatchControl__c afdbc = new AttachedFilesDeleteBatchControl__c();
        afdbc.Execute_Delete_Batch_Job__c = true;
        insert afdbc;
        
        List<myUpload_Item__c> uploadItemList = [SELECT Id FROM myUpload_Item__c];
        
        for(myUpload_Item__c uploadItem :uploadItemList){
            uploadItem.Upload__c = null;
        }
        update uploadItemList;
        
        Test.startTest();
        DateTime nextFireTime = System.now().addMinutes(1);
        
        String hour = String.valueOf(nextFireTime.hour());
        String min = String.valueOf(nextFireTime.minute());
        String sec = String.valueOf(nextFireTime.second());
        
        String cron = sec + ' ' + min + ' ' + hour + ' * * ?';
        
        System.schedule('DeleteOrphanAttachedFilesBatchScheduler'+String.valueOf(nextFireTime), cron, new DeleteOrphanAttachedFilesBatchScheduler());
        Test.stopTest();
    }
}