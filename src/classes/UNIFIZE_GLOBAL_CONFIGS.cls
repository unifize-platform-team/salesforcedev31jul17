Global class UNIFIZE_GLOBAL_CONFIGS 
{
	/*
     * Class Description:
     * This class defines all global configurations - static variables with values for Unifize app
     * 
     * Author : venkat.reddy@unifize.com
     * Creation Date : 20-04-2017
     * Comments : 
     */
    
    //debugging related
    global static boolean DEBUG = true;
    
    //user related
    
    
    //platform related

    
    //CAM related
    
}