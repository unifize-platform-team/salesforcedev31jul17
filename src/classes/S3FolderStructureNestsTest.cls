@isTest(SeeAllData=false)
private class S3FolderStructureNestsTest{
     @testSetup
    static void setup(){
        Account acc = new Account(Name = 'Test');
        insert acc;
        
        Nest__c nest = new Nest__c(
            Account__c = acc.Id,
            Scrap_Weight__c = 1,
            Net_Weight__c = 1, 
            Remnant_Weight__c =  1
        );
        insert nest;
    }    
    
    
    private static testMethod void testOne() {
        Nest__c nest = [SELECT Id FROM Nest__c LIMIT 1];
        S3FolderStructureNests s3FolderStructure = new S3FolderStructureNests();
        s3FolderStructure.getFolderStructure(nest.Id);
    }
}