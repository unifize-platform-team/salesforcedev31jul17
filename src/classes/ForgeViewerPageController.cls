global class ForgeViewerPageController{
    global String accessToken {get;set;}
    global String urn {get;set;}
    
    global ForgeViewerPageController(ApexPages.StandardController con){
        accessToken = AutodeskIntegrationHandler.authenticateViewer();
        urn = [SELECT Id, URN__c FROM myUpload_Item__c WHERE Id = :ApexPages.currentPage().getParameters().get('Id')].URN__c;
    }
    
    
    @RemoteAction
    global static List<myProject_Query_Item__c> getQueryItems(String queryId){
        return [SELECT Id, Name, Layer_Name__c FROM myProject_Query_Item__c WHERE myProject_Query__c = :queryId];
    }
}