@isTest(SeeAllData=false)
private class S3FilesDownloadExtTest{
    @testSetup
    static void setup(){
        Account acc = new Account(Name = 'Test');
        insert acc;
        
        myUpload_Item__c uploadItem = new myUpload_Item__c(Account__c = acc.Id);
        insert uploadItem;
    }
    
    
    private static testMethod void testOne() {
        List<myUpload_Item__c> myUploadItemList = [SELECT Id FROM myUpload_Item__c LIMIT 1];
        ApexPages.currentPage().getParameters().put('ids',myUploadItemList[0].Id);
        ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(myUploadItemList);
        setCon.setSelected(myUploadItemList);
        
        Test.startTest();
        S3FilesDownloadExt fileDownload = new S3FilesDownloadExt(setCon);
        Test.stopTest();
        
        System.assert(fileDownload.uploadItemsList.size() == 1);
    }
}