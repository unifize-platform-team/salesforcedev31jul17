global Class S3UploadComponentOnSaveMyItem implements iOnSave{
    global List<myUpload_Item__c> execute(List<S3DataWrapper> dataList){
        Savepoint sp = null;
        if(!Test.isRunningTest()){
            sp = Database.setSavepoint();
        }
        
        
        try{
            Map<String, myUpload_Item__c> myUploadMap = new Map<String, myUpload_Item__c>();
            for(S3DataWrapper data :dataList){
                myUpload_Item__c myUpload = new myUpload_Item__c();
                myUpload.Id = data.uploadItemId;
                myUpload.Bucket_Name__c = data.bucketName;
                myUpload.ETag__c = data.eTag;
                myUpload.File_Name__c = data.fileName;
                myUpload.AWS_Key__c = data.key;
                myUpload.Location__c = data.location.length() > 255 ? data.location.subString(0,255) : data.location;
                myUpload.File_Size__c = data.fileSize;
                myUpload.VersionId__c = data.versionId;
                myUpload.EndPoint__c = AWS_S3Properties__c.getInstance().Region__c;
                
                List<String> tempList = data.fileName.split('\\.');
                if(tempList != null && tempList.size() > 0){
                    myUpload.File_Type__c = tempList[tempList.size()-1];
                }
                
                List<String> tempList2 = data.key.split('/');
                if(tempList2 != null && tempList2.size() > 0){
                    myUpload.Key__c = data.key.left(data.key.length() - tempList2[tempList2.size()-1].length() - 1);
                }
                
                myUpload.put(data.relatedFieldName, data.recordId);
                
                myUploadMap.put(myUpload.File_Name__c, myUpload);
            }
            
            update myUploadMap.values();
            
            List<myItem_Version__c> versionList = new List<myItem_Version__c>();
            for(myUpload_Item__c uploadItem :myUploadMap.values()){
                myItem_Version__c version = new myItem_Version__c();
                version.myItem__c = uploadItem.myItem__c;
                version.Current_Attached_File__c = uploadItem.Id;
                versionList.add(version);
            }
            
            if(versionList.size() > 0){
                insert versionList;
            }
            
            return myUploadMap.values();
        }
        catch(Exception e){
            Database.rollback(sp);
            throw e;
        }  
    }   
}