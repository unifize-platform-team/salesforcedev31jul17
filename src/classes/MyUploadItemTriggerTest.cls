@isTest(SeeAllData=false)
private class MyUploadItemTriggerTest{
    @testSetup
    static void setup(){
        Account acc = new Account(
            Name = 'Test'
        );
        insert acc;
        
        Upload__c upload = new Upload__c(
            Account__c = acc.Id
        );
        insert upload;
        
        List<myUpload_Item__c> uploadItemList = new List<myUpload_Item__c>();
        uploadItemList.add(new myUpload_Item__c(
            Upload__c = upload.Id
        ));
        
        uploadItemList.add(new myUpload_Item__c(
            Upload__c = upload.Id
        ));
        
        uploadItemList.add(new myUpload_Item__c(
            Upload__c = upload.Id
        ));
        
        uploadItemList.add(new myUpload_Item__c(
            Upload__c = upload.Id
        ));
        
        insert uploadItemList;
        
        insert new AutodeskIntegrationProperties__c(
            BucketKey__c = 'Test',
            ClientId__c = 'Test',
            ClientSecret__c = 'Test',
            FileUploadEndPoint__c = 'Test',
            AuthenticationEndpoint__c = 'Test',
            AutodeskManifestEndpoint__c = 'Test'
        );
    }
    
    
    class MockAutodeskFileUploadResponderFailure implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setStatusCode(200);
            
            ResponseWrapperAutodeskMain responseBody = new ResponseWrapperAutodeskMain();
            responseBody.returnValue.bucketKey = 'Test';
            response.setBody(JSON.serialize(responseBody));
            return response;
        }
    }
    
    
    private static testMethod void testOne() { 
        Test.setMock(HttpCalloutMock.class, new MockAutodeskFileUploadResponderFailure());
        
        myUpload_Item__c uploadItem = [SELECT Id FROM myUpload_Item__c LIMIT 1];
        uploadItem.Bucket_Name__c = 'Test';
        uploadItem.File_Name__c = 'Test';
        uploadItem.Key__c = 'Test';
        uploadItem.AWS_Key__c = 'Test';
        
        Test.startTest();
        update uploadItem;
        Test.stopTest();
    }
    
    
    class MockAutodeskFileUploadResponderSuccess implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setStatusCode(200);
            
            ResponseWrapperAutodeskMain responseBody = new ResponseWrapperAutodeskMain();
            responseBody.returnValue.bucketKey = 'Test';
            responseBody.returnValue.objectId = 'Test';
            responseBody.code = 200;
            response.setBody(JSON.serialize(responseBody));
            return response;
        }
    }
    
    
    private static testMethod void testTwo() { 
        Test.setMock(HttpCalloutMock.class, new MockAutodeskFileUploadResponderSuccess());
        
        myUpload_Item__c uploadItem = [SELECT Id FROM myUpload_Item__c LIMIT 1];
        uploadItem.Bucket_Name__c = 'Test';
        uploadItem.File_Name__c = 'Test';
        uploadItem.Key__c = 'Test';
        uploadItem.AWS_Key__c = 'Test';
        uploadItem.Autodesk_ObjectId__c = 'Test';
        
        Test.startTest();
        update uploadItem;
        Test.stopTest();
    }
    
    
    class MockAutodeskAuthenticate implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setStatusCode(200);
            
            ResponseWrapperAutodeskMain responseBody = new ResponseWrapperAutodeskMain();
            
            response.setBody('{"access_token":"Test"}');
            return response;
        }
    }
    
    
    private static testMethod void testThree() { 
        Test.setMock(HttpCalloutMock.class, new MockAutodeskAuthenticate());
        
        Test.startTest();
        AutodeskIntegrationHandler.authenticate();
        Test.stopTest();
    }
    
    
    private static testMethod void testFour() { 
        Test.setMock(HttpCalloutMock.class, new MockAutodeskAuthenticate());
        
        Test.startTest();
        AutodeskIntegrationHandler.authenticateViewer();
        Test.stopTest();
    }
    
    
    private static testMethod void testFive() { 
        Test.setMock(HttpCalloutMock.class, new MockAutodeskFileUploadResponderFailure());
        
        List<myUpload_Item__c> uploadItemList = [SELECT Id FROM myUpload_Item__c];
        Integer count = 0;
        for(myUpload_Item__c uploadItem :uploadItemList){
            uploadItem.Bucket_Name__c = 'Test2'+count;
            uploadItem.File_Name__c = 'Test'+count;
            uploadItem.Key__c = 'Test'+count;
            uploadItem.AWS_Key__c = 'Test'+count;
            count++;
        }
        
        Test.startTest();
        update uploadItemList;
        Test.stopTest();
    }
    
    
    private static testMethod void testSix() { 
        Test.setMock(HttpCalloutMock.class, new MockAutodeskFileUploadResponderSuccess());
        
        myUpload_Item__c uploadItem = [SELECT Id FROM myUpload_Item__c LIMIT 1];
        uploadItem.Bucket_Name__c = 'Test';
        uploadItem.File_Name__c = 'Test';
        uploadItem.Autodesk_ObjectId__c = 'Test';
        uploadItem.Status__c = 'Translation Initiation Failed';
        uploadItem.No_of_Retries__c = 0;
        
        Test.startTest();
        update uploadItem;
        Test.stopTest();
    }
    
    
    private static testMethod void testSeven() { 
        Test.setMock(HttpCalloutMock.class, new MockAutodeskFileUploadResponderSuccess());
        
        myUpload_Item__c uploadItem = [SELECT Id FROM myUpload_Item__c LIMIT 1];
        uploadItem.Bucket_Name__c = 'Test';
        uploadItem.File_Name__c = 'Test';
        uploadItem.Autodesk_ObjectId__c = 'Test';
        uploadItem.Status__c = 'File Conversion Successful';
        uploadItem.No_of_Retries__c = 0;
        
        Test.startTest();
        update uploadItem;
        Test.stopTest();
    }
    
    
    private static testMethod void testEight() { 
        Test.setMock(HttpCalloutMock.class, new MockAutodeskFileUploadResponderSuccess());
        
        myUpload_Item__c uploadItem = [SELECT Id FROM myUpload_Item__c LIMIT 1];
        uploadItem.Bucket_Name__c = 'Test';
        uploadItem.File_Name__c = 'Test';
        uploadItem.Autodesk_ObjectId__c = 'Test';
        uploadItem.Status__c = 'Thumbnail Creation Failed';
        uploadItem.No_of_Retries__c = 0;
        
        Test.startTest();
        update uploadItem;
        Test.stopTest();
    }
    
    
    class MockManifestResponderFailure implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setStatusCode(201);
            
            ResponseWrapperAutodeskMain responseBody = new ResponseWrapperAutodeskMain();
            responseBody.returnValue.bucketKey = 'Test';
            responseBody.returnValue.objectId = 'Test';
            responseBody.code = 200;
            response.setBody(JSON.serialize(responseBody));
            return response;
        }
    }
    
    
    private static testMethod void testNine() { 
        Test.setMock(HttpCalloutMock.class, new MockManifestResponderFailure());
        
        myUpload_Item__c uploadItem = [SELECT Id FROM myUpload_Item__c LIMIT 1];
        uploadItem.Bucket_Name__c = 'Test';
        uploadItem.File_Name__c = 'Test';
        uploadItem.Key__c = 'Test';
        uploadItem.AWS_Key__c = 'Test';
        uploadItem.Autodesk_ObjectId__c = 'Test';
        
        Test.startTest();
        update uploadItem;
        Test.stopTest();
    }
    
    
    private static testMethod void testTen() { 
        Test.setMock(HttpCalloutMock.class, new MockManifestResponderFailure());
        
        myUpload_Item__c uploadItem = [SELECT Id FROM myUpload_Item__c LIMIT 1];
        uploadItem.Bucket_Name__c = 'Test';
        uploadItem.File_Name__c = 'Test';
        uploadItem.Autodesk_ObjectId__c = 'Test';
        uploadItem.Status__c = 'Thumbnail Creation Failed';
        uploadItem.No_of_Retries__c = 0;
        
        Test.startTest();
        update uploadItem;
        Test.stopTest();
    }
    
    
    private static testMethod void testEleven() { 
        Test.setMock(HttpCalloutMock.class, new MockAutodeskFileUploadResponderSuccess());
        
        myUpload_Item__c uploadItem = [SELECT Id FROM myUpload_Item__c LIMIT 1];
        uploadItem.Bucket_Name__c = 'Test';
        uploadItem.File_Name__c = 'Test';
        uploadItem.Key__c = 'Test';
        uploadItem.AWS_Key__c = null;
        uploadItem.Autodesk_ObjectId__c = 'Test';
        
        Test.startTest();
        update uploadItem;
        Test.stopTest();
    }
    
    
    private static testMethod void testTwelve() { 
        Test.setMock(HttpCalloutMock.class, new MockAutodeskFileUploadResponderSuccess());
        
        myUpload_Item__c uploadItem = [SELECT Id FROM myUpload_Item__c LIMIT 1];
        
        Test.startTest();
        MyUploadItemTriggerHandler.manifestStatus(uploadItem.Id);
        Test.stopTest();
    }
    
    
    class MockManifestStatusResponderSuccess implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setStatusCode(201);
            
            response.setBody('{"status":"success"}');
            return response;
        }
    }
    
    
    private static testMethod void testAutodeskManifestStatusBatchSuccess() { 
        Test.setMock(HttpCalloutMock.class, new MockManifestStatusResponderSuccess());
        
        myUpload_Item__c uploadItem = [SELECT Id FROM myUpload_Item__c LIMIT 1];
        uploadItem.Bucket_Name__c = 'Test';
        uploadItem.File_Name__c = 'Test';
        uploadItem.Autodesk_ObjectId__c = 'Test';
        uploadItem.Status__c = 'Translation Initiated';
        uploadItem.No_of_Retries__c = 0;
        update uploadItem;
        
        Test.startTest();
        DateTime nextFireTime = System.now().addMinutes(1);
        
        String hour = String.valueOf(nextFireTime.hour());
        String min = String.valueOf(nextFireTime.minute());
        String sec = String.valueOf(nextFireTime.second());
        
        String cron = sec + ' ' + min + ' ' + hour + ' * * ?';
        
        System.schedule('AutodeskManifestStatusScheduler'+String.valueOf(nextFireTime), cron, new AutodeskManifestStatusBatchScheduler());
        Test.stopTest();
    }
    
    
    class MockManifestStatusResponderFailure implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setStatusCode(201);
            
            response.setBody('{"status":"failed"}');
            return response;
        }
    }
    
    
    private static testMethod void testAutodeskManifestStatusBatchFailure() { 
        Test.setMock(HttpCalloutMock.class, new MockManifestStatusResponderFailure());
        
        myUpload_Item__c uploadItem = [SELECT Id FROM myUpload_Item__c LIMIT 1];
        uploadItem.Bucket_Name__c = 'Test';
        uploadItem.File_Name__c = 'Test';
        uploadItem.Autodesk_ObjectId__c = 'Test';
        uploadItem.Status__c = 'Translation Initiated';
        uploadItem.No_of_Retries__c = 0;
        update uploadItem;
        
        Test.startTest();
        DateTime nextFireTime = System.now().addMinutes(1);
        
        String hour = String.valueOf(nextFireTime.hour());
        String min = String.valueOf(nextFireTime.minute());
        String sec = String.valueOf(nextFireTime.second());
        
        String cron = sec + ' ' + min + ' ' + hour + ' * * ?';
        
        System.schedule('AutodeskManifestStatusScheduler'+String.valueOf(nextFireTime), cron, new AutodeskManifestStatusBatchScheduler());
        Test.stopTest();
    }
    
    
    class MockManifestStatusResponderInProgress implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setStatusCode(201);
            
            response.setBody('{"status":"in progress"}');
            return response;
        }
    }
    
    
    private static testMethod void testAutodeskManifestStatusBatchInProgress() { 
        Test.setMock(HttpCalloutMock.class, new MockManifestStatusResponderInProgress());
        
        myUpload_Item__c uploadItem = [SELECT Id FROM myUpload_Item__c LIMIT 1];
        uploadItem.Bucket_Name__c = 'Test';
        uploadItem.File_Name__c = 'Test';
        uploadItem.Autodesk_ObjectId__c = 'Test';
        uploadItem.Status__c = 'Translation Initiated';
        uploadItem.No_of_Retries__c = 0;
        update uploadItem;
        
        Test.startTest();
        DateTime nextFireTime = System.now().addMinutes(1);
        
        String hour = String.valueOf(nextFireTime.hour());
        String min = String.valueOf(nextFireTime.minute());
        String sec = String.valueOf(nextFireTime.second());
        
        String cron = sec + ' ' + min + ' ' + hour + ' * * ?';
        
        System.schedule('AutodeskManifestStatusScheduler'+String.valueOf(nextFireTime), cron, new AutodeskManifestStatusBatchScheduler());
        Test.stopTest();
    }
}