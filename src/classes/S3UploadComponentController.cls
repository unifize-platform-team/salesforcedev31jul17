global class S3UploadComponentController{
    global String unsupportedFileTypes {get;set;}
    global String recordIdString {get;set;}
    
    global String folderPath {
        get{
            system.debug(recordIdString);
            String objectName = ((Id) recordIdString).getSObjectType().getDescribe().getName();
            Map<String, S3UploadFolderStructure__c> s3UploadStructure = S3UploadFolderStructure__c.getAll();
            
            if(s3UploadStructure.containsKey(objectName)){
                Type t = Type.forName(s3UploadStructure.get(objectName).ClassName__c);
                S3FolderStructure s3Structure = (S3FolderStructure) t.newInstance();
                
                return s3Structure.getFolderStructure(recordIdString);
            }
            else{
                return '';
            }
        }
        
        set;
    }
    
    
    global S3UploadComponentController(){
        unsupportedFileTypes = '';
        
        for(UnsupportedFileTypesForUpload__c unsupportedFile :UnsupportedFileTypesForUpload__c.getAll().values()){
            if(unsupportedFileTypes == ''){
                unsupportedFileTypes = unsupportedFile.Name;
            }
            else{
                unsupportedFileTypes = unsupportedFileTypes + ',' + unsupportedFile.Name;
            }
        }
    }
    
    
    @remoteAction
    global static List<myUpload_Item__c>  upsertData(List<S3DataWrapper> dataList){
        if(dataList.size() > 0){
            //Change the save logic if Controller is changed
            if(dataList[0].onSaveController <> null && dataList[0].onSaveController <> ''){
                Type t = Type.forName(dataList[0].onSaveController);
                iOnSave onSave = (iOnSave) t.newInstance();
                return onSave.execute(dataList);
            }
            else{
                Map<String, myUpload_Item__c> myUploadMap = new Map<String, myUpload_Item__c>();
                for(S3DataWrapper data :dataList){
                    myUpload_Item__c myUpload = new myUpload_Item__c();
                    myUpload.Id = data.uploadItemId;
                    myUpload.Bucket_Name__c = data.bucketName;
                    myUpload.ETag__c = data.eTag;
                    myUpload.File_Name__c = data.fileName;
                    myUpload.AWS_Key__c = data.key;
                    myUpload.Location__c = data.location.length() > 255 ? data.location.subString(0,255) : data.location;
                    myUpload.File_Size__c = data.fileSize;
                    myUpload.VersionId__c = data.versionId;
                    myUpload.EndPoint__c = AWS_S3Properties__c.getInstance().Region__c;
                    
                    List<String> tempList = data.fileName.split('\\.');
                    if(tempList != null && tempList.size() > 0){
                        myUpload.File_Type__c = tempList[tempList.size()-1];
                    }
                    
                    List<String> tempList2 = data.key.split('/');
                    if(tempList2 != null && tempList2.size() > 0){
                        myUpload.Key__c = data.key.left(data.key.length() - tempList2[tempList2.size()-1].length() - 1);
                    }
                    
                    myUpload.put(data.relatedFieldName, data.recordId);
                    
                    myUploadMap.put(myUpload.File_Name__c, myUpload);
                }
                
                update myUploadMap.values();
                
                return myUploadMap.values();
            }
        }
        else{
            return null;
        } 
    }
    
    
    @remoteAction
    global static List<myUpload_Item__c> getMyUploadItems(S3DataWrapper dataWrapper){
        List<myUpload_Item__c> myUploadItemList = new List<myUpload_Item__c>();
        
        for(Integer i=0; i<dataWrapper.numberOfFiles; i++){
            myUpload_Item__c tempMyUpload = new myUpload_Item__c();
            tempMyUpload.put(dataWrapper.relatedFieldName, dataWrapper.recordId);
            myUploadItemList.add(tempMyUpload);
        }
        
        insert myUploadItemList;
        
        return [SELECT Id, Name FROM myUpload_Item__c WHERE Id IN :myUploadItemList];
    } 
}