/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=false)
private class MyParts_List_Controller_Test {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account a=new Account();
        a.Name='Test Unifize';
        a.Type='Partner';
        a.AccountSource='Web';
        a.Account_Source_Detail__c='Found in the web';
        a.Status__c='Active';
        insert a;
        
        myMaterial__c m=new myMaterial__c();
        m.Name='Test Material';
        insert m;
        
        
   /*     __c p=new Process__c();
        p.Process_Name__c='test Process';
        insert p;*/
        
        Part__c mp=new Part__c();
        mp.Account__c=a.id;
        mp.myMaterial__c=m.id;
        mp.Drawing_No__c='DOO1';
        mp.myPart_Status__c='Active';
        insert mp;
        
        contact c=new contact();
        c.lastname='Test Unifize';
        c.Accountid=a.id;
        insert c;
        
        Machine__c machine=new Machine__c();
        //machine.Name='Welder';
        machine.Manufacturer__c=a.id;
        //machine.Process__c=p.id;
        insert machine;
        
        myFactory__c myFactory=new myFactory__c();
        myFactory.Partner__c= a.id;
        insert myFactory;
        
        myMachine__c myMachine=new myMachine__c();
        myMachine.Machine__c=machine.id;
        myMachine.myFactory__c=myFactory.id;
        myMachine.Serial_No__c='MAC-00012';
        insert myMachine;
        
        Recordtype r=[select id from recordtype where developername='Nest' limit 1];
        
        myProject__c mproj=new myProject__c();
        mproj.RecordTypeId=r.id;
        mproj.Project_Name__c='Testunifize';
        mproj.Status__c='Feasibility';
        mproj.Required_Date_Time__c=system.now();
        mproj.Partner__c=a.id;
        mproj.Partner_Contact__c=c.id;
        mproj.myMachine__c=myMachine.id;
        insert mproj;
        
        myProject__c myproj=new myProject__c();
        myproj.RecordTypeid=r.id;
        myproj.Project_Name__c='PROJ-000125';
        myproj.Status__c='Nest';
        myproj.Required_Date_Time__c=system.now();
        myproj.Partner__c=a.id;
        myproj.Partner_Contact__c=c.id;
        myproj.myMachine__c=myMachine.id;
        insert myproj;
        
        myProject_Part__c mpp=new myProject_Part__c();
        mpp.myPart__c=mp.id;
        mpp.myProject__c=mproj.id;
        mpp.Quantity__c=2;
        insert mpp;
        
        MyParts_List_Controller.getMyProjectParts(mproj.id,myproj.id);
        MyParts_List_Controller.getMyProjectParts(mproj.id,null);
        string myProjectPartsSelMap='{"'+mp.id+'":"2"}';
        string myProjectPartsSelMap2='{"'+mpp.id+'":"2"}';
        MyParts_List_Controller.addProjectParts1(mproj.id,null,myProjectPartsSelMap,true,false);
        MyParts_List_Controller.addProjectParts1(mproj.id,null,myProjectPartsSelMap,false,true);
        MyParts_List_Controller.checkExistingParts(mproj.id,myProjectPartsSelMap);
        MyParts_List_Controller.addProjectParts1(mproj.id,myproj.id,myProjectPartsSelMap2,true,false);
        MyParts_List_Controller.addProjectParts1(mproj.id,myproj.id,myProjectPartsSelMap2,false,true);
        
    }
}