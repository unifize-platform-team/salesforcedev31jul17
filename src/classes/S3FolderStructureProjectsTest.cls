@isTest(SeeAllData=false)
private class S3FolderStructureProjectsTest{
     @testSetup
    static void setup(){
        Account acc = new Account(Name = 'Test');
        insert acc;
        
        myProject__c proj = new myProject__c( 
            Partner__c = acc.Id
        );
        insert proj;
    }    
    
    
    private static testMethod void testOne() {
        myProject__c proj = [SELECT Id FROM myProject__c LIMIT 1];
        S3FolderStructureProjects s3FolderStructure = new S3FolderStructureProjects();
        s3FolderStructure.getFolderStructure(proj.Id);
    }
}