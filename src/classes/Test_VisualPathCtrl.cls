@istest
public class Test_VisualPathCtrl {
    static  testmethod void method1(){
        Account a = new Account(Name = 'Test Account Joe ');
        
        Opportunity o = new Opportunity();

             
        insert a;
         
        o.AccountId = a.Id;
        o.Name = 'Test_Joe_123';
        o.StageName = 'Prospecting';
        o.CloseDate = date.today();
        o.Type = 'New Business';
        insert o;
        VisualPathCtrl.getstatus('StageName',o.id);
        
    }//VisualPathPickListDescController
    
    static  testmethod void VisualPathPickListDescController(){
        
        
         List<recordtype> recList=[SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'myProject__c'];
        myProject__c  m=new myProject__c ();
        m.RecordTypeId=recList[0].id;
            m.Status__c='Upload';
        insert m;
        
        //Use the PageReference Apex class to instantiate a page
       PageReference pageRef = Page.VisualPathPicklistDesc;
       
       //In this case, the Visualforce page named 'success' is the starting point of this test method. 
       Test.setCurrentPage(pageRef);
       
         ApexPages.currentPage().getParameters().put('recordTypeId', recList[0].id);
        ApexPages.currentPage().getParameters().put('id', m.id);
         ApexPages.currentPage().getParameters().put('recordTypeName',  recList[0].DeveloperName);
         ApexPages.currentPage().getParameters().put('sobjectType', 'myProject__c');
         ApexPages.currentPage().getParameters().put('picklistFieldName', 'status__c');

       
        
        VisualPathPickListDescController v=new VisualPathPickListDescController();
        
        
        
    }
    
    static  testmethod void VisualPathPickListDescController2(){
        
        
         List<recordtype> recList=[SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'myProject__c'];
        myProject__c  m=new myProject__c ();
        m.RecordTypeId=recList[0].id;
            m.Status__c='Upload';
        insert m;
        
        //Use the PageReference Apex class to instantiate a page
       PageReference pageRef = Page.VisualPathPicklistDesc;
       
       //In this case, the Visualforce page named 'success' is the starting point of this test method. 
       Test.setCurrentPage(pageRef);
       

        ApexPages.currentPage().getParameters().put('id', m.id);
         ApexPages.currentPage().getParameters().put('recordTypeName',  recList[0].DeveloperName);
         ApexPages.currentPage().getParameters().put('sobjectType', 'myProject__c');
         ApexPages.currentPage().getParameters().put('picklistFieldName', 'status__c');

       
        
        VisualPathPickListDescController v=new VisualPathPickListDescController();
        
        
        
    }
    
    static  testmethod void VisualPathPickListDescControlle3(){
        
        
         List<recordtype> recList=[SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'myProject__c'];
        myProject__c  m=new myProject__c ();
        m.RecordTypeId=recList[0].id;
            m.Status__c='Upload';
        insert m;
        
        //Use the PageReference Apex class to instantiate a page
       PageReference pageRef = Page.VisualPathPicklistDesc;
       
       //In this case, the Visualforce page named 'success' is the starting point of this test method. 
       Test.setCurrentPage(pageRef);
       ApexPages.currentPage().getParameters().put('id', m.id);
		 ApexPages.currentPage().getParameters().put('picklistFieldName', 'status__c');
        VisualPathPickListDescController v=new VisualPathPickListDescController();
        
    }

}