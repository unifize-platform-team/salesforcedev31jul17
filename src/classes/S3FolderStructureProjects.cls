public class S3FolderStructureProjects implements S3FolderStructure{
    public String getFolderStructure(String recordId){
        myProject__c project = [SELECT Name, Partner__r.Account_ID__c FROM myProject__c WHERE Id = :recordId];
        
        String folderStructure = 'Jobs/Project_Jobs/'+project.Partner__r.Account_ID__c+'/'+project.Name+'/';
        
        return folderStructure;
    }
}