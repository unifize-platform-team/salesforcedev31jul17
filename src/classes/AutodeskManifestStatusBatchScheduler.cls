global class AutodeskManifestStatusBatchScheduler implements Schedulable {
    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new AutodeskManifestStatusBatch(), 1);
    }
}