global class AutodeskManifestStatusBatch implements Database.AllowsCallouts, Database.Batchable<sObject>{
    global Database.querylocator start(Database.BatchableContext BC){
        AutodeskIntegrationProperties__c autodeskIntegratonProperties = AutodeskIntegrationProperties__c.getInstance();
        Integer noOfRetries = autodeskIntegratonProperties.AutodeskManifestStatusRetry__c <> null ? (Integer) autodeskIntegratonProperties.AutodeskManifestStatusRetry__c : 30;
        
        return Database.getQueryLocator('SELECT Id FROM myUpload_Item__c WHERE (Status__c = \'Translation Initiated\' OR  Status__c = \'File Conversion Inprogress\') AND  (No_of_Retries__c < :noOfRetries OR No_of_Retries__c = null)');
    }
    
    
    global void execute(Database.BatchableContext BC, List<myUpload_Item__c> scope){
        for(myUpload_Item__c uploadItem :scope){
            AutodeskIntegrationHandler.manifestStatus(uploadItem.Id);
        }
    }
    
    global void finish(Database.BatchableContext BC){
        Map<Id, CronTrigger> scheduleJobsMap = new Map<Id, CronTrigger>([SELECT Id, CronJobDetail.Name 
            FROM CronTrigger 
            WHERE CronJobDetail.Name LIKE 'AutodeskManifestStatusScheduler%']);
        
                    
        for(Id jobId :scheduleJobsMap.keySet()){
            System.abortJob(jobId);
        }
        
        DateTime nextFireTime = System.now().addMinutes(1);
        
        String hour = String.valueOf(nextFireTime.hour());
        String min = String.valueOf(nextFireTime.minute());
        String sec = String.valueOf(nextFireTime.second());
        
        String cron = sec + ' ' + min + ' ' + hour + ' * * ?';
        
        System.debug('cron--'+cron);
        
        if(!Test.isRunningTest()){
            System.schedule('AutodeskManifestStatusScheduler'+String.valueOf(nextFireTime), cron, new AutodeskManifestStatusBatchScheduler());
        }
    }
}