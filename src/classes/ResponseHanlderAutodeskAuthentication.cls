public class ResponseHanlderAutodeskAuthentication{
     public static void processResponse(HttpResponse response){
         String responseString = response.getBody();
         system.debug(responseString);
         Map<String, object> bodyMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
         if(!Test.isRunningTest()){
             Cache.Org.put('AutodeskAccessToken', (String) bodyMap.get('access_token'));
         }
     }
}