public class DirectEmailOptionHandler {
    @auraEnabled
    public static List<EmailTemplate> getEmailTemplate()
    {
       return [SELECT Name,Subject,TemplateType,Description,Body FROM EmailTemplate];
    }
    
    @auraEnabled
    public static String getEmailId(String recordId){
        return [SELECT email from Contact WHERE id=:recordId].email;
    }
    
    @AuraEnabled 
    public static void sendMailMethod(String mMail ,String mSubject ,String mbody){
    
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        // Set recipients to two contact IDs.
        // Replace IDs with valid record IDs in your org.
        message.toAddresses =new String[]{mMail};
        message.subject = mSubject;
        message.plainTextBody = mbody;
        Messaging.SingleEmailMessage[] messages = 
            new List<Messaging.SingleEmailMessage> {message};
                 Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        System.debug('res : '+results);
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: '
                  + results[0].errors[0].message);
        }
   }   
    

}