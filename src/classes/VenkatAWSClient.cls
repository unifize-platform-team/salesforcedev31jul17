global class VenkatAWSClient 
{
	/*
     * Class Description:
     * AWS SDK client for Unifize
     * 
     * Author : venkat.reddy@unifize.com
     * Creation Date : 04-05-2017
     * Comments : 
     */
     
	public String access  = '';
	public String secret = '';
	public String region = '';
	public Connector connector;
	
     
 	public void VenkatAWSClient()
	{	  		
 		try
 		{
 			connector = new Connector(access, secret); 	
 			if (UNIFIZE_GLOBAL_CONFIGS.DEBUG)
    		{
    			System.debug('AWS connector created successfully');
    		} 		
 		}
 		catch (Exception eX)
 		{
 			if (UNIFIZE_GLOBAL_CONFIGS.DEBUG)
    		{
    			System.debug('Exception while creating AWS connector:' + eX.getMessage());
    		} 			
 		} 		
 		return;
	}
	
	//create a bucket
	public void createBucket(UnifizeMethodResult retResult)
	{
		try
 		{
 			S3 s3 = connector.s3(region);
 			String name = 'TheBucketList';
			s3.createBucket(name);
 			if (UNIFIZE_GLOBAL_CONFIGS.DEBUG)
    		{
    			System.debug('TheBucketList bucket created successfully!');
    		} 	
    		retResult.result = true;	
 		}
 		catch (Exception eX)
 		{
 			retResult.result = false;
 			if (UNIFIZE_GLOBAL_CONFIGS.DEBUG)
    		{
    			System.debug('Exception while creating TheBucketList bucket:' + eX.getMessage());
    		} 			
 		} 		
 		return;		
	}
	
	//add an object to bucket
	public void addObjectToBucket(String bucketName, UnifizeMethodResult retResult)
	{		
		try
 		{
 			S3.Bucket bucket = connector.s3(region).bucket(bucketName);
			Map<String,String> headers = new Map<String,String>{'Content-Type' => 'text/plain'};
			bucket.createContent('foo.txt', headers, Blob.valueOf('bar'));
 			if (UNIFIZE_GLOBAL_CONFIGS.DEBUG)
    		{
    			System.debug('The object is added to the bucket successfully!');
    		} 	
    		retResult.result = true;	
 		}
 		catch (Exception eX)
 		{
 			retResult.result = false;
 			if (UNIFIZE_GLOBAL_CONFIGS.DEBUG)
    		{
    			System.debug('Exception while adding object to the bucket:' + eX.getMessage());
    		} 			
 		} 		
 		return;			
	}
	
	//get list of buckets
	public void getBucketsList(UnifizeMethodResult retResult) 
	{           
    	try
    	{
			S3 s3 = connector.s3(region);
        	List<S3.Bucket> buckets = s3.listBuckets(); 
        	if (UNIFIZE_GLOBAL_CONFIGS.DEBUG)
    		{
    			System.debug('Number of buckets in S3 are:' + buckets.size());
    		} 
        	retResult.result = true;    
    	}
    	catch(Exception eX)
    	{
    		retResult.result = false;
    		if (UNIFIZE_GLOBAL_CONFIGS.DEBUG)
    		{
    			System.debug('Exception while getting buckets list:' + eX.getMessage());
    		}
    	}          
    }	        
}