public class ThumbnailCompForUnicheckQuery {
    
    //before
	@AuraEnabled
    public static uniCHECK_Query__c getAttachedFileDetailsBefore(String recordId){
        return [SELECT Id, myItem_Version__r.Current_Attached_File__c FROM uniCHECK_Query__c WHERE Id = :recordId];
    }
    
    //after
    @AuraEnabled
    public static uniCHECK_Query__c getAttachedFileDetailsAfter(String recordId){
        return [SELECT Id, myItem_Version_After__r.Current_Attached_File__c FROM uniCHECK_Query__c WHERE Id = :recordId];
    }
}