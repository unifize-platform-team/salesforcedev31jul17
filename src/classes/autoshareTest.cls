@isTest
public class autoshareTest {
    
    public static testmethod void  jobApplicationShareTest(){
        position__c pst1 = new position__c (name='TestP');
        pst1.Recruiter_1__c=userinfo.getUserId();
        pst1.Recruiter_2__c=userinfo.getUserId();
        pst1.Recruiter_3__c=userinfo.getUserId();
        pst1.Recruiter_4__c=userinfo.getUserId();
        pst1.Recruiter_5__c=userinfo.getUserId();
        pst1.Recruiter_6__c=userinfo.getUserId();
        pst1.Recruiter_7__c=userinfo.getUserId();
        pst1.Recruiter_8__c=userinfo.getUserId();
        pst1.Recruiter_9__c=userinfo.getUserId();
        pst1.Recruiter_10__c=userinfo.getUserId();
        insert pst1 ;
        
        
        Candidate__c cnd = new Candidate__c();
        cnd.Aadhar_Card_No__c='123456789';
        insert cnd ;
        
        
        Job_application__c japp = new Job_application__c();
        japp.position__c =pst1.id;
        japp.Candidate__c=cnd.id;
        insert japp;
        
        position__c pst=[Select id,Recruiter_1__c,Recruiter_2__c, Recruiter_3__c,
                          Recruiter_10__c,Recruiter_4__c, Recruiter_5__c, Recruiter_6__c,Recruiter_7__c,
                          Recruiter_8__c,Recruiter_9__c from position__c where name ='TestP' Limit 1 ];
        pst.Recruiter_1__c=null;
        pst.Recruiter_2__c=null;
        pst.Recruiter_3__c=null;
        pst.Recruiter_4__c=null;
        pst.Recruiter_5__c=null;
        pst.Recruiter_6__c=null;
        pst.Recruiter_7__c=null;
        pst.Recruiter_8__c=null;
        pst.Recruiter_9__c=null;
        pst.Recruiter_10__c=null;
        update pst;
        
        test.startTest();
        pst.Recruiter_1__c=userinfo.getUserId();
        pst.Recruiter_2__c=userinfo.getUserId();
        pst.Recruiter_3__c=userinfo.getUserId();
        pst.Recruiter_4__c=userinfo.getUserId();
        pst.Recruiter_5__c=userinfo.getUserId();
        pst.Recruiter_6__c=userinfo.getUserId();
        pst.Recruiter_7__c=userinfo.getUserId();
        pst.Recruiter_8__c=userinfo.getUserId();
        pst.Recruiter_9__c=userinfo.getUserId();
        pst.Recruiter_10__c=userinfo.getUserId();
        update pst;
        system.debug('????' +japp.position__r.Recruiter_1__c);
        system.debug('????'+japp.position__r.Recruiter_2__c);
        delete japp;
        
        Test.stopTest();
        
        
        
    }
}