@isTest(SeeAllData=false)
private class S3UploadComponentControllerTest{
    
    @testSetup
    static void setup(){
        Account acc = new Account(Name = 'Test');
        insert acc;
        
        myUpload_Item__c uploadItem = new myUpload_Item__c(Account__c = acc.Id);
        insert uploadItem;
        
        UnsupportedFileTypesForUpload__c unsupportedFileType = new UnsupportedFileTypesForUpload__c(
            Name = 'test'
        );
        insert unsupportedFileType;
        
        UnsupportedFileTypesForUpload__c unsupportedFileType2 = new UnsupportedFileTypesForUpload__c(
            Name = 'test2'
        );
        insert unsupportedFileType2;
        
        Part__c part = new Part__c(
            Account__c = acc.Id
        );
        insert part;
        
        insert new AutodeskIntegrationProperties__c(
            BucketKey__c = 'Test',
            ClientId__c = 'Test',
            ClientSecret__c = 'Test',
            FileUploadEndPoint__c = 'Test',
            AuthenticationEndpoint__c = 'Test',
            AutodeskManifestEndpoint__c = 'Test'
        );
        
    }
    
    
    class MockAutodeskFileUploadResponderFailure implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setStatusCode(200);
            
            ResponseWrapperAutodeskMain responseBody = new ResponseWrapperAutodeskMain();
            responseBody.returnValue.bucketKey = 'Test';
            response.setBody(JSON.serialize(responseBody));
            return response;
        }
    }
        
        
    private static testMethod void testOne() {
        myUpload_Item__c uploadItem = [SELECT Id, Account__c FROM myUpload_Item__c LIMIT 1];
        S3DataWrapper s3DataWrap = new S3DataWrapper();
        s3DataWrap.uploadItemId = uploadItem.Id;
        s3DataWrap.location = 'https://test.test.unifizetest.com/test/test/test.csv';
        s3DataWrap.key = '/test/test/test.csv';
        s3DataWrap.fileName = '/test/test/test.csv';
        s3DataWrap.recordId = uploadItem.Account__c ;
        s3DataWrap.relatedFieldName = 'Account__c';
        
        Test.setMock(HttpCalloutMock.class, new MockAutodeskFileUploadResponderFailure());
        
        Test.startTest();
        List<myUpload_Item__c> uploadItemList = S3UploadComponentController.upsertData(new List<S3DataWrapper>{s3DataWrap});
        Test.stopTest();
        
        System.assert(uploadItemList.size() == 1);
    }
    
    
    private static testMethod void testTwo() {
        myUpload_Item__c uploadItem = [SELECT Id, Account__c FROM myUpload_Item__c LIMIT 1];
        S3DataWrapper s3DataWrap = new S3DataWrapper();
        s3DataWrap.uploadItemId = uploadItem.Id;
        s3DataWrap.location = 'https://test.test.unifizetest.com/test/test/test.csv';
        s3DataWrap.key = '/test/test/test.csv';
        s3DataWrap.fileName = '/test/test/test.csv';
        s3DataWrap.recordId = uploadItem.Account__c ;
        s3DataWrap.relatedFieldName = 'Account__c';
        s3DataWrap.numberOfFiles = 2;
        
        Test.startTest();
        List<myUpload_Item__c> uploadItemList = S3UploadComponentController.getMyUploadItems(s3DataWrap);
        Test.stopTest();
        
        System.assert(uploadItemList.size() == 2);
    }
    
    
    private static testMethod void testThree() {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        S3UploadComponentController s3Uploader = new S3UploadComponentController();
        s3Uploader.recordIdString = acc.Id;
        String folderPath = s3Uploader.folderPath;
        System.assert(s3Uploader.unsupportedFileTypes.length() == 10);
    }
    
    
    private static testMethod void testFour() {
        myUpload_Item__c uploadItem = [SELECT Id, Account__c FROM myUpload_Item__c LIMIT 1];
        S3DataWrapper s3DataWrap = new S3DataWrapper();
        s3DataWrap.uploadItemId = uploadItem.Id;
        s3DataWrap.location = 'https://test.test.unifizetest.com/test/test/test.csv';
        s3DataWrap.key = '/test/test/test.csv';
        s3DataWrap.fileName = '/test/test/test.csv';
        s3DataWrap.recordId = [SELECT Id FROM Part__c LIMIT 1].Id;
        s3DataWrap.relatedFieldName = 'myItem__c';
        s3DataWrap.onSaveController = 'S3UploadComponentOnSaveMyItem';
        
        Test.setMock(HttpCalloutMock.class, new MockAutodeskFileUploadResponderFailure());
        
        Test.startTest();
        List<myUpload_Item__c> uploadItemList = S3UploadComponentController.upsertData(new List<S3DataWrapper>{s3DataWrap});
        Test.stopTest();
        
        System.assert(uploadItemList.size() == 1);
    }
}