public class RequestProviderAutodeskAuthentication{
    public static HttpRequest getRequest(){
        AutodeskIntegrationProperties__c autodeskIntegratonProperties = AutodeskIntegrationProperties__c.getInstance();
        
        HttpRequest request = new HttpRequest();
        
        String body = '';
        body += 'grant_type=client_credentials';
        body += '&client_id='+autodeskIntegratonProperties.ClientId__c;
        body += '&client_secret='+autodeskIntegratonProperties.ClientSecret__c;
        body += '&scope=data:read data:write data:create bucket:read bucket:create viewables:read';
        
        request.setEndpoint(autodeskIntegratonProperties.AuthenticationEndpoint__c);
        request.setMethod('POST');
        request.setHeader('Content-Type','application/x-www-form-urlencoded');
        request.setBody(body);
        
        return request;
    }
}