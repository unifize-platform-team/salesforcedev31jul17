public class VisualPathPicklistDescriber {
    static final Pattern OPTION_PATTERN = Pattern.compile('<option.+?>(.+?)</option>'); 
    
    /**
        Desribe a picklist field for an sobject id. RecordType is automatically picked
        based on the record's RecordTypeId field value.
        example usage :
        List<String> options = VisualPathPicklistDescriber.describe(accountId, 'Industry');
    */
    public static List<String> describe(Id sobjectId, String pickListFieldAPIName) {
        return parseOptions(
                            new Map<String, String> {
                                                     'id' => sobjectId,
                                                     'pickListFieldName'=> pickListFieldAPIName
                                                    }
                            );
    }
    
    /**
        Describe a picklist field for a SobjectType, its given record type developer name and the picklist field
        example usage : 
        List<String> options = VisualPathPicklistDescriber.describe('Account', 'Record_Type_1', 'Industry'));
    */
    /*
    public static List<String> describe(String sobjectType, String recordTypeName, String pickListFieldAPIName) {
        return parseOptions(
                            new Map<String, String> {
                                                     'sobjectType' => sobjectType,
                                                     'recordTypeName' => recordTypeName,
                                                     'pickListFieldName'=> pickListFieldAPIName
                                                    }
                            );
    }*/
    
    /**
        Describe a picklist field for a SobjectType, its given record type ID and the picklist field
        example usage : 
        Id recType1Id = [Select Id from RecordType Where SobjectType = 'Account' 
                                            AND DeveloperName like 'Record_Type_2'].Id;
        System.assertEquals(REC_TYPE_1_OPTIONS, VisualPathPicklistDescriber.describe('Account', recType2Id, 'Industry'));
    */
    /*
    public static List<String> describe(String sobjectType, Id recordTypeId, String pickListFieldAPIName) {
        return parseOptions(
                            new Map<String, String> {
                                                     'sobjectType' => sobjectType,
                                                     'recordTypeId' => recordTypeId,
                                                     'pickListFieldName'=> pickListFieldAPIName
                                                    }
                            );
    }*/
    
    /*
        Internal method to parse the OPTIONS
    */
    static List<String> parseOptions(Map<String, String> params) {
        Pagereference pr = Page.VisualPathPicklistDesc;
        // to handle development mode, if ON
        pr.getParameters().put('core.apexpages.devmode.url', '1');
        
        for (String key : params.keySet()) {
            pr.getParameters().put(key, params.get(key));   
        }
        
        String xmlContent='';
               
        if(!test.isRunningTest()){
            xmlContent = pr.getContent().toString();
          }else{
            xmlContent +='<form id="j_id0:j_id1" name="j_id0:j_id1" method="post" action="https://unifize-portal--Vendor1--c.cs58.visual.force.com/apex/visualpathpicklistdesc?id=a2J0l0000008OTSEA2" enctype="application/x-www-form-urlencoded">';
            xmlContent +='<input type="hidden" name="j_id0:j_id1" value="j_id0:j_id1" />';
            xmlContent +='<script> if(!window.sfdcPage) { window.sfdcPage = new ApexDetailPage(); }UserContext.initialize({"ampm":["AM","PM"],"isAccessibleMode":false,"uiSkin":"Theme3","salesforceURL":"https://unifize-portal--Vendor1.cs58.my.salesforce.com","dateFormat":"M/d/yyyy","language":"en_US","locale":"en_US","userName":"sysadmin@unifize.com.vendor1","userId":"0056F000006zHGu","isCurrentlySysAdminSU":false,"renderMode":"RETRO","startOfWeek":"1","vfDomainPattern":"unifize-portal--Vendor1--(?:[^.]+)*.cs58.visual.force.com","auraDomain":"unifize-portal--Vendor1.lightning.force.com","dateTimeFormat":"M/d/yyyy h:mm a","orgPreferences":[{"index":257,"name":"TabOrganizer","value":true},{"index":113,"name":"GroupTasks","value":true}],"siteUrlPrefix":"","isDefaultNetwork":true,"labelLastModified":"1495737584000","today":"5/31/2017 6:44 PM","timeFormat":"h:mm a","userPreferences":[{"index":112,"name":"HideInlineEditSplash","value":false},{"index":114,"name":"OverrideTaskSendNotification","value":false},{"index":115,"name":"DefaultTaskSendNotification","value":false},{"index":119,"name":"HideUserLayoutStdFieldInfo","value":false},{"index":116,"name":"HideRPPWarning","value":true},{"index":87,"name":"HideInlineSchedulingSplash","value":false},{"index":88,"name":"HideCRUCNotification","value":false},{"index":89,"name":"HideNewPLESplash","value":false},{"index":90,"name":"HideNewPLEWarnIE6","value":false},{"index":122,"name":"HideOverrideSharingMessage","value":false},{"index":91,"name":"HideProfileILEWarn","value":false},{"index":93,"name":"HideProfileElvVideo","value":false},{"index":97,"name":"ShowPicklistEditSplash","value":false},{"index":92,"name":"HideDataCategorySplash","value":false},{"index":128,"name":"ShowDealView","value":false},{"index":129,"name":"HideDealViewGuidedTour","value":false},{"index":132,"name":"HideKnowledgeFirstTimeSetupMsg","value":false},{"index":104,"name":"DefaultOffEntityPermsMsg","value":false},{"index":135,"name":"HideNewCsnSplash","value":false},{"index":101,"name":"HideBrowserWarning","value":false},{"index":139,"name":"HideDashboardBuilderGuidedTour","value":true},{"index":140,"name":"HideSchedulingGuidedTour","value":false},{"index":180,"name":"HideReportBuilderGuidedTour","value":true},{"index":183,"name":"HideAssociationQueueCallout","value":false},{"index":194,"name":"HideQTEBanner","value":false},{"index":193,"name":"HideChatterOnboardingSplash","value":false},{"index":195,"name":"HideSecondChatterOnboardingSplash","value":true},{"index":270,"name":"HideIDEGuidedTour","value":false},{"index":282,"name":"HideQueryToolGuidedTour","value":false},{"index":196,"name":"HideCSIGuidedTour","value":false},{"index":271,"name":"HideFewmetGuidedTour","value":false},{"index":272,"name":"HideEditorGuidedTour","value":false},{"index":205,"name":"HideApexTestGuidedTour","value":false},{"index":206,"name":"HideSetupProfileHeaderTour","value":true},{"index":207,"name":"HideSetupProfileObjectsAndTabsTour","value":true},{"index":213,"name":"DefaultOffArticleTypeEntityPermMsg","value":false},{"index":214,"name":"HideSelfInfluenceGetStarted","value":false},{"index":215,"name":"HideOtherInfluenceGetStarted","value":false},{"index":216,"name":"HideFeedToggleGuidedTour","value":false},{"index":268,"name":"ShowChatterTab178GuidedTour","value":false},{"index":275,"name":"HidePeopleTabDeprecationMsg","value":false},{"index":276,"name":"HideGroupTabDeprecationMsg","value":false},{"index":224,"name":"HideUnifiedSearchGuidedTour","value":false},{"index":226,"name":"ShowDevContextMenu","value":true},{"index":227,"name":"HideWhatRecommenderForActivityQueues","value":false},{"index":228,"name":"HideLiveAgentFirstTimeSetupMsg","value":false},{"index":232,"name":"HideGroupAllowsGuestsMsgOnMemberWidget","value":false},{"index":233,"name":"HideGroupAllowsGuestsMsg","value":false},{"index":234,"name":"HideWhatAreGuestsMsg","value":false},{"index":235,"name":"HideNowAllowGuestsMsg","value":false},{"index":236,"name":"HideSocialAccountsAndContactsGuidedTour","value":true},{"index":237,"name":"HideAnalyticsHomeGuidedTour","value":true},{"index":238,"name":"ShowQuickCreateGuidedTour","value":false},{"index":245,"name":"HideFilePageGuidedTour","value":false},{"index":250,"name":"HideForecastingGuidedTour","value":false},{"index":251,"name":"HideBucketFieldGuide","value":false},{"index":263,"name":"HideSmartSearchCallOut","value":true},{"index":265,"name":"HideSocialProfilesKloutSplashScreen","value":false},{"index":273,"name":"ShowForecastingQuotaAttainment","value":false},{"index":280,"name":"HideForecastingQuotaColumn","value":false},{"index":301,"name":"HideManyWhoGuidedTour","value":false},{"index":284,"name":"HideExternalSharingModelGuidedTour","value":false},{"index":298,"name":"HideFileSyncBannerMsg","value":false},{"index":299,"name":"HideTestConsoleGuidedTour","value":false},{"index":302,"name":"HideManyWhoInlineEditTip","value":false},{"index":303,"name":"HideSetupV2WelcomeMessage","value":true},{"index":312,"name":"ForecastingShowQuantity","value":false},{"index":313,"name":"HideDataImporterIntroMsg","value":false},{"index":314,"name":"HideEnvironmentHubLightbox","value":false},{"index":316,"name":"HideSetupV2GuidedTour","value":false},{"index":317,"name":"HideFileSyncMobileDownloadDialog","value":false},{"index":322,"name":"HideEnhancedProfileHelpBubble","value":false},{"index":328,"name":"ForecastingHideZeroRows","value":false},{"index":330,"name":"HideEmbeddedComponentsFeatureCallout","value":true},{"index":341,"name":"HideDedupeMatchResultCallout","value":false},{"index":340,"name":"HideS1BrowserUI","value":false},{"index":346,"name":"HideS1Banner","value":true},{"index":358,"name":"HideEmailVerificationAlert","value":false},{"index":354,"name":"HideLearningPathModal","value":false},{"index":359,"name":"HideAtMentionsHelpBubble","value":false},{"index":368,"name":"LightningExperiencePreferred","value":true}],"networkId":""});';
            xmlContent +='</script><select  id="j_id0:j_id1:j_id2" name="j_id0:j_id1:j_id2"><option value="">--None--</option><option value="Upload" selected="selected">Upload</option>';
            xmlContent +='<option value="Part Creation">Part Creation</option>';
            xmlContent +='<option value="Clarifize">Clarifize</option>';
            xmlContent +='<option value="Complete">Complete</option>';
            xmlContent +='</select><div id="j_id0:j_id1:j_id3"></div>';
            xmlContent +='</form><span id="ajax-view-state-page-container" style="display: none"><span id="ajax-view-state" style="display: none"><input type="hidden"  id="com.salesforce.visualforce.ViewState" name="com.salesforce.visualforce.ViewState" value="i:AAAAWXsidCI6IjAwRDBsMDAwMDAwMFB3RSIsInYiOiIwMDAwMDAwMDAwMDAwMDAiLCJhIjoidmZlbmNyeXB0aW9ua2V5IiwidSI6IjAwNTZGMDAwMDA2ekhHdSJ9n+FuNpHm+t3h202lCZ1Btkgsp/RJhbRS6uTeCAAAAVxepLvdd7IFA3oaVq8YxBcx+zAqYCkVuvOUZcQEwT3hl/mdll6kyDxphByJhkZMTXSlgh8O6+8xSq2WQfi7g2mGXbnDB9O5q95hNv9d+SUz0pJJ8s99+sW66c6y51URW28XkzFX5D+tZS3EwRt9rbi7+tmqnBPJWT7TtWmBYDKW7sf3YiMBKRdJh73p25tn8G2OT+kyzEZ3O+Uh4iQCh/7CO1rTaJ+izlpZRJdkLQgAP6zy7IaIuEkhhNSSFQUVRrneEMJGWYO1a9flAMUL6X3YjlZVqBf1u7zgLTIWgsRrkuyOXaXuIM7z9WwlF+NNVHAGyX9AgYqD4dCAwGozDB4ViGNj5x0dIA5RHricsvDk+NTk3Dy5Mm7BNZ/6EO7PxrlHyiOIsjcTQNIkoV2DWGwC46DUjjIiM7hMx/Ux0XQGKTxUvZLX1XyfFBc8RRWsff/fiLumMdTRbGNztrHMsTqTeoaQr4F3P7OdvOBqc1kqnkVS3RnAor1ZBIKyginF7uXGpoDIWBFOOK9iaasjkmKouSw5k5t+oy7dhw6EViK+HFHP25HDXlW3qbkj5SnBjgKokjpcTZm+w5o1LQ4DVefBrt0OApW3PV2yKr2BrWe5E1r3EGbFaZfE9PRLpSUNdUHZrnHKRToXMFjGcF0shpfdb9z9lqC/PktOhSIRvsZhZL696roLFl+W7WF0v7CYrMYw6lfqQyUqCiJY7HuHksxOlCuNOORIQ0EdQG8e7xki/61LNSvxqMUzTd7XuhFBr8WV3QYtpGpejJgYRwXXqYKQQekPKxjJOhzAT8DVb5dDmBTlb2313eCqRf50WOE+KsHwlpFvfzGzykBRR/3GU0ZmG3S5yo8VwpH81mI0IG/5G7Pt7jC93UcX9Ds8PgGugLdm6hKEzU37jdwVkWjOcg3rprCUfCcX4dg4ipYe6lEl1Vz5ZRjGirMsQodmoLK34sEyTftvJ/38g8fQlDnY2CEB2rb0lj9LbtziLWJNPYewHTPQEFYcj10e5w6XetGTH8mhNNy+4AOoI64cmOiizC+rG6jtitXWrCtuCKnzd8oBCbCNkEe2w05LZREtcE+QqpFV4YQXycmQ2HVTJe7GTlLZX9dF3jOoiCOUVJN+slj1yYMFTQ3b0T+02UTxHo7C+4/XuKPQNtH13YF6UMwaXnOoQQjGU/f10gGD0jpapmybOy9TE5EpTQIYBO9gvHZ5adFcmBkN3oa2Urvrk/g0ao0vcUUV6ZPq4wwrYuazaLXXP+jLeXZ5IyWGzIjsjVxpkQXpP0/VS4G3p2KIyXh+GrqPacP8qup7CPA+QBkrD2/oSEkTyyMWVjSG0/FozOn/5wT99H9kkx9xIc3zZ1vHg+OB2AjBvpqRb5d95PQD5d0qpOnqTrKgkNhJT4Ko/ZwiQBPSzU3gHprzPZVUSNoF3bKdLkBqf+cEhBYumkudqwCoiAKoST1ZDQ3AijArGWvPf/4iKDbgCaQaLxsRjqdFDm77CC+HH90LNX4gaXtLWxaNHyjcOZVeHJAHpo71qR2g+4W9/WS4hLvR12gnfZ8duLxbl0S8j4/0oGlhjt/apMOI1/iExsUGy+pyGqCjazvSsSogFyHjZ6VfC3qRVIFGrLw3M4NDU39u4ornahSUgdApRoDpUuToY05ZFuw8kIlPjhsQbCfIWKkK+2332XB1Zq/lB1bcvkxUahcvq8kmXHlvQc1cFFf7P9e5Rvx5bCHKLnLts42kt1k07oxirhKKc87idsyHdBfEZlYb616kP5BTBlAKbtx3VIGJBzfG1qacLhDd4HZ8FWv5bnRqB06spEPmhlNCnZCx0ADmbrFo1kf3DEkqwhkDy5d/izkoSTS8voKxQUn/t3D+qpr2fUhaO/iAchyCTFF9MPS7Mr2n35vk9GfLlJw2ifvNg7hgoshnaYI/5Cx3XcuQMimJZLYu//burN5sA3MZggrGoVfpX9Puf+YQ/H9KAICn6S2VH6YDznFddY5cC9UAQx7eqwGK+8Y+k0N5ENrzB1Rbzn25Tl5LAs5KxiuqmaOLWEJL75ivnVg9UxCCI7MrPMeQUybJkAgglrRpd0M0lYViFIawQTyWlsAk0S1I9Mep8SaEIkR5z5vblsKfrbKAdzvpxqEhZYbfn4TBlWHZJX3zA7gX2tiWQ15+rwmFNN8hTkmPqodBU1EzVT89rp9ac+WUFCKZ/yqYzFn/WZbQ3TgOQpKACZzD3nY+WCNZ9oFgwVs2F2u/EsY6pM0vm/Xdy8swDQy2bG4JXbWBbQfxHytcCFuYt+OumPQGUAgJXoAO7KDyBCGFbJCqdSo/6H+EPLlFn1vFfBWB4Adeqi/l7DmEG2jkbQySb6DgwAdHqtG2tQ9cPX+0jLo+A6mh3Bb4J2H8wog=" /><input type="hidden"  id="com.salesforce.visualforce.ViewStateVersion" name="com.salesforce.visualforce.ViewStateVersion" value="201705251823440178" /><input type="hidden"  id="com.salesforce.visualforce.ViewStateMAC" name="com.salesforce.visualforce.ViewStateMAC" value="AGV5SnViMjVqWlNJNkluQlVRM1U0YzNWTGRrbHZhVkpxZWt0clJFbHdNWEZ2YzFGeFNua3hSbEZRZEZWMWJITndRbXh5UlRCY2RUQXdNMlFpTENKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUlzSW10cFpDSTZJbnRjSW5SY0lqcGNJakF3UkRCc01EQXdNREF3TUZCM1JWd2lMRndpZGx3aU9sd2lNREF3TURBd01EQXdNREF3TURBd1hDSXNYQ0poWENJNlhDSjJabk5wWjI1cGJtZHJaWGxjSWl4Y0luVmNJanBjSWpBd05UWkdNREF3TURBMmVraEhkVndpZlNJc0ltTnlhWFFpT2xzaWFXRjBJbDBzSW1saGRDSTZNVFE1TmpJek5qUTNNek13T1N3aVpYaHdJam93ZlE9PS4uNlNFZzhvSTd6ZnA5cDBZU3E5VWQtMm00c1JKOHUzcm9uZGtwQlFvd1hZdz0=" /><input type="hidden"  id="com.salesforce.visualforce.ViewStateCSRF" name="com.salesforce.visualforce.ViewStateCSRF" value="VmpFPSxNakF4Tnkwd05pMHdNMVF4TXpveE5Eb3pNeTR6TVRCYSx6UXZQblFYQnA3OTk0dFNCSEFBdUlJLE56QmlPV0U1" /></span></span><script type="text/javascript">Sfdc.onReady(function(){';
            xmlContent +='  SfdcApp && SfdcApp.Visualforce && SfdcApp.Visualforce.VSManager && SfdcApp.Visualforce.VSManager.vfPrepareForms(["j_id0:j_id1"]);';
            xmlContent +='';
            xmlContent +='});</script>';
          }
        
                
        Matcher mchr = OPTION_PATTERN.matcher(xmlContent);
        List<String> options = new List<String>();
        while(mchr.find()) {
            options.add(mchr.group(1));
        } 
        // remove the --None-- element
        if (!options.isEmpty()) options.remove(0);
        return options;
    }
}