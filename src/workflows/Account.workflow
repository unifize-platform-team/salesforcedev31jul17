<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Billing_Address_Same_As_Shipping_Address</fullName>
        <description>To copy address from billing address to shipping address.</description>
        <field>ShippingStreet</field>
        <formula>BillingStreet</formula>
        <name>Billing Address Same As Shipping Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Billing_address_to_shipping_address</fullName>
        <field>ShippingStreet</field>
        <formula>BillingStreet</formula>
        <name>Copy Billing address to shipping address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Copy the Billing address to Shipping address</fullName>
        <actions>
            <name>Copy_Billing_address_to_shipping_address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Same_as__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This rule copies the Billing address to Shipping address when a check box is checked.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
