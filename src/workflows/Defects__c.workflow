<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>All_Priority_Defects</fullName>
        <description>All Priority Defects</description>
        <protected>false</protected>
        <recipients>
            <recipient>Chief_Technical_Officer</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Salesforce_Developer</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Defects_Email_Template/Defects_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Defect_Email_template</fullName>
        <description>Defect:Email template</description>
        <protected>false</protected>
        <recipients>
            <recipient>Co_Founder</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Chief_Technical_Officer</recipient>
            <type>roleSubordinatesInternal</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Defects_Email_Template/Defects_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_to_developer</fullName>
        <description>Email to developer</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_to__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Development_Manager_Templates/email_developer_on_assigning_defect</template>
    </alerts>
    <alerts>
        <fullName>High_Priority_Defects</fullName>
        <description>High Priority Defects</description>
        <protected>false</protected>
        <recipients>
            <recipient>Chief_Technical_Officer</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Co_Founder</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Salesforce_Developer</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Defects_Email_Template/Defects_Email_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Defect_Closure_Date</fullName>
        <field>Defect_Closure_Date__c</field>
        <formula>now()</formula>
        <name>Defect Closure Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>All Priority Defects</fullName>
        <actions>
            <name>All_Priority_Defects</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Defects__c.Priority__c</field>
            <operation>equals</operation>
            <value>Medium,Low</value>
        </criteriaItems>
        <description>For all low &amp; medium priority defects</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Defect Closure date</fullName>
        <actions>
            <name>Defect_Closure_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Defects__c.Defect_Status__c</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email developer with defect details</fullName>
        <actions>
            <name>Email_to_developer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(Assigned_to__c &lt;&gt; null,ISCHANGED(Assigned_to__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>High Priority Defects</fullName>
        <actions>
            <name>High_Priority_Defects</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Defects__c.Priority__c</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <description>High priority defects email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
