<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Upload</fullName>
        <description>New Upload</description>
        <protected>false</protected>
        <recipients>
            <recipient>Service_Delivery_Head</recipient>
            <type>roleSubordinatesInternal</type>
        </recipients>
        <recipients>
            <recipient>ben@unifize.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lakshman.thatai@unifize.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>uni_CAM/New_Upload</template>
    </alerts>
</Workflow>
