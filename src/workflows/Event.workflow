<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Leave_Event</fullName>
        <description>Leave Event</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>Co_Founder</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>HR_Manager</recipient>
            <type>roleSubordinatesInternal</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Human_Resources/New_Leave_Event</template>
    </alerts>
    <rules>
        <fullName>Leave Application</fullName>
        <actions>
            <name>Leave_Event</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.Leave_Holiday_Type__c</field>
            <operation>equals</operation>
            <value>Privileged Leave (PL),Casual Leave (CL),Sick Leave (SL),Leave Without Pay (LWP)</value>
        </criteriaItems>
        <description>Triggers an email workflow for intimating the Co-Founders and the HR department that an Employee has created a Leave Application (Event)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
