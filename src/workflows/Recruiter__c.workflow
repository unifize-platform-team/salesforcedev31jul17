<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Unifize_Position_Alert</fullName>
        <description>New Unifize Position Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Recruiter_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Human_Resources/New_Unifize_Position</template>
    </alerts>
    <rules>
        <fullName>Recruiter Alert</fullName>
        <actions>
            <name>New_Unifize_Position_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Recruiter__c.Automatic_Emails__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Automatically sends an email alert to a Recruiter whenever they are added to a position and the &apos;Automatic Email&apos; Checkbox is checked to &apos;True&apos;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
