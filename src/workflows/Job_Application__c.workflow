<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Job_Application</fullName>
        <description>New Job Application</description>
        <protected>false</protected>
        <recipients>
            <field>Department_Manager__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <recipient>HR_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Human_Resources/Job_Application_Alert</template>
    </alerts>
    <rules>
        <fullName>Job Application Alert</fullName>
        <actions>
            <name>New_Job_Application</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Job_Application__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
