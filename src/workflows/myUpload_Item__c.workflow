<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UpdateAsUploadFailed</fullName>
        <field>Upload_Failed__c</field>
        <literalValue>1</literalValue>
        <name>UpdateAsUploadFailed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>UpdateAsUploadFailedAfterOneHour</fullName>
        <active>true</active>
        <criteriaItems>
            <field>myUpload_Item__c.File_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If the File_Name__c field is blank 1 hour after creation then flag record as failed</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>UpdateAsUploadFailed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>myUpload_Item__c.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
