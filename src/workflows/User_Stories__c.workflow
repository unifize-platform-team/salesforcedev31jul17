<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_developer_or_vendor_when</fullName>
        <description>Email to developer or vendor when</description>
        <protected>false</protected>
        <recipients>
            <field>Developer_Assigned__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Development_Manager_Templates/email_users</template>
    </alerts>
    <rules>
        <fullName>Email the developers when user stories as assigned</fullName>
        <actions>
            <name>Email_to_developer_or_vendor_when</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(Developer_Assigned__c &lt;&gt; NULL,ISCHANGED(Developer_Assigned__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
