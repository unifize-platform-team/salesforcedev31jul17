<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Deployed_Item</fullName>
        <description>Deployed Item</description>
        <protected>false</protected>
        <recipients>
            <recipient>Chief_Technical_Officer</recipient>
            <type>roleSubordinatesInternal</type>
        </recipients>
        <recipients>
            <recipient>ben@unifize.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Development_Manager_Templates/Deployed_Item</template>
    </alerts>
    <rules>
        <fullName>New Deployed Item</fullName>
        <actions>
            <name>Deployed_Item</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Deployed_Item__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Informs System Administrators that a New Deployed Item has been released</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
