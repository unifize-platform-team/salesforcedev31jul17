<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Review</fullName>
        <description>New Review</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Manager</recipient>
            <type>roleSubordinatesInternal</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Human_Resources/New_Review</template>
    </alerts>
    <rules>
        <fullName>New Review</fullName>
        <actions>
            <name>New_Review</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Review__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Triggers an email alert to the HR team whenever a new review is received for a Candidate</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
