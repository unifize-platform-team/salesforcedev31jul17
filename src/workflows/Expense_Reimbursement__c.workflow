<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CoFounder_Approval</fullName>
        <description>CoFounder Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>Co_Founder</recipient>
            <type>roleSubordinatesInternal</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Human_Resources/Expense_Report</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_for_CO_Founders</fullName>
        <description>Email Alert for CO-Founders</description>
        <protected>false</protected>
        <recipients>
            <recipient>Co_Founder</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Human_Resources/Expense_Report</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_for_Manager</fullName>
        <description>Email Alert for Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>Chief_Technical_Officer</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>HR_Manager</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Partner_Manager</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Sales_Marketing</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Service_Delivery_Head</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Human_Resources/Expense_Report</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Accounts</fullName>
        <description>Email to Accounts</description>
        <protected>false</protected>
        <recipients>
            <recipient>accounts@unifize.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Human_Resources/EmailToAccounts</template>
    </alerts>
    <alerts>
        <fullName>Final_Approval_Actions</fullName>
        <ccEmails>rohit_anand1990@yahoo.co.in</ccEmails>
        <description>Final Approval Actions</description>
        <protected>false</protected>
        <recipients>
            <recipient>rohit.anand@unifize.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Human_Resources/Expense_Report</template>
    </alerts>
    <alerts>
        <fullName>Final_Rejected_Expense_Reimbursement</fullName>
        <description>Final Rejected Expense Reimbursement</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>HR_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Human_Resources/Rejected_Expense_Reimbursement</template>
    </alerts>
    <alerts>
        <fullName>Initial_Submission_Actions</fullName>
        <description>Initial Submission Actions</description>
        <protected>false</protected>
        <recipients>
            <recipient>rohit.anand@unifize.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Human_Resources/Expense_Report</template>
    </alerts>
    <alerts>
        <fullName>Manager_Approval</fullName>
        <description>Manager Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>Chief_Technical_Officer</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>HR_Manager</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Partner_Manager</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Sales_Marketing</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <recipients>
            <recipient>Service_Delivery_Head</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Human_Resources/Expense_Report</template>
    </alerts>
    <alerts>
        <fullName>Rejected_Email_Alert</fullName>
        <description>Rejected Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Human_Resources/Rejected_Expense_Reimbursement</template>
    </alerts>
    <fieldUpdates>
        <fullName>Appove_by_Manager</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Appove by Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Field_Update</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approval Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Automatic_Approve</fullName>
        <description>If the Manager is not approving within 3 days,the mail should go to Co-founder automatically</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Automatic Approve</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Expenses_Approval</fullName>
        <description>First Step of Approval process</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Expenses Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Expenses_Approval_Rejected</fullName>
        <description>field update works when Expense reimbursement approval is rejected</description>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Expenses Approval Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Initial_Status</fullName>
        <description>Update the filed as &apos;Pending&apos; when the Expense Reimbursement is created.</description>
        <field>Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Initial Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_Field_Update</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Rejected Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_for_Approval</fullName>
        <field>Status__c</field>
        <literalValue>Submitted_for_Approval</literalValue>
        <name>Submitted for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
