<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>myProject_Upload_Notes_Comments</fullName>
        <field>Upload_Notes_Comments__c</field>
        <formula>Upload__r.Notes_Comments__c</formula>
        <name>myProject Upload Notes/Comments</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Upload Notes%2FComments</fullName>
        <actions>
            <name>myProject_Upload_Notes_Comments</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>myProject__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Triggers the Upload Notes/ Comments into the myProjects Uploads Notes/Comments</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>myUploadWorkOrder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>myProject__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Calls a flow that creates a Work Order and the associated uniCHECK General and Account Points whenever a myUpload is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
